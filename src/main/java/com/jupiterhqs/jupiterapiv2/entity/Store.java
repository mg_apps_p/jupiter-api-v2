package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;
import java.util.Set;

@Entity()
@Table(name = "stores")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE stores SET store_deleted = true WHERE id = ?")
@Where(clause = "store_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Store {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_stores_store_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_stores_store_id",
            sequenceName = "seq_stores_store_id"
    )
    private long storeId;

    @Column(name = "store_name", length = 50, nullable = false, unique = true)
    private String name;

    @Column(name = "store_address", nullable = false, unique = true)
    private String address;

    @Column(name = "store_email", nullable = false)
    private String email;

    @Column(name = "store_phone", nullable = false)
    private String phone;

    @CreatedDate
    @Column(name = "store_created")
    private Date created;

    @LastModifiedDate
    @Column(name = "store_updated")
    private Date updated;

    @Column(name = "store_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "store")
    @JsonBackReference("expense_store_id_ref")
    private Set<Expense> expenses;

    @OneToMany(mappedBy = "store")
    @JsonBackReference("purchase_store_id_ref")
    private Set<Purchase> purchases;
    
    @OneToMany(mappedBy = "store")
    @JsonBackReference("product_store_store_id_ref")
    private Set<ProductStore> productStores;
    
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "stores")
    @JsonIgnore
    private Set<User> users;
}
