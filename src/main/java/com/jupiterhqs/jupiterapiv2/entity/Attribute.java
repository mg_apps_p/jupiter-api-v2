package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.jupiterhqs.jupiterapiv2.util.enums.AttributeTypeEnum;
import com.jupiterhqs.jupiterapiv2.util.enums.AttributeUnitEnum;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity()
@Table(name = "attribute_catalog")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE attribute_catalog SET attribute_deleted = true WHERE id = ?")
@Where(clause = "attribute_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Attribute {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_attribute_catalog_attribute_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_attribute_catalog_attribute_id",
            sequenceName = "seq_attribute_catalog_attribute_id"
    )
    private long attributeId;

    @Column(name = "attribute_name", length = 50, nullable = false, unique = true)
    private String name;

    @Column(name = "attribute_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private AttributeTypeEnum type;

    @Column(name = "attribute_unit", nullable = false)
    @Enumerated(EnumType.STRING)
    private AttributeUnitEnum unit;

    @CreatedDate
    @Column(name = "attribute_created")
    private Date created;

    @LastModifiedDate
    @Column(name = "attribute_updated")
    private Date updated;

    @Column(name = "attribute_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;
    
    @OneToMany
    @JsonManagedReference(value = "attribute_attribute_value_ref")
    private List<AttributeValue> attributeValues;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.ALL,
            },
            mappedBy = "attributes")
    @JsonIgnore
    private Set<Category> categories;
}


