package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.List;

@Entity()
@Table(name = "status_catalog")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE status_catalog SET status_deleted = true WHERE id = ?")
@Where(clause = "status_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_status_catalog_status_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_status_catalog_status_id",
            sequenceName = "seq_status_catalog_status_id")
    private long statusId;

    @Column(name = "status_name", nullable = false)
    private String name;

    @CreatedDate
    @Column(name = "status_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "status_updated")
    private LocalDateTime updated;

    @Column(name = "status_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "status")
    @JsonBackReference("expense_category_status_id_ref")
    private List<Expense> expenses;

    @OneToMany(mappedBy = "status")
    @JsonBackReference
    private List<Sale> sales;

    @OneToMany(mappedBy = "status")
    @JsonBackReference
    private List<Purchase> purchases;
    
    @OneToMany(mappedBy = "status")
    @JsonBackReference
    private List<ProductTransfer> productTransfers;
}
