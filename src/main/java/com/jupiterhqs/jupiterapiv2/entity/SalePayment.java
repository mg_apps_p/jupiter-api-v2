package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "sale_payments")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE sale_payments SET sale_payments_deleted = true WHERE id = ?")
@Where(clause = "sale_payments_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class SalePayment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sale_payments_sale_payments_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_sale_payments_sale_payments_id",
            sequenceName = "seq_sale_payments_sale_payments_id"
    )
    private long salePaymentId;

    @Column(name = "sale_payments_amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "sale_payments_note")
    private String note;

    @CreatedDate
    @Column(name = "sale_payments_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "sale_payments_updated")
    private LocalDateTime updated;

    @Column(name = "sale_payments_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "sale_payments_sale_id")
    @JsonManagedReference("sale_payments_sale_id_ref")
    private Sale sale;

    @ManyToOne(optional = false)
    @JoinColumn(name = "sale_payments_payment_type_id")
    @JsonManagedReference(value = "sale_payments_payment_type_id_ref")
    private PaymentType paymentType;
}
