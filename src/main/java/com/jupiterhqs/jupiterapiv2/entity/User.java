package com.jupiterhqs.jupiterapiv2.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jupiterhqs.jupiterapiv2.util.enums.Role;

@Entity
@Table(name = "users")
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE users SET users_deleted = true WHERE id = ?")
@Where(clause = "users_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class User implements UserDetails {

    private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_users_users_id")
    @SequenceGenerator(
            name = "seq_users_users_id",
            sequenceName = "seq_users_users_id",
            initialValue = 1,
            allocationSize = 1

    )
    private long userId;

    @Column(name = "users_firstname", length = 50, nullable = false)
    private String firstname;

    @Column(name = "users_lastname", length = 50, nullable = false)
    private String lastname;

    @JsonIgnore
    @Column(name = "users_password", length = 255, nullable = false)
    private String password;
    
    @Column(name = "users_email", length = 255, nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    @CreatedDate
    @Column(name = "users_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "users_updated")
    private LocalDateTime updated;

    @Column(name = "users_deleted")
    @Builder.Default
    private Boolean deleted = false;
    
    @Column(name = "users_default_store_id", nullable = false)
    private Long defaultStoreId;
    
    @Column(name = "users_default_store_name")
    private String defaultStoreName;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_store",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "store_id") })
    private Set<Store> stores;
    
    @OneToMany(mappedBy="user")
    @JsonBackReference(value = "sales_user_id_ref")
    private List<Sale> sales;
    
    @OneToMany(mappedBy="user")
    @JsonBackReference(value = "sales_user_id_ref")
    private List<ProductTransfer> productTransfers;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !deleted;
    }
}


