package com.jupiterhqs.jupiterapiv2.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Entity()
@Table(name = "sales")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE sales SET sale_deleted = true WHERE id = ?")
@Where(clause = "sale_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sales_sale_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_sales_sale_id",
            sequenceName = "seq_sales_sale_id")
    private long saleId;

    @Column(name = "sale_description", nullable = true)
    private String description;

    @Column(name = "sale_quantity", nullable = false)
    private long quantity;

    @Column(name = "sale_amount", nullable = false)
    private BigDecimal amount;
    
    @Column(name = "sale_partial_paid", nullable = false)
    private BigDecimal partialPaid;

    @Column(name = "sale_cert", length = 4096, nullable = false)
    private String cert;

    @Column(name = "sale_seller", nullable = false)
    private String seller;

    @CreatedDate
    @Column(name = "sale_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "sale_updated")
    private LocalDateTime updated;

    @Column(name = "sale_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false)
    @JsonManagedReference(value = "sale_status_ref")
    @JoinColumn(name = "sale_status_id")
    private Status status;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "sale_payment_type_id")
    @JsonManagedReference(value = "sale_payment_type_ref")
    private PaymentType paymentType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sale", fetch = FetchType.EAGER)
    @JsonManagedReference(value = "sale_sale_details_ref")
    private Set<SaleDetail> saleDetails;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JsonManagedReference("customer_sale_ref")
    @JoinColumn(name = "sale_customer_id")
    private Customer customer;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JsonManagedReference("store_sale_ref")
    @JoinColumn(name = "sale_store_id")
    private Store store;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "sale_payment_status_id")
    @JsonManagedReference(value = "sale_payment_status_id_ref")
    private PaymentStatus paymentStatus;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="sale_user_id")
    @JsonIgnore
    private User user;
}
