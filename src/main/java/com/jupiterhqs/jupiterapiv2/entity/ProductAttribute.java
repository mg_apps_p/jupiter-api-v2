package com.jupiterhqs.jupiterapiv2.entity;

import com.jupiterhqs.jupiterapiv2.model.ProductAttributeId;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity()
@Table(name = "product_attributes")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE product_attributes SET product_deleted = true WHERE id = ?")
@Where(clause = "product_attribute_deleted = false")
@EntityListeners(AuditingEntityListener.class)
@IdClass(ProductAttributeId.class)
public class ProductAttribute {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "product_attribute_product_id")
    private Product product;

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "product_attribute_attribute_id")
    private Attribute attribute;

    @Column(name = "product_attribute_value")
    private String value;

    @CreatedDate
    @Column(name = "product_attribute_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "product_attribute_updated")
    private LocalDateTime updated;

    @Column(name = "product_attribute_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false; 
}


