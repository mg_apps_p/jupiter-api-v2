package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Entity()
@Table(name = "purchases")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE purchases SET purchase_deleted = true WHERE id = ?")
@Where(clause = "purchase_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_purchases_purchase_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_purchases_purchase_id",
            sequenceName = "seq_purchases_purchase_id"
    )
    private long purchaseId;

    @Column(name = "purchase_amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "purchase_partial_paid", nullable = false)
    private BigDecimal partialPaid;

    @Column(name = "purchase_reference", length = 512, nullable = false)
    private String reference;

    @CreatedDate
    @Column(name = "purchase_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "purchase_updated")
    private LocalDateTime updated;

    @Column(name = "purchase_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "purchase_vendor_id")
    @JsonManagedReference("purchase_store_id_ref")
    private Vendor vendor;

    @OneToMany(mappedBy = "purchase", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference("purchase_purchase_detail_id_ref")
    private Set<PurchaseDetail> purchaseDetails;

    @ManyToOne(optional = false)
    @JoinColumn(name = "purchase_payment_status_id")
    @JsonManagedReference(value = "purchase_payment_status_id_ref")
    private PaymentStatus paymentStatus;

    @ManyToOne(optional = false)
    @JsonManagedReference(value = "purchase_status_id_ref")
    @JoinColumn(name = "purchase_status_id")
    private Status status;

    @ManyToOne(optional = false)
    @JoinColumn(name = "purchase_store_id")
    private Store store;
}
