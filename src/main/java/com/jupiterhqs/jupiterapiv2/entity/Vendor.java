package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.*;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;
import java.util.Set;

@Entity()
@Table(name = "vendors")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE vendors SET vendors_deleted = true WHERE id = ?")
@Where( clause = "vendors_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Vendor {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_vendors_vendor_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_vendors_vendor_id",
            sequenceName = "seq_vendors_vendor_id"
    )
    private long vendorId;

    @Column(name = "vendors_name", length = 50, nullable = false)
    private String name;

    @Column(name = "vendors_description",  nullable = true)
    private String description;

    @CreatedDate
    @Column(name = "vendors_created")
    private Date created;

    @LastModifiedDate
    @Column(name = "vendors_updated")
    private Date updated;

    @Column(name = "vendors_deleted",  nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "vendor", fetch = FetchType.LAZY)
    @JsonBackReference(value = "purchase_vendor_id_ref")
    private Set<Purchase> purchases;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Vendor{");
        sb.append("vendorId=").append(vendorId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", created=").append(created);
        sb.append(", updated=").append(updated);
        sb.append(", deleted=").append(deleted);
        sb.append('}');
        return sb.toString();
    }
}
