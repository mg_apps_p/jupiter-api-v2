package com.jupiterhqs.jupiterapiv2.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.jupiterhqs.jupiterapiv2.model.RelatedProductId;

import java.time.LocalDateTime;

@Entity()
@Table(name = "related_products")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Builder
@SQLDelete(sql = "UPDATE related_products SET related_product_deleted = true WHERE id = ?")
@Where(clause = "related_product_deleted = false")
@EntityListeners(AuditingEntityListener.class)
@IdClass(RelatedProductId.class)
public class RelatedProduct {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "related_product_related_product_id")
    private Product relatedProduct;

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "related_product_product_id")
    private Product product;

    @CreatedDate
    @Column(name = "related_product_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "related_product_updated")
    private LocalDateTime updated;

    @Column(name = "related_product_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;


}
