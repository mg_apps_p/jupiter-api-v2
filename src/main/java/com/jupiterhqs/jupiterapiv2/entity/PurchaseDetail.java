package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "purchase_detail")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE purchase_detail SET purchase_detail_deleted = true WHERE id = ?")
@Where(clause = "purchase_detail_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class PurchaseDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_purchase_detail_purchase_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_purchase_detail_purchase_id",
            sequenceName = "seq_purchase_detail_purchase_id"
    )
    private long purchaseDetailId;

    @Column(name = "purchase_detail_quantity", nullable = false)
    private Long quantity;

    @Column(name = "purchase_detail_price", nullable = false)
    private BigDecimal price;

    @Column(name = "purchase_detail_total_price", nullable = false)
    private BigDecimal totalPrice;

    @CreatedDate
    @Column(name = "purchase_detail_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "purchase_detail_updated")
    private LocalDateTime updated;

    @Column(name = "purchase_detail_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;
    
    @ManyToOne
    @JoinColumn(name ="purchase_detail_purchase_id")
    @JsonBackReference
    private Purchase purchase;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "purchase_detail_product_store_id")
    @JsonManagedReference(value = "purchase_detail_product_store_id_ref")
    private ProductStore productStore;
}
