package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.*;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity()
@Table(name = "subcategories_catalog")
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE subcategories_catalog SET subcategories_deleted = true WHERE id = ?")
@Where(clause = "subcategories_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Subcategory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_subcategories_subcategories_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_subcategories_subcategories_id",
            sequenceName = "seq_subcategories_subcategories_id"
    )
    private long subcategoryId;

    @Column(name = "subcategories_name", length = 50, nullable = false)
    private String name;

    @CreatedDate
    @Column(name = "subcategories_created")
    private Date created;

    @LastModifiedDate
    @Column(name = "subcategories_updated")
    private Date updated;

    @Column(name = "subcategories_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false)
    @JoinColumn(name = "subcategories_category_id", nullable = false)
    @JsonBackReference("subcategory_category_id_ref")
    private Category category;

    @OneToMany(mappedBy = "subcategory", fetch = FetchType.LAZY)
    @JsonBackReference("subcategory_product_id_ref")
    @JsonIgnore
    private List<Product> products;
    
    @Override
    public int hashCode() {
    	return Objects.hash(name, created);
    }
}
