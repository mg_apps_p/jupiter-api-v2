package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity()
@Table(name = "product_transfer_detail")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE product_transfer_detail SET product_transfer_detail_deleted = true WHERE id = ?")
@Where(clause = "product_transfer_detail_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class ProductTransferDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_product_transfer_detail_product_transfer_detail_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_product_transfer_detail_product_transfer_detail_id",
            sequenceName = "seq_product_transfer_detail_product_transfer_detail_id"
    )
    private long productTransferDetailId;

    @Column(name = "product_transfer_detail_description", nullable = false)
    private Long quantity;

    @CreatedDate
    @Column(name = "product_transfer_detail_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "product_transfer_detail_updated")
    private LocalDateTime updated;

    @Column(name = "product_transfer_detail_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false)
    @JoinColumn(name = "product_transfer_detail_product_store_origin_id")
    @JsonManagedReference(value = "product_transfer_detail_product_store_origin_id_ref")
    private ProductStore productStoreOrigin;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "product_transfer_detail_product_store_destination_id")
    @JsonManagedReference(value = "product_transfer_detail_product_store_destination_id_ref")
    private ProductStore productStoreDestination;
    
    @ManyToOne
    @JoinColumn(name = "product_transfer_detail_product_transfer_id")
    @JsonBackReference
    private ProductTransfer productTransfer;
}
