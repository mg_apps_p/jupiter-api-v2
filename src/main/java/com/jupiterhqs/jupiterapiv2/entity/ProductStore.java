package com.jupiterhqs.jupiterapiv2.entity;


import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity()
@Table(name = "product_stores")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE product_stores SET product_store_deleted = true WHERE id = ?")
@Where(clause = "product_store_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class ProductStore {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_product_store_product_store_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_product_store_product_store_id",
            sequenceName = "seq_product_store_product_store_id"
    )
    private long productStoreId;

	@ManyToOne(optional = false)
	@JoinColumn(name = "product_store_product_id")
    @JsonIdentityReference(alwaysAsId = true)
	private Product product;

	@ManyToOne(optional = false)
	@JoinColumn(name = "product_store_store_id")
	private Store store;

	@Column(name = "product_store_quantity", nullable = false)
	private Long quantity;

	@Column(name = "product_store_price", nullable = false)
	private BigDecimal price;

	@CreatedDate
	@Column(name = "product_store_created")
	private LocalDateTime created;

	@LastModifiedDate
	@Column(name = "product_store_updated")
	private LocalDateTime updated;

	@Column(name = "product_store_deleted", nullable = false)
	@Builder.Default
	private Boolean deleted = false;

	@Override
	public int hashCode() {
		return Objects.hash(productStoreId, product, store);
	}
		
}
