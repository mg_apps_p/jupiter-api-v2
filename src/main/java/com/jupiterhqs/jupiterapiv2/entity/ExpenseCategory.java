package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.Set;

@Entity()
@Table(name = "expense_category_catalog")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE expense_category_catalog SET expense_category_deleted = true WHERE id = ?")
@Where(clause = "expense_category_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class ExpenseCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_expense_category_expense_category_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_expense_category_expense_category_id",
            sequenceName = "seq_expense_category_expense_category_id")
    private long expenseCategoryId;

    @Column(name = "expense_category_name", length = 100, unique = true, nullable = false)
    private String name;

    @CreatedDate
    @Column(name = "expense_category_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "expense_category_updated")
    private LocalDateTime updated;

    @Column(name = "expense_category_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy ="expenseCategory", cascade = CascadeType.ALL)
    @JsonBackReference("expense_category_expense_id_ref")
    private Set<Expense> expenses;
}
