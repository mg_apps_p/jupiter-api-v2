package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "expenses")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@Builder
@SQLDelete(sql = "UPDATE expenses SET expense_deleted = true WHERE id = ?")
@Where(clause = "expense_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Expense {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_expense_expense_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_expense_expense_id",
            sequenceName = "seq_expense_expense_id")
    private long expenseId;

    @Column(name = "expense_name", nullable = false)
    private String name;

    @Column(name = "expense_amount", nullable = false)
    private BigDecimal amount;

    @CreatedDate
    @Column(name = "expense_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "expense_updated")
    private LocalDateTime updated;

    @Column(name = "expense_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "expense_payment_type")
    @JsonManagedReference
    private PaymentType paymentType;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "expense_expense_category_id")
    @JsonManagedReference("expense_category_expense_id_ref")
    private ExpenseCategory expenseCategory;

    @ManyToOne(optional = false)
    @JoinColumn(name = "expense_status_id")
    @JsonManagedReference("expense_status_id_ref")
    private Status status;

    @ManyToOne(optional = false)
    @JoinColumn(name = "expense_store_id")
    @JsonManagedReference("expense_store_id_ref")
    private Store store;
}
