package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.Set;

@Entity()
@Table(name = "product_transfer")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE product_transfer SET product_transfer_deleted = true WHERE id = ?")
@Where(clause = "product_transfer_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class ProductTransfer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_product_transfer_product_transfer_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_product_transfer_product_transfer_id",
            sequenceName = "seq_product_transfer_product_transfer_id"
    )
    private long productTransferId;

    @Column(name = "product_transfer_description", length = 50, nullable = false)
    private String description;
    
    @Column(name = "product_transfer_quantity", nullable = false)
    private Long quantity;

    @CreatedDate
    @Column(name = "product_transfer_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "product_transfer_updated")
    private LocalDateTime updated;

    @Column(name = "product_transfer_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productTransfer", fetch = FetchType.EAGER)
    @JsonManagedReference(value = "sale_sale_details_ref")
    private Set<ProductTransferDetail> productTransferDetails;
    
    @ManyToOne(optional = false)
    @JoinColumn(name="product_transfer_user_id")
    @JsonIgnore
    private User user; 
    
    @ManyToOne(optional = false)
    @JsonManagedReference(value = "product_transfer_status_id")
    @JoinColumn(name = "product_transfer_status_id_ref")
    private Status status;
}
