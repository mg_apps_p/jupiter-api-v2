package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "payment_types_catalog")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE payment_types_catalog SET payment_type_deleted = true WHERE id = ?")
@Where(clause = "payment_type_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class PaymentType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_payment_types_payment_type_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_payment_types_payment_type_id",
            sequenceName = "seq_payment_types_payment_type_id"
    )
    private long paymentTypeId;

    @Column(name = "payment_type_name", length = 50, nullable = false, unique = true)
    private String name;

    @Column(name = "payment_type_description", nullable = false)
    private String description;

    @CreatedDate
    @Column(name = "payment_type_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "payment_type_updated")
    private LocalDateTime updated;

    @Column(name = "payment_type_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "paymentType", fetch = FetchType.LAZY)
    @JsonBackReference
    @JsonIgnore
    private List<Sale> sales;

    @Override
    public String toString() {
        return "name =" + name  + "description = " + description;
    }
}
