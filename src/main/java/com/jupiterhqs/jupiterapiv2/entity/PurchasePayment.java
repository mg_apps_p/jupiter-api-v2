package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "purchase_payments")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE purchase_payments SET purchase_payments_deleted = true WHERE id = ?")
@Where(clause = "purchase_payments_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class PurchasePayment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_purchase_payments_purchase_payments_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_purchase_payments_purchase_payments_id",
            sequenceName = "seq_purchase_payments_purchase_payments_id"
    )
    private long purchasePaymentId;

    @Column(name = "purchase_payments_amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "purchase_payments_note")
    private String note;

    @CreatedDate
    @Column(name = "purchase_payments_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "purchase_payments_updated")
    private LocalDateTime updated;

    @Column(name = "purchase_payments_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "purchase_payments_purchase_id")
    @JsonManagedReference("purchase_payments_purchase_id_ref")
    private Purchase purchase;

    @ManyToOne(optional = false)
    @JoinColumn(name = "purchase_payments_payment_type_id")
    @JsonManagedReference(value = "purchase_payments_payment_type_id_ref")
    private PaymentType paymentType;
}
