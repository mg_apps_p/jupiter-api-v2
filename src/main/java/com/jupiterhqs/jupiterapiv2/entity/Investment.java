package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "investments")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Builder
@SQLDelete(sql = "UPDATE investments SET investment_deleted = true WHERE id = ?")
@Where(clause = "investment_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Investment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_investments_investment_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_investments_investment_id",
            sequenceName = "seq_investments_investment_id"
    )
    private long investmentId;

    @Column(name = "investment_name", length = 255, nullable = false)
    private String name;

    @Column(name = "Investment_amount", nullable = false)
    private BigDecimal amount;

    @CreatedDate
    @Column(name = "investment_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "investment_updated")
    private LocalDateTime updated;

    @Column(name = "investment_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "investment_payment_type_catalog_id")
    @JsonManagedReference
    private PaymentType paymentType;

}
