package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.List;

@Entity()
@Table(name = "product_code_catalog")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE product_code_catalog SET product_code_deleted = true WHERE id = ?")
@Where(clause = "product_code_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class ProductCode {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_product_code__product_code_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_product_code__product_code_id",
            sequenceName = "seq_product_code_product_code_id"
    )
    private long productCodeId;

    @Column(name = "product_code_name", length = 50, nullable = false, unique = true)
    private String name;

    @Column(name = "product_code_description", length = 50, nullable = false)
    private String description;

    @CreatedDate
    @Column(name = "product_code_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "product_code_updated")
    private LocalDateTime updated;

    @Column(name = "product_code_deleted")
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "productCode", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Product> products;
}


