package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Entity()
@Table(name = "sale_details")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE sale_details SET sale_detail_deleted = true WHERE id = ?")
@Where(clause = "sale_detail_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class SaleDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sale_details_sale_detail_id")
    @SequenceGenerator(
            
            name = "seq_sale_details_sale_detail_id",
            sequenceName = "seq_sale_details_sale_detail_id")
    private long saleDetailId;

    @Column(name = "sale_detail_description", nullable = true)
    private String description;

    @Column(name = "sale_detail_quantity", nullable = false)
    private long quantity;

    @Column(name = "sale_detail_price", nullable = false)
    private BigDecimal price;

    @Column(name = "sale_detail_total_price", nullable = false)
    private BigDecimal totalPrice;

    @Column(name = "sale_detail_created")
    @CreatedDate
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "sale_detail_updated")
    private LocalDateTime updated;

    @Column(name = "sale_detail_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne
    @JoinColumn(name = "sale_detail_sales_id")
    @JsonBackReference
    private Sale sale;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "sale_detail_product_store_id")
    @JsonManagedReference("sale_detail_product_store_id_ref")
    private ProductStore productStore;

    @OneToMany(mappedBy = "saleDetail", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<ProductTag> productTags;
}
