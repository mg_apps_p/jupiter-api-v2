package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "payment_status_catalog")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE payment_status_catalog SET payment_status_deleted = true WHERE id = ?")
@Where(clause = "payment_status_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class PaymentStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_payment_status_payment_status_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_payment_status_payment_status_id",
            sequenceName = "seq_payment_status_payment_status_id"
    )
    private long paymentStatusId;

    @Column(name = "payment_status_name", length = 50, nullable = false, unique = true)
    private String name;

    @CreatedDate
    @Column(name = "payment_status_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "payment_status_updated")
    private LocalDateTime updated;

    @Column(name = "payment_status_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy="paymentStatus")
    @JsonBackReference("purchases_payment_status_id_ref")
    private List<Purchase> purchases;
    
    @OneToMany(mappedBy="paymentStatus")
    @JsonBackReference("sales_payment_status_id_ref")
    private List<Sale> sales;
}
