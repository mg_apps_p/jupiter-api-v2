package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.List;

@Entity()
@Table(name = "customers")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE customers SET customer_deleted = true WHERE id = ?")
@Where(clause = "customer_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_customers_customer_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_customers_customer_id",
            sequenceName = "seq_customers_customer_id")
    private long customerId;

    @Column(name = "customer_name", unique = true, length = 50, nullable = false)
    private String name;

    @Column(name = "customer_phone")
    private String phone;

    @Column(name = "customer_email", unique = true)
    private String email;

    @Column(name = "customer_rfc",  unique = true)
    private String rfc;

    @CreatedDate
    @Column(name = "customer_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "customer_updated")
    private LocalDateTime updated;

    @Column(name = "customer_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @JsonBackReference("customer_sale_ref")
    private List<Sale> sales;
}
