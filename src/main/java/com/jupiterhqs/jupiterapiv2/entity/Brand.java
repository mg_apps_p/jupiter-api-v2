package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;
import java.util.Set;

@Entity()
@Table(name = "brands")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@Builder
@SQLDelete(sql = "UPDATE brands SET brand_deleted = true WHERE id = ?")
@Where( clause = "brand_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_brands_brand_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_brands_brand_id",
            sequenceName = "seq_brands_brand_id"
    )
    private long brandId;

    @Column(name = "brand_name", length = 50, nullable = false, unique = true)
    private String name;

    @CreatedDate
    @Column(name = "brand_created")
    private Date created;

    @LastModifiedDate
    @Column(name = "brand_updated")
    private Date updated;

    @Column(name = "brand_deleted",  nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
    @JsonBackReference(value = "product_brands_id_ref")
    private Set<Product> products;
}
