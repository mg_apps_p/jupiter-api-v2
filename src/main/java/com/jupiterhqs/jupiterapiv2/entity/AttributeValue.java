package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Entity()
@Table(name = "attribute_values_catalog")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE attribute_values_catalog SET attribute_value_deleted = true WHERE id = ?")
@Where(clause = "attribute_value_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class AttributeValue {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_attribute_values_catalog_attribute_value_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_attribute_values_catalog_attribute_value_id",
            sequenceName = "seq_attribute_values_catalog_attribute_value_id"
    )
    private long attributeValueId;

    @Column(name = "attribute_value_value", length = 50, nullable = false, unique = true)
    private String value;

    @CreatedDate
    @Column(name = "attribute_value_created")
    private Date created;

    @LastModifiedDate
    @Column(name = "attribute_value_updated")
    private Date updated;

    @Column(name = "attribute_value_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false)
    @JoinColumn(name = "attribute_value_attribute_id")
    @JsonBackReference(value = "attribute_attribute_value_ref")
    private Attribute attribute;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AttributeValue{");
        sb.append("attributeValueId=").append(attributeValueId);
        sb.append(", value='").append(value).append('\'');
        sb.append(", created=").append(created);
        sb.append(", updated=").append(updated);
        sb.append(", deleted=").append(deleted);
        sb.append(", attribute=").append(attribute);
        sb.append('}');
        return sb.toString();
    }
}


