package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.Set;


@Entity()
@Table(name = "categories_catalog")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE categories_catalog SET category_deleted = true WHERE id = ?")
@Where(clause = "category_deleted = false")
@EntityListeners(AuditingEntityListener.class)
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_categories_category_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_categories_category_id",
            sequenceName = "seq_categories_category_id"
    )
    private Long categoryId;

    @Column(name = "category_name", unique = true, length = 50, nullable = false)
    private String name;

    @CreatedDate
    @Column(name = "category_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "category_updated")
    private LocalDateTime updated;

    @Column(name = "category_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
    @JsonManagedReference("subcategory_category_id_ref")
    @OrderBy("name")
    private Set<Subcategory> subcategories;

    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.ALL,
            })
    @JoinTable(name = "category_attributes",
            joinColumns = { @JoinColumn(name = "category_id") },
            inverseJoinColumns = { @JoinColumn(name = "attribute_id") })
    private Set<Attribute> attributes;
}
