package com.jupiterhqs.jupiterapiv2.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity()
@Table(name = "products")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@SQLDelete(sql = "UPDATE products SET product_deleted = true WHERE id = ?")
@Where(clause = "product_deleted = false")
@EntityListeners(AuditingEntityListener.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "productId", scope = Long.class)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_products_product_id")
    @SequenceGenerator(
            allocationSize = 1,
            name = "seq_products_product_id",
            sequenceName = "seq_products_product_id"
    )
    private long productId;

    @Column(name = "product_name", length = 50, nullable = false, unique = true)
    private String name;

    @Column(name = "product_description", length = 255, nullable = false)
    private String description;

    @Column(name = "product_suggested_price", nullable = false)
    private BigDecimal suggestedPrice;
    
    @Column(name = "product_average_sale_price", nullable = false)
    private BigDecimal averageSalePrice;
    
    @Column(name = "product_average_purchase_price", nullable = false)
    private BigDecimal averagePurchasePrice;

    @Column(name = "product_sku", length = 100, unique = true)
    private String sku;

    @Column(name = "product_code", length = 100, unique = true)
    private String code;

    @CreatedDate
    @Column(name = "product_created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "product_updated")
    private LocalDateTime updated;

    @Column(name = "product_deleted", nullable = false)
    @Builder.Default
    private Boolean deleted = false;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_subcategory_id")
    @JsonManagedReference(value = "subcategory_product_id_ref")
    private Subcategory subcategory;

    @ManyToOne(optional = false)
    @JoinColumn(name = "product_brand_id")
    @JsonManagedReference("product_brands_id_ref")
    private Brand brand;
    
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_product_code_id")
    @JsonManagedReference("product_code_products_ref")
    private ProductCode productCode;
    
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonIdentityReference(alwaysAsId = true)
    private List<ProductStore> productStores;
    
    @Override
    public int hashCode() {
        return Objects.hash(productId, name, description, created);
    }
}
