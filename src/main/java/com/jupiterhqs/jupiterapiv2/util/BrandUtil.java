package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Brand;
import com.jupiterhqs.jupiterapiv2.model.BrandDto;

public class BrandUtil {

    private BrandUtil() {
    }

    public static Brand create(Long brandId) {
        return Brand.builder()
                .brandId(brandId)
                .build();
    }

    public static Brand create(BrandDto brandDto) {
        return Brand.builder()
                .name(brandDto.getName())
                .build();
    }

    public static Brand create(String name) {
        return Brand.builder()
                .name(name)
                .build();
    }
}
