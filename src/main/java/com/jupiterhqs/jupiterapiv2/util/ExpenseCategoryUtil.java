package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.ExpenseCategory;
import com.jupiterhqs.jupiterapiv2.model.ExpenseCategoryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExpenseCategoryUtil {

    protected static final Logger log = LoggerFactory.getLogger(ExpenseCategoryUtil.class);

    private ExpenseCategoryUtil() {
    }

    public static ExpenseCategory create(ExpenseCategoryDto expenseCategoryDTO) {
        return ExpenseCategory.builder()
                .name(expenseCategoryDTO.getName())
                .build();
    }

    public static ExpenseCategory create(long expenseCategoryId, ExpenseCategoryDto expenseCategoryDTO) {
        return ExpenseCategory.builder()
                .expenseCategoryId(expenseCategoryId)
                .name(expenseCategoryDTO.getName())
                .build();
    }

    public static ExpenseCategory create(String name) {
        return ExpenseCategory.builder()
                .name(name)
                .build();
    }
}
