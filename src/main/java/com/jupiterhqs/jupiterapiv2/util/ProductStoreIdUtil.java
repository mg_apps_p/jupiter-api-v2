package com.jupiterhqs.jupiterapiv2.util;

import java.util.List;
import java.util.stream.Collectors;

import com.jupiterhqs.jupiterapiv2.model.ProductDto;
import com.jupiterhqs.jupiterapiv2.model.ProductStoreDto;
import com.jupiterhqs.jupiterapiv2.model.ProductStoreId;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDetailDto;
import com.jupiterhqs.jupiterapiv2.model.StoreDto;

public class ProductStoreIdUtil {

    private ProductStoreIdUtil() {
    }

    public static List<Long> create(List<PurchaseDetailDto> purchaseDetailsDto) {
        return purchaseDetailsDto.stream()
                .map(purchaseDetailDto -> purchaseDetailDto.getProductStore().getProductStoreId())
                .collect(Collectors.toList());
    }
    
   
    public static List<ProductStoreId> create(List<Long> productIds, long storeId) {
        return productIds.stream()
                .map(productId -> create(storeId, productId))
                .collect(Collectors.toList());
    }
    
    public static ProductStoreId create(ProductStoreDto productStoreDto) {
    	return ProductStoreId.builder()
    			.store(StoreUtil.create(productStoreDto.getStore().getStoreId()))
    			.product(ProductUtil.create(productStoreDto.getProduct().getProductId()))
    			.build();
    }
    
    public static ProductStoreId create(StoreDto storeDto, ProductDto productDto) {
    	return ProductStoreId.builder()
    			.store(StoreUtil.create(storeDto.getStoreId()))
    			.product(ProductUtil.create(productDto.getProductId()))
    			.build();
    }
    
    public static ProductStoreId create(long storeId, long productId) {
    	return ProductStoreId.builder()
    			.store(StoreUtil.create(storeId))
    			.product(ProductUtil.create(productId))
    			.build();
    }
    

    
}
