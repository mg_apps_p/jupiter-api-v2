package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Category;
import com.jupiterhqs.jupiterapiv2.entity.Subcategory;
import com.jupiterhqs.jupiterapiv2.model.SubcategoryDto;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SubcategoryUtil {

	public static Set<Subcategory> create(Set<SubcategoryDto> subcategoriesDto, Category category) {
		if (subcategoriesDto.isEmpty()) {
			throw new RuntimeException("SubcategoriesDTO can't be empty");
		}

		return subcategoriesDto.stream().map(subcategoryDto -> create(subcategoryDto, category)).collect(Collectors.toSet());
	}

	public static Subcategory create(Long subcategoryId) {
		return Subcategory.builder().subcategoryId(subcategoryId).build();
	}

	public static Subcategory create(String name, Category category) {
		return Subcategory.builder().name(name).category(category).build();
	}

	public static Subcategory create(SubcategoryDto subcategoryDto, Category category) {
		if(subcategoryDto.getSubcategoryId() == null) {
			return Subcategory.builder().name(subcategoryDto.getName()).category(category).build();
		}
		
		return Subcategory.builder()
				.subcategoryId( subcategoryDto.getSubcategoryId())
				.name(subcategoryDto.getName()).category(category).build();
	}
}
