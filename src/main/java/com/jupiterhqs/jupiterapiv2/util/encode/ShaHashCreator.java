package com.jupiterhqs.jupiterapiv2.util.encode;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ShaHashCreator {

    private ShaHashCreator() {
    }

    public static String create512Algorithm(String value) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        byte[] messageDigestBytes = md.digest(value.getBytes(StandardCharsets.UTF_8));

        return convertToHex(messageDigestBytes);
    }

    private static String convertToHex(final byte[] messageDigest) {
        BigInteger bigInt = new BigInteger(1, messageDigest);
        String hexText = bigInt.toString(16);
        while(hexText.length() < 32) {
            hexText = "0".concat(hexText);
        }

        return hexText;
    }
}
