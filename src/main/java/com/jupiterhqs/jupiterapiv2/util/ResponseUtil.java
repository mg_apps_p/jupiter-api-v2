package com.jupiterhqs.jupiterapiv2.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseUtil {

    public static <T> ResponseEntity<T> created(T body) {
        return  new ResponseEntity<T>(body, HttpStatus.CREATED);
    }
}
