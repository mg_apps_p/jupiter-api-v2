package com.jupiterhqs.jupiterapiv2.util;


import com.jupiterhqs.jupiterapiv2.entity.Vendor;
import com.jupiterhqs.jupiterapiv2.model.VendorDto;

public class VendorUtil {

    public static Vendor create(String name, String description) {
        Vendor vendor = new Vendor();
        vendor.setName(name);
        vendor.setDescription(description);

        return vendor;
    }

    public static Vendor create(Long vendorId, VendorDto vendorDto) {
        return Vendor.builder()
                .vendorId(vendorId)
                .name(vendorDto.getName())
                .description(vendorDto.getDescription())
                .build();
    }
    
    public static Vendor create(Long vendorId) {
        return Vendor.builder()
                .vendorId(vendorId)
                .build();
    }
}
