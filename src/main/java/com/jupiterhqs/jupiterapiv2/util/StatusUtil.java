package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Status;
import com.jupiterhqs.jupiterapiv2.model.StatusDto;
import com.jupiterhqs.jupiterapiv2.util.enums.StatusEnum;

public class StatusUtil {

    private StatusUtil() {
    }

    public static Status create(StatusDto statusDto) {
        return Status.builder()
                .name(statusDto.getName())
                .build();
    }

    public static Status create(String name) {
        return Status.builder()
                .name(name)
                .build();
    }

    public static Status create(long statusId) {
        return Status.builder()
                .statusId(statusId)
                .build();
    }

    public static Status create(StatusEnum statusEnum) {
        return Status.builder()
                .statusId(Long.valueOf(statusEnum.ordinal()))
                .build();
    }
}
