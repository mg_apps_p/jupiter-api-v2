package com.jupiterhqs.jupiterapiv2.util;

import java.util.ArrayList;

import com.jupiterhqs.jupiterapiv2.entity.User;
import com.jupiterhqs.jupiterapiv2.model.RegisterRequest;
import com.jupiterhqs.jupiterapiv2.model.StoreDto;
import com.jupiterhqs.jupiterapiv2.model.UserDto;
import com.jupiterhqs.jupiterapiv2.util.enums.Role;

public class UserUtil {


    private UserUtil() {
    }

    public static User create(UserDto userDto) {
        return User.builder()
                .firstname(userDto.getFirstname())
                .lastname(userDto.getLastname())
                .password(userDto.getPassword())
                .email(userDto.getEmail())
                .build();
    }
    
    public static User create(long userId) {
        return User.builder()
        		.userId(userId)
                .build();
    }

    public static User create(RegisterRequest request) {
        return User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .password(request.getPassword())
                .email(request.getEmail())
                .role(Role.USER)
                .stores(StoreUtil.create(request.getStores()))
                .defaultStoreId(new ArrayList<StoreDto>(request.getStores()).get(0).getStoreId())
                .build();
    }
}
