package com.jupiterhqs.jupiterapiv2.util;


import com.jupiterhqs.jupiterapiv2.entity.Sale;
import com.jupiterhqs.jupiterapiv2.model.SaleDetailDto;
import com.jupiterhqs.jupiterapiv2.model.SaleDto;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.PaymentStatusIds;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class SaleUtil {

    protected static final Logger log = LoggerFactory.getLogger(SaleUtil.class);

    private SaleUtil() {
    }

    public static Sale create(SaleDto saleDto) {
        BigDecimal amount = getAmount(saleDto.getSaleDetails());
        return Sale.builder()
                .paymentType(PaymentTypeUtil.create(
                        saleDto.getPaymentType().getPaymentTypeId(),
                        saleDto.getPaymentType()
                ))
                .description(saleDto.getDescription())
                .partialPaid(getPartialPaid(saleDto, amount))
                .status(StatusUtil.create(1L))
                .paymentStatus(PaymentStatusUtil.create(saleDto.getPaymentStatus()))
                .store(StoreUtil.create(saleDto.getStore().getStoreId()))
                .seller(saleDto.getUser().getFirstname())
                .user(UserUtil.create(saleDto.getUser().getUserId()))
                .build();
    }
    
    public static Sale create(Long saleId) {
        return Sale.builder()
        		.saleId(saleId)
                .build();
    }

    public static String buildCert(Sale sale){
        try {
            // TODO env variable
            String input = new StringBuilder().append("jupiterOutlet")
                    .append(sale.getCustomer().getCustomerId())
                    .append(sale.getCustomer().getName())
                    .append(new Date().getTime())
                    .append(sale.getPaymentType().getName())
                    .append(sale.getQuantity())
                    .append(sale.getSaleDetails().iterator().next().getProductTags().iterator().next().getSerialNumber())
                    .append(new Date().getTime())
                    .toString();
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());

            return StringParser.parseToHex(messageDigest);
        } catch (NoSuchAlgorithmException ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.SALE_CERT);

            throw new RuntimeException();
        }
    }

    public static BigDecimal sumAmount(List<Sale> sales) {
        return sales.stream()
                .map(Sale::getAmount)
                .reduce(BigDecimal.ZERO, (value, acc) -> acc.add(value));
    }
    
    private static BigDecimal getAmount(Set<SaleDetailDto> saleDetailsDto) {
        return saleDetailsDto.stream()
                .map(saleDetailDto -> saleDetailDto.getPrice().multiply(new BigDecimal(saleDetailDto
                        .getQuantity())))
                .reduce(BigDecimal.ZERO, (currentValue, accumulator) -> accumulator.add(currentValue));
    }
    
    public static BigDecimal getPartialPaid(SaleDto saleDto, BigDecimal amount) {
        BigDecimal partialPaid;
        long paymentStatusId = saleDto.getPaymentStatus().getPaymentStatusId();
        if (paymentStatusId == PaymentStatusIds.PAID) {
            partialPaid = amount;
        } else if (paymentStatusId == PaymentStatusIds.PARTIAL) {
            partialPaid = saleDto.getPartialPaid();
        } else {
            partialPaid = BigDecimal.ZERO;
        }

        return partialPaid;
    }
}
