package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Attribute;
import com.jupiterhqs.jupiterapiv2.model.AttributeDto;
import com.jupiterhqs.jupiterapiv2.util.enums.AttributeTypeEnum;
import com.jupiterhqs.jupiterapiv2.util.enums.AttributeUnitEnum;

public class AttributeUtil {

    private AttributeUtil() {
    }

    public static Attribute create(AttributeDto attributeDto) {
        return Attribute.builder()
                .name(attributeDto.getName())
                .type(attributeDto.getType())
                .unit(attributeDto.getUnit())
                .build();
    }

    public static Attribute create(Long attributeId, AttributeDto attributeDto) {
        return Attribute.builder()
                .attributeId(attributeId)
                .name(attributeDto.getName())
                .type(attributeDto.getType())
                .unit(attributeDto.getUnit())
                .build();
    }

    public static Attribute create(String name, AttributeTypeEnum type, AttributeUnitEnum unit) {
        return Attribute.builder()
                .name(name)
                .type(type)
                .unit(unit)
                .build();
    }
}
