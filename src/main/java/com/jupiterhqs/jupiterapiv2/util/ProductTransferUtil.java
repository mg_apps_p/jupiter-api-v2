package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.ProductTransfer;
import com.jupiterhqs.jupiterapiv2.model.ProductTransferDetailDto;
import com.jupiterhqs.jupiterapiv2.model.ProductTransferDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class ProductTransferUtil {

	protected static final Logger log = LoggerFactory.getLogger(ProductTransferUtil.class);

	private ProductTransferUtil() {
	}

	public static ProductTransfer create(ProductTransferDto productTransferDto) {
		return ProductTransfer.builder().description(productTransferDto.getDescription()).status(StatusUtil.create(1L))
				.user(UserUtil.create(productTransferDto.getUser().getUserId())).build();
	}

	public static ProductTransfer create(Long productTransferId) {
		return ProductTransfer.builder().productTransferId(productTransferId).build();
	}

	public static Set<Long> getProductStoreIds(Set<ProductTransferDetailDto> productTransferDetailDtos) {
		Set<Long> ids = new HashSet<Long>();
    	 productTransferDetailDtos.forEach(productTransferDetailDto -> {
    		 ids.add(productTransferDetailDto.getProductStoreOrigin().getProductStoreId());
			 ids.add(productTransferDetailDto.getProductStoreDestination().getProductStoreId());
    	 });
    			
    		
    	 return ids;
    }
}
