package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Attribute;
import com.jupiterhqs.jupiterapiv2.entity.Product;
import com.jupiterhqs.jupiterapiv2.entity.ProductAttribute;
import com.jupiterhqs.jupiterapiv2.model.AttributeDto;
import com.jupiterhqs.jupiterapiv2.model.ProductAttributeDto;

import java.util.List;
import java.util.stream.Collectors;

public class ProductAttributeUtil {

	private ProductAttributeUtil() {
	}


	public static ProductAttribute create(Product product, Attribute attribute) {
		return ProductAttribute.builder().product(product).attribute(attribute).build();
	}

	public static ProductAttribute create(Product product, Attribute attribute, String value) {
		return ProductAttribute.builder().product(product).attribute(attribute).value(value).build();
	}

	public static ProductAttribute create(Product product, AttributeDto attributeDto, String value) {
		return ProductAttribute.builder().product(product)
				.attribute(AttributeUtil.create(attributeDto.getAttributeId(), attributeDto)).value(value).build();
	}

	public static List<ProductAttribute> create(Product product, List<AttributeDto> attributes) {
		return attributes.stream()
				.map(attributeDto -> ProductAttributeUtil.create(product, attributeDto, attributeDto.getValue()))
				.collect(Collectors.toList());
	}

	public static List<ProductAttribute> update(Product product, List<ProductAttributeDto> productAttributesDto) {
		return productAttributesDto.stream().map(productAttributeDto -> create(product,
				productAttributeDto)).toList();
	}
	
	public static ProductAttribute create(Product product, ProductAttributeDto productAttributeDto ) {
	    String createdDate = productAttributeDto.getCreated();
		return ProductAttribute.builder()
				.product(product)
				.attribute(AttributeUtil.create(productAttributeDto.getAttribute().getAttributeId(), productAttributeDto.getAttribute()))
				.value(productAttributeDto.getValue())
				.created(createdDate != null ? DateUtil.getLocalDateTime(createdDate) : null)
				.build();
	}
}
