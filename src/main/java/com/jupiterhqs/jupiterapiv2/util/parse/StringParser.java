package com.jupiterhqs.jupiterapiv2.util.parse;

import java.math.BigInteger;

public class StringParser {
    private StringParser() {
    }


    public static String parseToHex(final byte[] messageDigest) {
        BigInteger bigint = new BigInteger(1, messageDigest);
        String hexText = bigint.toString(16);
        while (hexText.length() < 32) {
            hexText = "0".concat(hexText);
        }

        return hexText.toUpperCase();
    }

    public static String parseWithId(String message, long id) {
        return String.format("%s%s", message, "[" + id + "].");
    }

}
