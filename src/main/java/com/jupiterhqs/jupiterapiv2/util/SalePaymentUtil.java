package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.SalePayment;
import com.jupiterhqs.jupiterapiv2.model.PaymentTypeDto;
import com.jupiterhqs.jupiterapiv2.model.SaleDto;
import com.jupiterhqs.jupiterapiv2.model.SalePaymentDto;

public class SalePaymentUtil {

    private SalePaymentUtil() {
    }

    public static SalePayment create(SalePaymentDto salePaymentDto) {
        String createdDate = salePaymentDto.getCreated();
        PaymentTypeDto paymentTypeDto = salePaymentDto.getPaymentType();
        SaleDto saleDto = salePaymentDto.getSale();
        return SalePayment.builder()
                .amount(salePaymentDto.getAmount())
                .note(salePaymentDto.getNote())
                .paymentType(PaymentTypeUtil.create(paymentTypeDto.getPaymentTypeId(), paymentTypeDto))
                .created(createdDate != null ? DateUtil.getLocalDateTime(createdDate) : null)
                .sale(SaleUtil.create(saleDto.getSaleId()))
                .build();
    }
}
