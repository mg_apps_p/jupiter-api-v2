package com.jupiterhqs.jupiterapiv2.util;

import java.util.Set;

import com.jupiterhqs.jupiterapiv2.entity.ProductTransfer;
import com.jupiterhqs.jupiterapiv2.entity.ProductTransferDetail;
import com.jupiterhqs.jupiterapiv2.model.ProductStoreDto;
import com.jupiterhqs.jupiterapiv2.model.ProductTransferDetailDto;

public class ProductTransferDetailUtil {

    private ProductTransferDetailUtil() {
    }

    public static ProductTransferDetail create(ProductTransferDetailDto productTransferDetailDto, ProductTransfer productTransfer) {
        ProductStoreDto productStoreOriginDto = productTransferDetailDto.getProductStoreOrigin();
        ProductStoreDto productStoreDestinationDto = productTransferDetailDto.getProductStoreDestination();

        return ProductTransferDetail.builder()
                .quantity(productTransferDetailDto.getQuantity())
                .productStoreOrigin(ProductStoreUtil.create(productStoreOriginDto.getProductStoreId()))
                .productStoreDestination(ProductStoreUtil.create(productStoreDestinationDto.getProductStoreId()))
                .productTransfer(productTransfer)
                .build();
    }
    
    public static Long sumQuantity(Set<ProductTransferDetail> productTransferDetails) {
    	return productTransferDetails.stream().map(ProductTransferDetail::getQuantity)
    			.reduce(0L, (quantity, total) -> total + quantity);
    }
}
