package com.jupiterhqs.jupiterapiv2.util.export;

import com.jupiterhqs.jupiterapiv2.entity.ProductTag;
import com.jupiterhqs.jupiterapiv2.entity.Sale;
import com.jupiterhqs.jupiterapiv2.entity.SaleDetail;
import com.jupiterhqs.jupiterapiv2.entity.Store;
import com.jupiterhqs.jupiterapiv2.util.SaleDetailUtil;
import com.lowagie.text.Font;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SalePDFExporter {

    private final int FONT_SIZE_VALUE = 14;
    private final int TABLE_COLUMN_SIZE = 5;

    private Sale sale;
    private Document document;
    private PdfWriter writer;


    public void export(HttpServletResponse response) throws DocumentException, IOException {
        document = new Document(PageSize.A4);
        this.writer = PdfWriter.getInstance(document, response.getOutputStream());
        document.open();

        buildHeader();
        buildNewLines(1);
        buildSaleContact();
        buildNewLines(1);
        buildSaleDetails();
        buildNewLines(1);
        buildTotal();
        buildFooter();
        document.close();
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    private void buildHeader() {
        Paragraph title = new Paragraph("NOTA DE VENTA", getBoldFont(24));
        title.setAlignment(Paragraph.ALIGN_CENTER);

        Paragraph saleId = new Paragraph("No:  " + sale.getSaleId());
        saleId.setAlignment(Paragraph.ALIGN_RIGHT);

        Paragraph date = new Paragraph("Fecha:  " +
                LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        date.setAlignment(Paragraph.ALIGN_RIGHT);

        Paragraph logo = new Paragraph("Jupiter Computers", getBoldFont(16));
        logo.setAlignment(Paragraph.ALIGN_LEFT);

        Paragraph contact = new Paragraph(getStoreDetails(),
                getBasicFont(9)
        );
        contact.setAlignment(Paragraph.ALIGN_LEFT);

        document.add(title);
        document.add(saleId);
        document.add(date);
        document.add(logo);
        document.add(contact);
    }

    private void buildSaleContact() {
        Paragraph customerParagraph = new Paragraph("Cliente:  " + sale.getCustomer().getName(),
                getBasicFont(11));
        customerParagraph.setAlignment(Paragraph.ALIGN_LEFT);

        Paragraph paymentTypeParagraph = new Paragraph("Tipo Pago:  " + sale.getPaymentType().getName(),
                getBasicFont(11));
        paymentTypeParagraph.setAlignment(Paragraph.ALIGN_LEFT);

        document.add(customerParagraph);
        document.add(paymentTypeParagraph);

        if (sale.getDescription() != null) {
            Paragraph descriptionParagraph = new Paragraph("Descripción:  " + sale.getDescription(),
                    getBasicFont(11));
            descriptionParagraph.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(descriptionParagraph);
        }
    }

    private void buildSaleDetails() {
        PdfPTable table = new PdfPTable(TABLE_COLUMN_SIZE);
        table.setWidthPercentage(100f);
        table.setWidths(new float[]{3f, 3.5f, 1.0f, 2.5f, 3.5f});
        table.setSpacingBefore(10);

        writeTableHeader(table);
        writeTableData(table);

        document.add(table);
    }

    private void buildTotal() {
        BigDecimal total = SaleDetailUtil.sumTotalAmount(sale.getSaleDetails());
        Phrase paymentTypePhrase = new Phrase();
        paymentTypePhrase.addAll(List.of(
                new Chunk("Total:  ", getBoldFont(16)),
                new Chunk(String.format("%,.2f", total), getBasicFont(FONT_SIZE_VALUE)),
                new Chunk(" MXN", getBasicFont(FONT_SIZE_VALUE)))
        );
        Paragraph totalParagraph = new Paragraph(paymentTypePhrase);
        totalParagraph.setAlignment(Paragraph.ALIGN_RIGHT);

        document.add(totalParagraph);
    }

    private void buildFooter() {
        this.buildNewLines(10);
        Phrase phrase = new Phrase("cert: " + sale.getCert(), getBasicFont(8));
        Paragraph paragraph = new Paragraph(phrase);
        paragraph.setAlignment(Paragraph.ALIGN_BOTTOM);

        document.add(paragraph);
    }

    private void buildNewLines(int lines) {
        for (int i = 0; i < lines; i++) {
            document.add(Chunk.NEWLINE);
        }
    }

    private void writeTableHeader(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.BLACK);

        cell.setPhrase(new Phrase("Producto", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Descripción", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Qty", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Precio Unitario", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Precio Total", font));
        table.addCell(cell);
    }

    private void writeTableData(PdfPTable table) {
        sale.getSaleDetails().forEach(saleDetail -> {
            table.addCell(saleDetail.getProductStore().getProduct().getName());
            table.addCell(saleDetail.getDescription() + "\n"
                    + getProductTags(saleDetail));
            table.addCell(String.valueOf(saleDetail.getQuantity()));
            table.addCell(String.format("%,.2f", saleDetail.getPrice()));
            table.addCell(String.format("%,.2f", saleDetail.getTotalPrice()));
        });
    }

    private Font getBoldFont(int size) {
        Font font = FontFactory.getFont(FontFactory.COURIER);
        font.setStyle("bold");
        font.setSize(size);

        return font;
    }

    private Font getBasicFont(int size) {
        Font font = new Font();
        font.setSize(size);

        return font;
    }

    private String getProductTags(SaleDetail saleDetail) {
        List<ProductTag> productTags = saleDetail.getProductTags()
                .stream()
                .collect(Collectors.toList());
        if (productTags.get(0).getIsAutogenerated() == true) {
            return "";
        } else {
            return productTags.stream()
                    .map(ProductTag::getSerialNumber)
                    .toList()
                    .toString()
                    .replace("[", "")
                    .replace("]", "")
                    .trim();
        }
    }

    private String getStoreDetails() {
        Store store = sale.getStore();
        return String.format(""" 
                %s
                %s
                Correo: %s
                Whatsapp: %s""", store.getName(), store.getAddress(), store.getEmail(), store.getPhone()
        );
    }
}
