package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.entity.Sale;
import com.jupiterhqs.jupiterapiv2.entity.SaleDetail;
import com.jupiterhqs.jupiterapiv2.model.SaleDetailDto;

import java.math.BigDecimal;
import java.util.Set;

public class SaleDetailUtil {

    private SaleDetailUtil() {
    }

    public static SaleDetail create(SaleDetailDto saleDetailDto, Sale sale, ProductStore productStore) {
        return SaleDetail.builder()
                .description(saleDetailDto.getDescription())
                .quantity(saleDetailDto.getQuantity())
                .price(saleDetailDto.getPrice())
                .totalPrice(saleDetailDto.getPrice().multiply(new BigDecimal(saleDetailDto.getQuantity())))
                .productStore(productStore)
                .sale(sale)
                .build();
    }

    public static SaleDetail create(long saleDetailId) {
        return SaleDetail.builder()
                .saleDetailId(saleDetailId)
                .build();
    }

    public static Long sumQuantity(Set<SaleDetail> saleDetails) {
        return saleDetails.stream()
                .map(SaleDetail::getQuantity)
                .reduce(0L, (quantity, total) -> total + quantity);
    }

    public static BigDecimal sumTotalAmount(Set<SaleDetail> saleDetails) {
        return saleDetails.stream()
                .map(SaleDetail::getTotalPrice)
                .reduce(BigDecimal.ZERO, (amount, total) -> total.add(amount));
    }
}
