package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.*;
import com.jupiterhqs.jupiterapiv2.model.ProductCodeDto;

public class ProductCodeUtil {

    private ProductCodeUtil() {
    }

    public static ProductCode create(long productCodeId) {
        return ProductCode.builder()
                .productCodeId(productCodeId)
                .build();
    }

    public static ProductCode create(ProductCodeDto productCodeDto) {
    	if (productCodeDto.getProductCodeId() == null) {
            return ProductCode.builder()
                    .name(productCodeDto.getName())
                    .description(productCodeDto.getDescription())
                    .build();
    	}
    	
        return ProductCode.builder()
        		.productCodeId(productCodeDto.getProductCodeId())
                .name(productCodeDto.getName())
                .description(productCodeDto.getDescription())
                .build();

    }
    
    public static ProductCode create(String name, String description) {
        return ProductCode.builder()
                .name(name)
                .description(description)
                .build();
    }
}
