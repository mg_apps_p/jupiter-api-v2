package com.jupiterhqs.jupiterapiv2.util;


import com.jupiterhqs.jupiterapiv2.entity.PaymentType;
import com.jupiterhqs.jupiterapiv2.model.PaymentTypeDto;

public class PaymentTypeUtil {

    private PaymentTypeUtil() {}

    public static PaymentType create(Long paymentTypeId, PaymentTypeDto paymentTypeDto) {
        return PaymentType.builder()
                .paymentTypeId(paymentTypeId)
                .name(paymentTypeDto.getName())
                .description(paymentTypeDto.getDescription())
                .build();
    }

    public static PaymentType create(String name, String description) {
        return PaymentType.builder()
                .name(name)
                .description(description)
                .build();
    }
}
