package com.jupiterhqs.jupiterapiv2.util.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.jupiterhqs.jupiterapiv2.entity.Product;

import java.io.IOException;
import java.util.*;

public class ProductProductCodeSerializer extends StdSerializer<List<Product>> {

    private static final long serialVersionUID = -7788859858267288992L;

	public ProductProductCodeSerializer() {
        super((Class<List<Product>>) null);
    }

    public ProductProductCodeSerializer(Class<List<Product>> t) {
        super(t);
    }

    @Override
    public void serialize(List<Product> products, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        List<Long> ids = new ArrayList<>();
        for (Product product : products) {
            ids.add(product.getProductId());
        }
        jsonGenerator.writeObject(ids);
    }
}
