package com.jupiterhqs.jupiterapiv2.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.jupiterhqs.jupiterapiv2.entity.*;
import com.jupiterhqs.jupiterapiv2.model.ProductDto;
import com.jupiterhqs.jupiterapiv2.model.ProductStoreDto;
import com.jupiterhqs.jupiterapiv2.model.ProductStoreId;
import com.jupiterhqs.jupiterapiv2.model.ProductTransferDetailDto;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDetailDto;
import com.jupiterhqs.jupiterapiv2.model.SaleDetailDto;
import com.jupiterhqs.jupiterapiv2.model.StoreDto;

public class ProductStoreUtil {

    private ProductStoreUtil() {
    }
    
    public static ProductStore create(long productStoreId) {
    	return ProductStore.builder().productStoreId(productStoreId).build();
    }
    
    public static ProductStore create(ProductStoreDto productStoreDto) {
    	return create(productStoreDto.getStore(), productStoreDto.getProduct());
    }

    public static ProductStore create(StoreDto store, ProductDto productDto) {
        return ProductStore.builder()
                .store(StoreUtil.create(store.getStoreId()))
                .product(ProductUtil.create(productDto.getProductId()))
                .build();
    }
    
    public static List<ProductStore> create(Product product, List<ProductStoreDto> productStores) {
    	return productStores.stream().map(productStore -> ProductStoreUtil.create(product, StoreUtil.create(productStore.getStore().getStoreId())))
    			.collect(Collectors.toList());
    }

    public static ProductStore create(Product product, Store store) {
        return ProductStore.builder()
                .product(product)
                .store(store)
                .quantity(0L)
                .price(BigDecimal.ZERO)
                .build();
    }
    
    public static List<Long> create(Set<SaleDetailDto> saleDetailDtos) {
        return saleDetailDtos.stream()
                .map(saleDetailDto -> saleDetailDto.getProductStore().getProductStoreId())
                .collect(Collectors.toList());
    }
    
}
