package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Expense;
import com.jupiterhqs.jupiterapiv2.model.*;
import com.jupiterhqs.jupiterapiv2.util.constants.StatusIdConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

public class ExpenseUtil {

    protected static final Logger log = LoggerFactory.getLogger(ExpenseUtil.class);

    private ExpenseUtil() {
    }

    public static Expense create(ExpenseDto expenseDto) {
        return Expense.builder()
                .name(expenseDto.getName())
                .amount(expenseDto.getAmount())
                .expenseCategory(ExpenseCategoryUtil.create(
                        expenseDto.getExpenseCategory().getExpenseCategoryId(), expenseDto.getExpenseCategory()))
                .paymentType(PaymentTypeUtil.create(
                        expenseDto.getPaymentType().getPaymentTypeId(), expenseDto.getPaymentType()))
                .status(StatusUtil.create(StatusIdConstants.CREATED))
                .store(StoreUtil.create(expenseDto.getStore().getStoreId()))
                .build();
    }

    public static Expense create(long expenseId, ExpenseDto expenseDto) {
        return Expense.builder()
                .expenseId(expenseId)
                .name(expenseDto.getName())
                .amount(expenseDto.getAmount())
                .build();
    }

    public static void update(Expense expenseToUpdate, ExpenseDto expenseDto) {
        ExpenseCategoryDto expenseCategoryDto = expenseDto.getExpenseCategory();
        StoreDto storeDto = expenseDto.getStore();
        StatusDto statusDto = expenseDto.getStatus();
        PaymentTypeDto paymentTypeDto = expenseDto.getPaymentType();
        expenseToUpdate.setName(expenseDto.getName());
        expenseToUpdate.setAmount(expenseToUpdate.getAmount());
        expenseToUpdate.setStatus(StatusUtil.create(statusDto.getStatusId()));
        expenseToUpdate.setExpenseCategory(ExpenseCategoryUtil.create(
                expenseCategoryDto.getExpenseCategoryId(),
                expenseCategoryDto)
        );
        expenseToUpdate.setPaymentType(PaymentTypeUtil.create(paymentTypeDto.getPaymentTypeId(), paymentTypeDto));
        expenseToUpdate.setStore(StoreUtil.create(storeDto.getStoreId()));
    }

    public static BigDecimal sumAmount(List<Expense> expenses) {
        return expenses.stream()
                .map(Expense::getAmount)
                .reduce(BigDecimal.ZERO, (value, acc) -> acc.add(value));
    }
}
