package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.model.BalanceResponse;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class BalanceUtil {

    public static int MAX_HOUR = 23;
    public static int MIN_HOUR = 0;
    public static int MAX_MINUTE = 59;
    public static int MIN_MINUTE = 0;

    private BalanceUtil() {
    }

    public static BalanceResponse create(BigDecimal balance,
                                         BigDecimal expensesAmount,
                                         BigDecimal salesAmount,
                                         LocalDateTime from,
                                         LocalDateTime to) {
        return BalanceResponse.builder()
                .balance(balance)
                .expensesAmount(expensesAmount)
                .salesAmount(salesAmount)
                .from(from)
                .to(to)
                .build();
    }

    public static LocalDateTime getTodayDateWithCustomTime(int hour, int minute) {
        LocalDate currentDate = LocalDate.now();
        LocalTime customTime = LocalTime.of(hour, minute);

        return buildDate(currentDate, customTime);
    }

    public static LocalDateTime getCustomDateWithTime(String date, int hour, int minute) {
        LocalDate localDate = LocalDate.parse(date);
        LocalTime customTime = LocalTime.of(hour, minute);

        return buildDate(localDate, customTime);

    }

    private static LocalDateTime buildDate(LocalDate localDate, LocalTime localTime) {
        return LocalDateTime.of(localDate, localTime);
    }
}
