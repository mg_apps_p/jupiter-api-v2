package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Investment;
import com.jupiterhqs.jupiterapiv2.model.InvestmentDto;
import com.jupiterhqs.jupiterapiv2.model.PaymentTypeDto;

import java.util.ArrayList;
import java.util.List;

public class InvestmentUtil {

    private InvestmentUtil(){}

    public static List<Investment> create(List<InvestmentDto> investmentDtos) {
        List<Investment> investments = new ArrayList<>();
        for (InvestmentDto investmentDTO : investmentDtos) {
            PaymentTypeDto paymentTypeDTO = investmentDTO.getPaymentType();
            Investment investment = Investment.builder()
                    .name(investmentDTO.getName())
                    .amount(investmentDTO.getAmount())
                    .paymentType(PaymentTypeUtil.create(paymentTypeDTO.getPaymentTypeId(), paymentTypeDTO))
                    .build();
            investments.add(investment);
        }

        return investments;
    }
}
