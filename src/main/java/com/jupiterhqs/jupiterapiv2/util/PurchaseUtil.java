package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Purchase;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDetailDto;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDto;
import com.jupiterhqs.jupiterapiv2.model.StoreDto;
import com.jupiterhqs.jupiterapiv2.model.VendorDto;
import com.jupiterhqs.jupiterapiv2.util.constants.PaymentStatusIds;
import com.jupiterhqs.jupiterapiv2.util.encode.ShaHashCreator;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class PurchaseUtil {

    private static int LAST_CHARS = 15;

    private PurchaseUtil() {
    }

    public static Purchase create(long purchaseId) {
        return Purchase.builder()
                .purchaseId(purchaseId)
                .build();
    }

    public static Purchase create(PurchaseDto purchaseDto) {
        StoreDto storeDto = purchaseDto.getStore();
        BigDecimal amount = getAmount(purchaseDto.getPurchaseDetails());
        String createdDate = purchaseDto.getCreated();
        return Purchase.builder()
                .amount(amount)
                .partialPaid(getPartialPaid(purchaseDto, amount))
                .status(StatusUtil.create(1L))
                .paymentStatus(PaymentStatusUtil.create(purchaseDto.getPaymentStatus()))
                .store(StoreUtil.create(storeDto.getStoreId(), storeDto))
                .created(createdDate != null ? DateUtil.getLocalDateTime(createdDate) : null)
                .build();
    }

    public static String buildReference(String input) throws NoSuchAlgorithmException {
        String hash = ShaHashCreator.create512Algorithm(input);

        return hash.substring(hash.length() - LAST_CHARS).toUpperCase();
    }
   
  
    private static BigDecimal getAmount(List<PurchaseDetailDto> purchaseDetails) {
        return purchaseDetails.stream()
                .map(purchaseDetailDto -> purchaseDetailDto.getPrice().multiply(new BigDecimal(purchaseDetailDto
                        .getQuantity())))
                .reduce(BigDecimal.ZERO, (currentValue, accumulator) -> accumulator.add(currentValue));
    }

    private static BigDecimal getPartialPaid(PurchaseDto purchaseDto, BigDecimal amount) {
        BigDecimal partialPaid;
        long paymentStatusId = purchaseDto.getPaymentStatus().getPaymentStatusId();
        if (paymentStatusId == PaymentStatusIds.PAID) {
            partialPaid = amount;
        } else if (paymentStatusId == PaymentStatusIds.PARTIAL) {
            partialPaid = purchaseDto.getPartialPaid();
        } else {
            partialPaid = BigDecimal.ZERO;
        }

        return partialPaid;
    }
    

}
