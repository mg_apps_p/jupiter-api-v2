package com.jupiterhqs.jupiterapiv2.util;

import java.time.*;

public class DateUtil {
    private DateUtil() {
    }

    public static LocalDateTime getLocalDateTime(String isoStringDate) {
        return LocalDateTime.ofInstant(Instant.parse(isoStringDate), ZoneId.systemDefault());
    }
}
