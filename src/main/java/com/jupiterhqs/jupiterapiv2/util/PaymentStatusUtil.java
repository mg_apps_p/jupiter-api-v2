package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.PaymentStatus;
import com.jupiterhqs.jupiterapiv2.model.PaymentStatusDto;


public class PaymentStatusUtil {

    private PaymentStatusUtil() {
    }

    public static PaymentStatus create(PaymentStatusDto paymentStatusDto) {
        return PaymentStatus.builder()
                .paymentStatusId(paymentStatusDto.getPaymentStatusId())
                .name(paymentStatusDto.getName())
                .build();
    }

    public static PaymentStatus create(String name) {
        return PaymentStatus.builder()
                .name(name)
                .build();
    }

    public static PaymentStatus create(long statusId) {
        return PaymentStatus.builder()
                .paymentStatusId(statusId)
                .build();
    }
}
