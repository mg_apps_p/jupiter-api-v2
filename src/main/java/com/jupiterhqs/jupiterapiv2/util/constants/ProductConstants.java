package com.jupiterhqs.jupiterapiv2.util.constants;

public class ProductConstants {

    private ProductConstants(){}

    public final static int MIN_QUANTITY = 1;
}
