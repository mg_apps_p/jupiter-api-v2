package com.jupiterhqs.jupiterapiv2.util.constants;

public class StatusIdConstants {
    private StatusIdConstants() {
    }

    public final static int CREATED = 1;
    public final static int COMPLETED = 2;
    public final static int CANCEL = 3;
    public final static int PROCESSED = 4;

}
