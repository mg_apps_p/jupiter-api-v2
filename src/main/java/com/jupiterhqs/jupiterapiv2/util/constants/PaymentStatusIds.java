package com.jupiterhqs.jupiterapiv2.util.constants;

public class PaymentStatusIds {

    private PaymentStatusIds() {}

    public static long PAID = 1;
    public static long UNPAID = 2;
    public static long PARTIAL = 3;
}
