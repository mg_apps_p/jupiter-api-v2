package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.PurchasePayment;
import com.jupiterhqs.jupiterapiv2.model.PaymentTypeDto;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDto;
import com.jupiterhqs.jupiterapiv2.model.PurchasePaymentDto;

public class PurchasePaymentUtil {

    private PurchasePaymentUtil() {
    }

    public static PurchasePayment create(PurchasePaymentDto purchasePaymentDto) {
        String createdDate = purchasePaymentDto.getCreated();
        PaymentTypeDto paymentTypeDto = purchasePaymentDto.getPaymentType();
        PurchaseDto purchaseDto = purchasePaymentDto.getPurchase();
        return PurchasePayment.builder()
                .amount(purchasePaymentDto.getAmount())
                .note(purchasePaymentDto.getNote())
                .paymentType(PaymentTypeUtil.create(paymentTypeDto.getPaymentTypeId(), paymentTypeDto))
                .created(createdDate != null ? DateUtil.getLocalDateTime(createdDate) : null)
                .purchase(PurchaseUtil.create(purchaseDto.getPurchaseId()))
                .build();
    }
}
