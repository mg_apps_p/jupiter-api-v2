package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.*;
import com.jupiterhqs.jupiterapiv2.model.ProductDto;

import java.math.BigDecimal;

public class ProductUtil {

    private ProductUtil() {
    }

    public static Product create(long productId) {
        return Product.builder()
                .productId(productId)
                .build();
    }

    public static Product create(ProductDto productDto) {
        return Product.builder()
                .name(productDto.getName())
                .description(productDto.getDescription())
                .suggestedPrice(productDto.getSuggestedPrice())
                .averageSalePrice(BigDecimal.ZERO)
                .averagePurchasePrice(BigDecimal.ZERO)
                .subcategory(SubcategoryUtil.create(productDto.getSubcategory().getSubcategoryId()))
                .brand(BrandUtil.create(productDto.getBrand().getBrandId()))
                .build();
    }

    public static Product create(Long productId, ProductDto productDto) {
        return Product.builder()
                .productId(productId)
                .name(productDto.getName())
                .description(productDto.getDescription())
                .suggestedPrice(productDto.getSuggestedPrice())
                .brand(BrandUtil.create(productDto.getBrand().getBrandId()))
                .build();
    }

    public static Product create(String name,
                                 String description,
                                 Long currentQuantity,
                                 BigDecimal suggestedPrice,
                                 BigDecimal averageSalePrice,
                                 BigDecimal averagePurchasePrice,
                                 Subcategory subcategory,
                                 Brand brand,
                                 ProductCode productCode,
                                 Store store) {
        return Product.builder()
                .name(name)
                .description(description)
                .suggestedPrice(suggestedPrice)
                .averageSalePrice(averageSalePrice)
                .averagePurchasePrice(averagePurchasePrice)
                .subcategory(subcategory)
                .brand(brand)
                .productCode(productCode)
                .build();

    }
    
    public static void update(Product productSaved, ProductDto productToUpdate) {
    	productSaved.setName(productToUpdate.getName());	
    	productSaved.setDescription(productToUpdate.getDescription());
    	productSaved.setSuggestedPrice(productToUpdate.getSuggestedPrice());
    	productSaved.setSku(productToUpdate.getSku());
    	productSaved.setBrand(BrandUtil.create(productToUpdate.getBrand().getBrandId()));
    }
}
