package com.jupiterhqs.jupiterapiv2.util;

import java.util.Set;
import java.util.stream.Collectors;

import com.jupiterhqs.jupiterapiv2.entity.Store;
import com.jupiterhqs.jupiterapiv2.model.StoreDto;

public class StoreUtil {

    private StoreUtil() {
    }

    public static Store create(long storeId) {
        return Store.builder()
                .storeId(storeId)
                .build();
    }

    public static Store create(StoreDto storeDto) {
        return Store.builder()
                .name(storeDto.getName())
                .address(storeDto.getAddress())
                .email(storeDto.getEmail())
                .phone(storeDto.getPhone())
                .build();
    }

    public static Store create(Long storeId, StoreDto storeDto) {
        return Store.builder()
                .storeId(storeId)
                .name(storeDto.getName())
                .address(storeDto.getAddress())
                .email(storeDto.getEmail())
                .phone(storeDto.getPhone())
                .build();
    }

    public static Store create(String name, String address, String email, String phone) {
        return Store.builder()
                .name(name)
                .address(address)
                .email(email)
                .phone(phone)
                .build();
    }

	public static Set<Store> create(Set<StoreDto> storeDtos) {
		return storeDtos.stream()
				.map(storeDto -> create(storeDto.getStoreId(), storeDto)).collect(Collectors.toSet());
	}
}
