package com.jupiterhqs.jupiterapiv2.util.enums;

public enum Role {
    ADMIN,
    USER
}
