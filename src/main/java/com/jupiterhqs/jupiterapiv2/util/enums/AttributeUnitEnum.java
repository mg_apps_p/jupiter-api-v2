package com.jupiterhqs.jupiterapiv2.util.enums;

public enum AttributeUnitEnum {
    DYNAMIC,
    WITHOUT_UNIT,
    MB,
    GB,
    TB,
    PB,
    GHZ,
    CM,
    M,
    IN
}
