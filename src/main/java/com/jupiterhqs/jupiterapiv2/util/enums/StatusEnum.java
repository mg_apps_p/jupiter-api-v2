package com.jupiterhqs.jupiterapiv2.util.enums;

public enum StatusEnum {
    CREATED (1),
    COMPLETED(2),
    CANCEL(3),
    PROCESSED(4);
    private int value;

    private StatusEnum(int value) {
        this.value = value;
    }
}
