package com.jupiterhqs.jupiterapiv2.util.enums;

public enum AttributeTypeEnum {
    STRING,
    NUMBER,
    BOOLEAN,
    CATALOG
}
