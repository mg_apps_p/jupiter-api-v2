package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Category;
import com.jupiterhqs.jupiterapiv2.model.CategoryDto;

public class CategoryUtil {
	
    public static Category create(CategoryDto categoryDTO){
        if (categoryDTO == null){
            throw new RuntimeException("SubcategoryDTO can't be null");
        }

        return Category.builder()
                .name(categoryDTO.getName())
                .build();
    }

    public static Category create(Long categoryId) {
        return Category.builder()
                .categoryId(categoryId)
                .build();
    }
}
