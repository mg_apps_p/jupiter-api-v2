package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.AttributeValue;
import com.jupiterhqs.jupiterapiv2.model.AttributeValueDto;

public class AttributeValueUtil {

    private AttributeValueUtil() {
    }

    public static AttributeValue create(AttributeValueDto attributeValueDto) {
        return AttributeValue.builder()
                .value(attributeValueDto.getValue())
                .build();
    }

    public static AttributeValue create(Long attributeValueId) {
        return AttributeValue.builder()
                .attributeValueId(attributeValueId)
                .build();
    }

    public static AttributeValue create(String value) {
        return AttributeValue.builder()
                .value(value)
                .build();
    }
}
