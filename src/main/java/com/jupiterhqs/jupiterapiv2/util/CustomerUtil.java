package com.jupiterhqs.jupiterapiv2.util;


import com.jupiterhqs.jupiterapiv2.entity.Customer;
import com.jupiterhqs.jupiterapiv2.model.CustomerDto;

public class CustomerUtil {


    public static Customer create(CustomerDto customerDto) {	
        return Customer.builder()
                .name(customerDto.getName())
                .phone(customerDto.getPhone())
                .email(customerDto.getEmail())
                .build();
    }
    
    public static Customer create(long customerId) {	
      	return Customer.builder()
    			.customerId(customerId)
    			.build();
    }
    
}

    
