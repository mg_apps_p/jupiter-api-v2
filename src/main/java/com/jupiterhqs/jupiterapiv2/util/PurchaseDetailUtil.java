package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.entity.Purchase;
import com.jupiterhqs.jupiterapiv2.entity.PurchaseDetail;
import com.jupiterhqs.jupiterapiv2.model.ProductDto;
import com.jupiterhqs.jupiterapiv2.model.ProductStoreDto;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDetailDto;
import com.jupiterhqs.jupiterapiv2.util.constants.PaymentStatusIds;

import java.math.BigDecimal;


public class PurchaseDetailUtil {

    private PurchaseDetailUtil() {
    }

    public static PurchaseDetail create(PurchaseDetailDto purchaseDetailDto, Purchase purchase, ProductStore productStore) {
        BigDecimal totalPrice = purchaseDetailDto.getPrice()
                .multiply(BigDecimal.valueOf(purchaseDetailDto.getQuantity()));

        return PurchaseDetail.builder()
                .quantity(purchaseDetailDto.getQuantity())
                .price(purchaseDetailDto.getPrice())
                .totalPrice(totalPrice)
                .productStore(productStore)
                .purchase(purchase)
                .build();
    }


}
