package com.jupiterhqs.jupiterapiv2.util;

import com.jupiterhqs.jupiterapiv2.entity.Product;
import com.jupiterhqs.jupiterapiv2.entity.RelatedProduct;
import com.jupiterhqs.jupiterapiv2.model.RelatedProductDto;

import java.util.List;
import java.util.stream.Collectors;

public class RelatedProductUtil {

    private RelatedProductUtil() {
    }

    public static List<RelatedProduct> create(RelatedProductDto relatedProductDto) {
        return relatedProductDto.getRelatedProducts().stream()
                .map(relatedProduct -> {
                    return create(relatedProduct, relatedProductDto.getProduct());
                })
                .collect(Collectors.toList());

    }

    public static RelatedProduct create(Product relatedProduct, Product product) {
        return RelatedProduct.builder()
                .product(product)
                .relatedProduct(relatedProduct)
                .build();
    }
}
