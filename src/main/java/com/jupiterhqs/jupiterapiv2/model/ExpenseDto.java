package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseDto {

    private long expenseId;

    @Digits( integer = 8, fraction = 2 )
    @Positive
    private BigDecimal amount;

    @NotBlank
    @NotEmpty
    @Size(min = 4, max = 100)
    private String name;

    @Valid
    private ExpenseCategoryDto expenseCategory;

    @Valid
    private PaymentTypeDto paymentType;

    @Valid
    private StoreDto store;

    private StatusDto status;

    private String created;


}
