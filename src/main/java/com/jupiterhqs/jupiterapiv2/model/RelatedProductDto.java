package com.jupiterhqs.jupiterapiv2.model;

import com.jupiterhqs.jupiterapiv2.entity.Product;
import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RelatedProductDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1431097172503750805L;

    @NotNull
    private Product product;
    
    @NotEmpty
    private List<Product> relatedProducts;
}
