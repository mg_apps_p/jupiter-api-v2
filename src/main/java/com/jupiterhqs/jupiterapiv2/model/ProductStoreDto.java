package com.jupiterhqs.jupiterapiv2.model;

import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

import jakarta.validation.Valid;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductStoreDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1431097172503750805L;
    
    private Long productStoreId;

    @Valid
    private ProductDto product;
    
    @Valid
    private StoreDto store;
   
    private Long quantity;
    private BigDecimal price;
}
