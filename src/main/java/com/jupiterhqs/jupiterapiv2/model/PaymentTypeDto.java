package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentTypeDto implements Serializable {

    private static final long serialVersionUID = -4532940685344234333L;

    private Long paymentTypeId;

    @NotBlank
    @NotEmpty
    @Size(min = 5, max = 50)
    private String name;

    @NotBlank
    @NotEmpty
    @Size(min = 1, max = 255)
    private String description;
}
