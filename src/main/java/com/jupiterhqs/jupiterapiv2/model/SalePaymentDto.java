package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SalePaymentDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1431097172503750805L;

    private Long salePaymentId;

    @Valid
    private SaleDto sale;

    @Valid
    private PaymentTypeDto paymentType;

    @Digits(integer = 6, fraction = 2)
    private BigDecimal amount;

    @NotNull
    @NotEmpty
    private String created;

    private String note;
}
