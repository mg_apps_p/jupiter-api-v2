package com.jupiterhqs.jupiterapiv2.model;

import com.jupiterhqs.jupiterapiv2.util.enums.AttributeTypeEnum;
import com.jupiterhqs.jupiterapiv2.util.enums.AttributeUnitEnum;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttributeDto {

    private long attributeId;

    @NotBlank
    @NotEmpty
    @Size(min = 2)
    private String name;

    @Valid
    private AttributeTypeEnum type;

    private AttributeUnitEnum unit;

    private String value;
}
