package com.jupiterhqs.jupiterapiv2.model;


import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SaleDetailDto implements Serializable {

    private static final long serialVersionUID = 75856131563522729L;

    private Long saleDetailId;

    @Size(min = 3, max = 255)
    private String description;

    private List<ProductTagDto> productTags;

    @NotNull
    @Min(value = 1)
    private Long quantity;

    @Digits(integer = 6, fraction = 2)
    @PositiveOrZero
    private BigDecimal price;

    @Valid
    private ProductStoreDto productStore;
}
