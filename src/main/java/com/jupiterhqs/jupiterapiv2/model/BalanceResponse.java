package com.jupiterhqs.jupiterapiv2.model;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BalanceResponse {
    private BigDecimal balance;
    private BigDecimal expensesAmount;
    private BigDecimal salesAmount;
    private LocalDateTime from;
    private LocalDateTime to;
}
