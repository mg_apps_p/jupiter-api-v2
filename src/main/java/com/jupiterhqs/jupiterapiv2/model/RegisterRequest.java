package com.jupiterhqs.jupiterapiv2.model;

import java.util.Set;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {

    @NotEmpty
    @NotBlank
    private String firstname;

    @NotEmpty
    @NotBlank
    private String lastname;

    @NotEmpty
    @NotBlank
    private String email;

    @NotEmpty
    @NotBlank
    private String password;
    
    @Valid
    private Set<StoreDto> stores;
}
