package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.*;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductTagDto {

    private long productTagId;

    @NotEmpty
    @NotBlank
    private String serialNumber;

    private int groupQuantity;

    private int groupSequence;
}
