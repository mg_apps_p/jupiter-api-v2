package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.*;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseCategoryDto {

    private long expenseCategoryId;

    @NotBlank
    @NotEmpty
    @Size(min = 4, max = 100)
    private String name;
}
