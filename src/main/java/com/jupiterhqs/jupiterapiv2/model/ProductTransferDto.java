package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import lombok.*;

import java.io.Serializable;
import java.util.Set;


@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductTransferDto implements Serializable {

    private static final long serialVersionUID = 3280109732062467631L;
    
    private Long productTransferId;

    @Valid
    private Set<ProductTransferDetailDto> productTransferDetails;

    private String description;

    
    private UserDto user;
}
