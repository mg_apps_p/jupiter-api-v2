package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationRequest {

	@NotEmpty
	@NotBlank
	private String email;

	@NotEmpty
	@NotBlank
	private String password;
}
