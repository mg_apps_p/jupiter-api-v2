package com.jupiterhqs.jupiterapiv2.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BrandDto {

    private Long brandId;

    @NotEmpty
    @NotBlank
    @Size(min = 2, max = 50)
    private String name;
    
    @JsonBackReference("product_brands_id_ref")
    private ProductDto product;

}
