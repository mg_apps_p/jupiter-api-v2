package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductAttributeDto {
	
	Long productAttributeId;

    @Valid
    private ProductDto product;

    @Valid
    private AttributeDto attribute;

    @NotBlank
    @NotEmpty
    @Size(min = 2)
    private String value;
    
    private String created;
}
