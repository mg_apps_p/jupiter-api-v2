package com.jupiterhqs.jupiterapiv2.model;

import com.jupiterhqs.jupiterapiv2.entity.Attribute;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttributeValueDto {

    @NotEmpty
    @NotBlank
    private String value;

    @Valid
    private Attribute attribute;
}
