package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StoreDto implements Serializable {

    private static final long serialVersionUID = -2279261770549599298L;

    private long storeId;

    @NotBlank
    @NotEmpty
    @Size(min = 3, max = 50)
    private String name;

    @NotBlank
    @NotEmpty
    @Size(min = 5, max = 255)
    private String address;

    @NotBlank
    @NotEmpty
    @Email
    private String email;

    @NotBlank
    @NotEmpty
    private String phone;
}
