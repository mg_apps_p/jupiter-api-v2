package com.jupiterhqs.jupiterapiv2.model;


import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serializable;


@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductTransferDetailDto implements Serializable {

    private static final long serialVersionUID = 75856131563522729L;

    private Long productTransferDetailId;

    @NotNull
    @Min(value = 1)
    private Long quantity;

    @Valid
    private ProductStoreDto productStoreOrigin;
    
    @Valid
    private ProductStoreDto productStoreDestination;
}
