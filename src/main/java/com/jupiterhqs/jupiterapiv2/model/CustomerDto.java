package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;


@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {

    private Long customerId;

    @Size(min = 2, max = 50)
    @NotBlank
    @NotEmpty
    private String name;
    private String phone;
    private String email;
    private String rfc;
}
