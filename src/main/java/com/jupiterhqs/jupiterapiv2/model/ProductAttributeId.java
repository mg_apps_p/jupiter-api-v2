package com.jupiterhqs.jupiterapiv2.model;

import com.jupiterhqs.jupiterapiv2.entity.Attribute;
import com.jupiterhqs.jupiterapiv2.entity.Product;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductAttributeId implements Serializable {

    @Serial
    private static final long serialVersionUID = 1431097172503750805L;

    private Attribute attribute;
    
    private Product product;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
