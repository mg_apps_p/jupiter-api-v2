package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SaleDto implements Serializable {

    private static final long serialVersionUID = 3280109732062467631L;
    
    private Long saleId;

    @Valid
    private Set<SaleDetailDto> saleDetails;

    private String description;

    @Valid
    private UserDto user;

    @Valid
    private PaymentTypeDto paymentType;

    @Valid
    private CustomerDto customer;

    @Valid
    private StoreDto store;
    
    @Valid
    private PaymentStatusDto paymentStatus;
    
    @Digits(integer = 6, fraction = 2)
    private BigDecimal partialPaid;
}
