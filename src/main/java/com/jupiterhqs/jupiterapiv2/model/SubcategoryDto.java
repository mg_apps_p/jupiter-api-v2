package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubcategoryDto implements Serializable {

    private static final long serialVersionUID = 6143531116632046277L;

    private Long subcategoryId;

    @NotBlank
    @NotEmpty
    @Size(min = 3, max = 50)
    private String name;
    
    
    @JsonBackReference(value = "subcategory_product_id_ref")
    private ProductDto product;
}
