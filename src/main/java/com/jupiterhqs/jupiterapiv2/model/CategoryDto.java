package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.io.Serializable;
import java.util.Set;


@Setter
@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto implements Serializable {

    private static final long serialVersionUID = -1362258531757232654L;

    private Long categoryId;

    @NotBlank
    @NotEmpty
    @Size(min = 3, max = 50)
    private String name;

    @Valid
    private Set<SubcategoryDto> subcategories;
}
