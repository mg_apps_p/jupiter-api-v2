package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvestmentDto implements Serializable {


    private static final long serialVersionUID = -403455615148817133L;

    @NotBlank
    @NotEmpty
    @Size(min = 4, max = 50)
    private String name;

    @NotBlank
    @NotEmpty
    @Size(min = 1, max = 255)
    private String description;

    @Digits( integer = 7, fraction = 2)
    @Positive
    private BigDecimal amount;

    @Valid
    private PaymentTypeDto paymentType;
}
