package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.*;
import lombok.NoArgsConstructor;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VendorDto {

    private Long vendorId;

    @NotEmpty
    @NotBlank
    @Size(min = 3, max = 50)
    private String name;

    @NotEmpty
    @NotBlank
    @Size(min = 5, max = 255)
    private String description;
}
