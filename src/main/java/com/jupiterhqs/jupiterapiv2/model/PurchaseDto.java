package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1431097172503750805L;

    private Long purchaseId;

    private List<PurchaseDetailDto> purchaseDetails;

    @Digits(integer = 6, fraction = 2)
    private BigDecimal partialPaid;

    @Valid
    private VendorDto vendor;

    @Valid
    private PaymentStatusDto paymentStatus;

    @Valid
    private StoreDto store;

    private String created;
}
