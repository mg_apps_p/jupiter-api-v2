package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDetailDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1431097172503750805L;

    private Long purchaseDetailId;

    @NotNull
    @Min(value = 1)
    private Long quantity;

    @Digits(integer = 6, fraction = 2)
    @Positive
    private BigDecimal price;

    @Valid
    private ProductStoreDto productStore;
}
