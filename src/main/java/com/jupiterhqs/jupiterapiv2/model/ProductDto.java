package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto implements Serializable {

	@Serial
	private static final long serialVersionUID = 1431097172503750805L;

	private Long productId;

	@NotBlank
	@NotEmpty
	@Size(min = 2, max = 50)
	private String name;

	@NotBlank
	@NotEmpty
	@Size(min = 1, max = 255)
	private String description;

	@Digits(integer = 6, fraction = 2)
	@Positive
	private BigDecimal suggestedPrice;

	@Valid
	@JsonManagedReference(value = "subcategory_product_id_ref")
	private SubcategoryDto subcategory;

	@Valid
	@JsonManagedReference("product_brands_id_ref")
	private BrandDto brand;

	@Valid
	private List<ProductStoreDto> productStores;

	@Valid
	@JsonManagedReference("product_code_products_ref")
	private ProductCodeDto productCode;

	@Valid
	private List<AttributeDto> attributes;

	private List<ProductAttributeDto> productAttributes;

	private String sku;
	private String code;
	private BigDecimal averageSalePrice;
	private BigDecimal averagePurchasePrice;
}
