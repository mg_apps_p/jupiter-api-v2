package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.*;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements Serializable {

	private static final long serialVersionUID = 3280109784062467631L;

	private long userId;

	@NotEmpty
	@NotBlank
	private String firstname;

	@NotEmpty
	@NotBlank
	private String lastname;


	private String password;

	@NotEmpty
	@NotBlank
	private String email;
}
