package com.jupiterhqs.jupiterapiv2.model;

import jakarta.validation.constraints.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductCodeDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1431097172503750805L;

    private Long productCodeId;

    @NotBlank
    @NotEmpty
    @Size(min = 2, max = 50)
    private String name;

    @NotBlank
    @NotEmpty
    @Size(min = 1, max = 50)
    private String description;
    
    @JsonBackReference("product_code_products_ref")
    private ProductDto product;

}
