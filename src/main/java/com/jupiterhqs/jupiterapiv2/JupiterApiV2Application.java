package com.jupiterhqs.jupiterapiv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JupiterApiV2Application {

	public static void main(String[] args) {
		SpringApplication.run(JupiterApiV2Application.class, args);
	}

}
