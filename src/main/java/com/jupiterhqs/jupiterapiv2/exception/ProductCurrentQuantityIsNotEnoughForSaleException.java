package com.jupiterhqs.jupiterapiv2.exception;

public class ProductCurrentQuantityIsNotEnoughForSaleException extends RuntimeException {
    private static final long serialVersionUID = 1L;

	public ProductCurrentQuantityIsNotEnoughForSaleException(String name, long quantity) {
        super("PRODUCT_CURRENT_QUANTITY_IS_NOT_ENOUGH_FOR_SALE_ERROR: [ " + name + ", " + quantity + " ]");
    }
}
