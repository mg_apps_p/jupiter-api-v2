package com.jupiterhqs.jupiterapiv2.exception;

public class SeedInitConfigExecuteTwiceException extends  RuntimeException{
    private static final long serialVersionUID = -1288691865943257697L;

	public SeedInitConfigExecuteTwiceException() {
        super("Seeds can't execute twice");
    }
}
