package com.jupiterhqs.jupiterapiv2.exception;

public class ProductCurrentQuantityIsNotEnoughForProductTransferException extends RuntimeException {
    private static final long serialVersionUID = 1L;

	public ProductCurrentQuantityIsNotEnoughForProductTransferException(String name, long quantity) {
        super("PRODUCT_CURRENT_QUANTITY_IS_NOT_ENOUGH_FOR_PRODUCT_TRANSFER_ERROR: [ " + name + ", " + quantity + " ]");
    }

}
