package com.jupiterhqs.jupiterapiv2.exception;

public class StoreNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 4957925778141434994L;

	public StoreNotFoundException(String message) {
        super(message);
    }
}
