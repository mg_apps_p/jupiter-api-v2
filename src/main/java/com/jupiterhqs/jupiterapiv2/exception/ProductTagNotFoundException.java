package com.jupiterhqs.jupiterapiv2.exception;

public class ProductTagNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 3462334045852763914L;

	public ProductTagNotFoundException(String message) {
        super(message);
    }
}
