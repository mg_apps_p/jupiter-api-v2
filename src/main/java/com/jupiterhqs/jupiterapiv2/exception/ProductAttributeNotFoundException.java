package com.jupiterhqs.jupiterapiv2.exception;

import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;

public class ProductAttributeNotFoundException extends RuntimeException {
    public ProductAttributeNotFoundException(long productId) {
        super(StringParser.parseWithId(ErrorMessages.PRODUCT_ATTRIBUTE_FIND_BY_ID, productId));
    }
}
