package com.jupiterhqs.jupiterapiv2.exception;

import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;

public class AttributeValueNotFoundException extends RuntimeException {
    public AttributeValueNotFoundException(long attributeValueId) {
        super(StringParser.parseWithId(ErrorMessages.ATTRIBUTE_VALUE_FIND_BY_ID, attributeValueId));
    }
}
