package com.jupiterhqs.jupiterapiv2.exception;

public class VendorNameDuplicateException extends RuntimeException {

    private static final long serialVersionUID = 3504363833575680232L;

	public VendorNameDuplicateException(String name) {

        super("PRODUCT_TYPE_CODE_DUPLICATED: " + name );
    }
}
