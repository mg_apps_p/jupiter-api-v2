package com.jupiterhqs.jupiterapiv2.exception;

public class ExpenseCategoryNotFoundException extends RuntimeException {
    public ExpenseCategoryNotFoundException(String message) {
        super(message);
    }
}
