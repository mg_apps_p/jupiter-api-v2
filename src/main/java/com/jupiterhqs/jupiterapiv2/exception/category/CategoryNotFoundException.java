package com.jupiterhqs.jupiterapiv2.exception.category;

public class CategoryNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
}
