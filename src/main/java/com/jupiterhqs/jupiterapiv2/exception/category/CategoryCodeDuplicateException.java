package com.jupiterhqs.jupiterapiv2.exception.category;

public class CategoryCodeDuplicateException extends RuntimeException {

    private static final long serialVersionUID = 1L;

	public CategoryCodeDuplicateException(String code) {
        super("PRODUCT_TYPE_CODE_DUPLICATED: " + code );
    }
}
