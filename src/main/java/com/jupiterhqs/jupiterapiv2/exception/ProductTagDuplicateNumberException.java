package com.jupiterhqs.jupiterapiv2.exception;

public class ProductTagDuplicateNumberException extends  RuntimeException{
    private static final long serialVersionUID = 1L;

	public ProductTagDuplicateNumberException(String number ) {
        super("PRODUCT_TAG_DUPLICATE_NUMBER_ERROR : ["+number+"]");
    }
}
