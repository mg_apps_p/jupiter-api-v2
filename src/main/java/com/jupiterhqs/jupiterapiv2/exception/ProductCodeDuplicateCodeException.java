package com.jupiterhqs.jupiterapiv2.exception;

public class ProductCodeDuplicateCodeException extends  RuntimeException{
    private static final long serialVersionUID = -8188480459702844505L;

	public ProductCodeDuplicateCodeException(String code ) {
        super("PRODUCT_CODE_DUPLICATE_CODE_ERROR : ["+code+"]");
    }
}
