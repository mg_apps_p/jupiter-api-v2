package com.jupiterhqs.jupiterapiv2.exception;

public class VendorNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 4808676659129483548L;
}
