package com.jupiterhqs.jupiterapiv2.exception;

import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;

public class StatusNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 3569849274716813176L;

	public StatusNotFoundException(long statusId) {
        super(StringParser.parseWithId(ErrorMessages.STATUS_FIND_BY_ID, statusId));
    }
}
