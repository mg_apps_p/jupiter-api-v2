package com.jupiterhqs.jupiterapiv2.exception;

import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;

public class BrandNotFoundException extends RuntimeException {
    public BrandNotFoundException(long id) {
        super(StringParser.parseWithId(ErrorMessages.BRAND_FIND_BY_ID, id));
    }
}
