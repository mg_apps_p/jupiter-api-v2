package com.jupiterhqs.jupiterapiv2.config;


import com.jupiterhqs.jupiterapiv2.entity.*;
import com.jupiterhqs.jupiterapiv2.repository.*;
import com.jupiterhqs.jupiterapiv2.util.*;
import com.jupiterhqs.jupiterapiv2.util.enums.AttributeTypeEnum;
import com.jupiterhqs.jupiterapiv2.util.enums.AttributeUnitEnum;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


@RequiredArgsConstructor
@Component
public class DataSeed {
    private static final Logger log = LoggerFactory.getLogger(DataSeed.class);

    final PaymentTypeRepository paymentTypeRepository;
    final VendorRepository vendorRepository;
    final CustomerRepository customerRepository;
    final CategoryRepository categoryRepository;

    final StoreRepository storeRepository;
    final ExpenseCategoryRepository expenseCategoryRepository;

    final ProductRepository productRepository;
    final ProductCodeRepository productCodeRepository;
    final ProductStoreRepository productStoreRepository;
    final StatusRepository statusRepository;
    final AttributeRepository attributeRepository;
    final BrandRepository brandRepository;
    final PaymentStatusRepository paymentStatusRepository;
    Store store;
    ProductCode productCode;
    Brand brand;
    Product product;
    Map<String, Attribute> attributes;


    @Transactional
    public Boolean seeds() {
        try {
            log.info("Executing seeds...");
            initStore();
            initPaymentType();
            initVendor();
            initCustomer();
            initExpenseCategory();
            initStatus();
            initAttributes();
            initCategory();
            initBrand();
            initProductCode();
            initProduct();
            initProductStore();
            initPaymentStatus();
            log.info("Successful database seeds.");
            return true;
        } catch (Exception ex) {
            log.trace("Failed executing seeds.");
            log.trace("SEED_ERROR: " + ex.getMessage());
            throw ex;
        }
    }

    private void initStore() {
        store = storeRepository.save(StoreUtil.create("Demo",
                "Av. Paseo de la Reforma 100 col. Tabacalera CDMX",
                "demo@mail.com",
                "55667788"));
    }

    private void initPaymentType() {
        paymentTypeRepository.save(PaymentTypeUtil.create("Efectivo", "Pesos Mexicanos"));
        paymentTypeRepository.save(PaymentTypeUtil.create("Tarjeta", "Master Card, Visa, Amex"));
        paymentTypeRepository.save(PaymentTypeUtil.create("Transferencia Bancaria",
                "Transferencia a cuenta BBVA"));
    }

    void initVendor() {
        vendorRepository.save(VendorUtil.create("Luis Compugamo", "Luis compugamo de guadalajara"));
        vendorRepository.save(VendorUtil.create("Pareja", "Pareja de TJ"));
        vendorRepository.save(VendorUtil.create("Fernando Maza", "De mazatlan"));
        vendorRepository.save(VendorUtil.create("Del toro", "de SDC"));
        vendorRepository.save(VendorUtil.create("Jarocho Mesones", "de mesones de la cdmx"));
    }

    void initCustomer() {
        customerRepository.save(Customer.builder().name("Plaza").build());
    }


    void initExpenseCategory() {
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Comida"));
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Pago de servicios"));
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Gastos de la tienda"));
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Sueldos"));
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Gastos de la bodega"));
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Ahorro"));
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Gastos de Baruch"));
        expenseCategoryRepository.save(ExpenseCategoryUtil.create("Gastos de Jafet"));
    }


    private void initStatus() {
        statusRepository.save(StatusUtil.create("CREATED"));
        statusRepository.save(StatusUtil.create("COMPLETED"));
        statusRepository.save(StatusUtil.create("CANCEL"));
        statusRepository.save(StatusUtil.create("PROCESSED"));
    }

    private void initAttributes() {
        attributes = new HashMap<>();
        attributes.put("storage", attributeRepository.save(AttributeUtil.create("Almacenamiento", AttributeTypeEnum.STRING, AttributeUnitEnum.GB)));
        attributes.put("memory", attributeRepository.save(AttributeUtil.create("Memoria", AttributeTypeEnum.STRING, AttributeUnitEnum.GB)));
        attributes.put("cpu", attributeRepository.save(AttributeUtil.create("Procesador", AttributeTypeEnum.STRING, AttributeUnitEnum.GHZ)));
        attributes.put("videoCard", attributeRepository.save(AttributeUtil.create("Tarjeta Video", AttributeTypeEnum.BOOLEAN, AttributeUnitEnum.WITHOUT_UNIT)));
        attributes.put("displayType", attributeRepository.save(AttributeUtil.create("Tipo Pantalla", AttributeTypeEnum.STRING, AttributeUnitEnum.WITHOUT_UNIT)));
        attributes.put("hdmi", attributeRepository.save(AttributeUtil.create("HDMI", AttributeTypeEnum.BOOLEAN, AttributeUnitEnum.WITHOUT_UNIT)));
        attributes.put("displayPort", attributeRepository.save(AttributeUtil.create("DisplayPort", AttributeTypeEnum.BOOLEAN, AttributeUnitEnum.WITHOUT_UNIT)));
        attributes.put("vga", attributeRepository.save(AttributeUtil.create("VGA", AttributeTypeEnum.BOOLEAN, AttributeUnitEnum.WITHOUT_UNIT)));
        attributes.put("dvi", attributeRepository.save(AttributeUtil.create("DVI", AttributeTypeEnum.BOOLEAN, AttributeUnitEnum.WITHOUT_UNIT)));
        attributes.put("size", attributeRepository.save(AttributeUtil.create("Pulgadas", AttributeTypeEnum.STRING, AttributeUnitEnum.IN)));
    }

    private void initCategory() {
        Category desktops = new Category();
        Category laptops = new Category();
        Category displays = new Category();
        Category accessories = new Category();
        desktops.setName("Desktops and All-in-one");
        desktops.setSubcategories(
                Set.of(
                        SubcategoryUtil.create("Ultra slim", desktops),
                        SubcategoryUtil.create("Tiny", desktops),
                        SubcategoryUtil.create("SFF", desktops),
                        SubcategoryUtil.create("Desktop", desktops),
                        SubcategoryUtil.create("AIO", desktops)
                )
        );
        laptops.setName("Laptops");
        laptops.setSubcategories(
        		Set.of(
                        SubcategoryUtil.create("Menos de 13\"", laptops),
                        SubcategoryUtil.create("14\"", laptops),
                        SubcategoryUtil.create("15\"", laptops),
                        SubcategoryUtil.create("16\" o más", laptops)
                )
        );
        displays.setName("Monitores");
        displays.setSubcategories(
        		Set.of(
                		SubcategoryUtil.create("14\"", displays),
                        SubcategoryUtil.create("15\"", displays),
                        SubcategoryUtil.create("17\"", displays),
                        SubcategoryUtil.create("19\"", displays),
                        SubcategoryUtil.create("19.5\"", displays),
                        SubcategoryUtil.create("20\"", displays),
                        SubcategoryUtil.create("20.5\"", displays),
                        SubcategoryUtil.create("22\"", displays),
                        SubcategoryUtil.create("23\"", displays),
                        SubcategoryUtil.create("24\"", displays),
                        SubcategoryUtil.create("27 o más\"", displays)
                )
        );
        accessories.setName("Accessories");
        accessories.setSubcategories(
        		Set.of(
                        SubcategoryUtil.create("Cable VGA", accessories),
                        SubcategoryUtil.create("Cable Poder Interlock", accessories),
                        SubcategoryUtil.create("Cable Trebol", accessories),
                        SubcategoryUtil.create("Cable HDMI", accessories),
                        SubcategoryUtil.create("Cable DisplayPort", accessories),
                        SubcategoryUtil.create("Memoria", accessories),
                        SubcategoryUtil.create("Procesador", accessories),
                        SubcategoryUtil.create("Disco Estado Solido", accessories),
                        SubcategoryUtil.create("Disco Duro", accessories),
                        SubcategoryUtil.create("Cargador Laptop", accessories)
                )
        );
        desktops.setAttributes(Set.of(
                attributes.get("storage"),
                attributes.get("memory"),
                attributes.get("cpu"),
                attributes.get("videoCard"),
                attributes.get("hdmi"),
                attributes.get("displayPort"),
                attributes.get("vga")
        ));
        laptops.setAttributes(Set.of(
                attributes.get("size"),
                attributes.get("storage"),
                attributes.get("memory"),
                attributes.get("cpu"),
                attributes.get("videoCard"),
                attributes.get("hdmi"),
                attributes.get("displayPort"),
                attributes.get("vga")
        ));
        displays.setAttributes(Set.of(
                attributes.get("displayType"),
                attributes.get("size"),
                attributes.get("hdmi"),
                attributes.get("displayPort"),
                attributes.get("vga")
        ));

        categoryRepository.save(desktops);
        categoryRepository.save(laptops);
        categoryRepository.save(displays);
        categoryRepository.save(accessories);
    }

    private void initBrand() {
        brand = brandRepository.save(BrandUtil.create("Dell"));
        brandRepository.save(BrandUtil.create("Hp"));
        brandRepository.save(BrandUtil.create("Lenovo"));
        brandRepository.save(BrandUtil.create("Kingston"));
        brandRepository.save(BrandUtil.create("Seagate"));
    }
    
    private void initProductCode() {
    	productCode = productCodeRepository.save(ProductCodeUtil.create("Product code demo", "Description producto code."));
    }

    private void initProduct() {
        product = productRepository.save(ProductUtil.create(
                "Producto Demo",
                "Producto Demo",
                0L,
                new BigDecimal(100),
                new BigDecimal(100),
                new BigDecimal(100),
                SubcategoryUtil.create(1L),
                brand,
                productCode,
                store));
    }
    
    private void initProductStore() {
    	productStoreRepository.save(ProductStoreUtil.create(product, store));
    }

    private void initPaymentStatus() {
        paymentStatusRepository.save(PaymentStatusUtil.create("PAID"));
        paymentStatusRepository.save(PaymentStatusUtil.create("UNPAID"));
        paymentStatusRepository.save(PaymentStatusUtil.create("PARTIAL"));
    }
}
