package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.SalePayment;
import com.jupiterhqs.jupiterapiv2.model.SalePaymentDto;
import com.jupiterhqs.jupiterapiv2.service.SalePaymentService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sale-payments")
@AllArgsConstructor
public class SalePaymentController {

    private SalePaymentService salePaymentService;

    @PostMapping("/")
    public ResponseEntity<SalePayment> save(@Valid @RequestBody SalePaymentDto salePaymentDto) {
        return ResponseUtil.created(salePaymentService.save(salePaymentDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<SalePayment>> findAll(Pageable pageable) {
        return ResponseEntity.ok(salePaymentService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<SalePayment> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(salePaymentService.findOneById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(salePaymentService.sofDelete(id));
    }
}
