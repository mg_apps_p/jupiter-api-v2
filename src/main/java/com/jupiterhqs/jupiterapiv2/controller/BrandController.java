package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Brand;
import com.jupiterhqs.jupiterapiv2.model.BrandDto;
import com.jupiterhqs.jupiterapiv2.service.BrandService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/brands")
@AllArgsConstructor
public class BrandController {

    private BrandService brandService;

    @PostMapping("/")
    public ResponseEntity<Brand> save(@Valid @RequestBody BrandDto brandDto) {
        return ResponseUtil.created(brandService.save(brandDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<Brand>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(brandService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Brand> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(brandService.findOneById(id));
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(brandService.softDelete(id));
    }
}
