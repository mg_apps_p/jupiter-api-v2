package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Category;
import com.jupiterhqs.jupiterapiv2.model.CategoryDto;
import com.jupiterhqs.jupiterapiv2.service.CategoryService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
@AllArgsConstructor
public class CategoryController {

    private CategoryService categoryService;

    @PostMapping("/")
    public ResponseEntity<Category> save(@Valid @RequestBody CategoryDto categoryDTO) {
        return ResponseUtil.created(categoryService.save(categoryDTO));
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Category> update(@PathVariable("id") long id, @Valid @RequestBody CategoryDto categoryDTO) {
        return ResponseEntity.ok(categoryService.update(id, categoryDTO));
    }
    

    @GetMapping("/")
    public ResponseEntity<Page<Category>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(categoryService.findAll(pageable, pattern));
    }


    @GetMapping("/{id}")
    public ResponseEntity<Category> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(categoryService.findOneById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(categoryService.softDelete(id));
    }
}
