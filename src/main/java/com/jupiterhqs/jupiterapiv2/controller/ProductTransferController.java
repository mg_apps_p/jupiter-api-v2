package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.ProductTransfer;
import com.jupiterhqs.jupiterapiv2.model.ProductTransferDto;
import com.jupiterhqs.jupiterapiv2.service.ProductTransferService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product-transfers")
@AllArgsConstructor
public class ProductTransferController {

    private final ProductTransferService productTransferService;

    @PostMapping("/")
    public ResponseEntity<ProductTransfer> save(@Valid @RequestBody ProductTransferDto productDto) {
        return ResponseUtil.created(productTransferService.save(productDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<ProductTransfer>> findAll(Pageable pageable) {
        return ResponseEntity.ok(productTransferService.findAll(pageable));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(productTransferService.softDelete(id));
    }
}
