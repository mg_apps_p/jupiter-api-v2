package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Sale;
import com.jupiterhqs.jupiterapiv2.model.SaleDto;
import com.jupiterhqs.jupiterapiv2.service.SaleService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import com.lowagie.text.DocumentException;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/sales")
@AllArgsConstructor
public class SaleController {

    private SaleService saleService;

    @PostMapping("/")
    public ResponseEntity<Sale> save(@Valid @RequestBody SaleDto saleDto) {
        return ResponseUtil.created(saleService.save(saleDto));
    }

    @GetMapping("/")
    ResponseEntity<Page<Sale>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
    	   return ResponseEntity.ok(saleService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Sale> findOneById(@PathVariable long id) {
        return ResponseEntity.ok(saleService.findOneById(id));
    }
    
    @GetMapping("/store/{storeId}")
    public ResponseEntity<Page<Sale>> findByStore(
            Pageable pageable,
            @PathVariable("storeId") long storeId,
            @RequestParam("pattern") String pattern
    ) {
        return ResponseEntity.ok(saleService.findByStore(pageable, storeId, pattern));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable long id) {
        return ResponseEntity.ok(saleService.softDelete(id));
    }

    @GetMapping("/export/{id}")
    public void exportToPDF(HttpServletResponse response, @PathVariable long id) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=sale_" + id + "_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        saleService.exportToPdf(response, id);
    }
}
