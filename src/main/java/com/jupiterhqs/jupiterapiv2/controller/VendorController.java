package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Vendor;
import com.jupiterhqs.jupiterapiv2.model.VendorDto;
import com.jupiterhqs.jupiterapiv2.service.VendorService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vendors")
@AllArgsConstructor
public class VendorController {

    private VendorService vendorService;

    @PostMapping("/")
    public ResponseEntity<Vendor> save(@Valid @RequestBody VendorDto vendorDto) {
        return ResponseUtil.created(vendorService.save(vendorDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<Vendor>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(vendorService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Vendor> findOneById(@PathVariable("id") long id) {

        return ResponseEntity.ok(vendorService.findOneById(id));
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(vendorService.softDelete(id));
    }
}
