package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.PaymentStatus;
import com.jupiterhqs.jupiterapiv2.model.PaymentStatusDto;
import com.jupiterhqs.jupiterapiv2.service.PaymentStatusService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment-status")
@AllArgsConstructor
public class PaymentStatusController {

    private PaymentStatusService paymentStatusService;

    @PostMapping("/")
    public ResponseEntity<PaymentStatus> save(@Valid @RequestBody PaymentStatusDto paymentStatus) {
        return ResponseUtil.created(paymentStatusService.save(paymentStatus));
    }

    @GetMapping("/")
    public ResponseEntity<Page<PaymentStatus>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(paymentStatusService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PaymentStatus> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(paymentStatusService.findOneById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(paymentStatusService.softDelete(id));
    }
}
