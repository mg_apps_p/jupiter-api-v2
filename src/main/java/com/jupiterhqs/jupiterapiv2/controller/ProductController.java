package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Product;
import com.jupiterhqs.jupiterapiv2.model.ProductDto;
import com.jupiterhqs.jupiterapiv2.service.ProductService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping("/")
    public ResponseEntity<Product> save(@Valid @RequestBody ProductDto productDto) {
        return ResponseUtil.created(productService.save(productDto));
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable("id") long id, @Valid @RequestBody ProductDto ProductDto) {
        return ResponseEntity.ok(productService.update(id, ProductDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<Product>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(productService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(productService.findOneById(id));
    }

    @GetMapping("/codeOrSku/{codeOrSku}")
    public ResponseEntity<Product> findByCodeOrSku(@PathVariable("codeOrSku") String codeOrSku, @RequestParam("storeId") long storeId) {
        return ResponseEntity.ok(productService.findByCodeOrSku(codeOrSku, storeId));
    }

    @GetMapping("/store/{storeId}")
    public ResponseEntity<Page<Product>> findByStore(
            Pageable pageable,
            @PathVariable("storeId") long storeId,
            @RequestParam("pattern") String pattern
    ) {
        return ResponseEntity.ok(productService.findByStore(pageable, storeId, pattern));
    }
    
    @GetMapping("/stores/origin/{storeOriginId}/destination/{storeDestinationId}")
    public ResponseEntity<Page<Product>> findInBothStores(Pageable pageable,
    		@PathVariable("storeOriginId") long storeOriginId,
            @PathVariable("storeDestinationId") long storeDestinationId,
            @RequestParam("pattern") String pattern) {
    	return ResponseEntity.ok(productService.findInBothStores(pageable, storeOriginId, storeDestinationId, pattern));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(productService.sofDelete(id));
    }
}
