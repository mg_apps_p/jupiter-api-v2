package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.ProductCode;
import com.jupiterhqs.jupiterapiv2.model.ProductCodeDto;
import com.jupiterhqs.jupiterapiv2.service.ProductCodeService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product-codes")
@AllArgsConstructor
public class ProductCodeController {
	

    private ProductCodeService productCodeService;

    @PostMapping("/")
    public ResponseEntity<ProductCode> save(@Valid @RequestBody ProductCodeDto productCodeDto) {
        return ResponseUtil.created(productCodeService.save(productCodeDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<ProductCode>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(productCodeService.findAll(pageable, pattern));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(productCodeService.sofDelete(id));
    }
}
