package com.jupiterhqs.jupiterapiv2.controller;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.service.ProductStoreService;

@RestController
@RequestMapping("/products-store")
@AllArgsConstructor
public class ProductStoreController {
	
	final ProductStoreService productStoreService;
	
    @GetMapping("/store/{storeId}")
    public ResponseEntity<Page<ProductStore>> findByStore(
            Pageable pageable,
            @PathVariable("storeId") long storeId,
            @RequestParam("pattern") String pattern
    ) {
        return ResponseEntity.ok(productStoreService.findAllProductByPatternAndStore(pageable, storeId, pattern));
    }
}
