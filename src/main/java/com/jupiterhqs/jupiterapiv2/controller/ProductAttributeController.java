package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.ProductAttribute;
import com.jupiterhqs.jupiterapiv2.service.ProductAttributeService;
import lombok.AllArgsConstructor;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product-attributes")
@AllArgsConstructor
public class ProductAttributeController {
	

    private ProductAttributeService productAttributeService;


    @GetMapping("/product/{productId}")
    public ResponseEntity<List<ProductAttribute>> findByProduct( @PathVariable("productId") Long productId) {
        return ResponseEntity.ok(productAttributeService.findByProduct(productId));
    }

}
