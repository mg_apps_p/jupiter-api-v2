package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Subcategory;
import com.jupiterhqs.jupiterapiv2.model.SubcategoryDto;
import com.jupiterhqs.jupiterapiv2.service.SubcategoryService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subcategories")
@AllArgsConstructor
public class SubcategoryController {

    private SubcategoryService subCategoryService;

    @PostMapping("/")
    public ResponseEntity<Subcategory> save(@Valid @RequestBody SubcategoryDto subCategoryDto){
        return subCategoryService.save(subCategoryDto);
    }

    @GetMapping("/")
    public ResponseEntity<List<Subcategory>> findAll(){
        return subCategoryService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subcategory> findOneById(@PathVariable("id") long id){
        return subCategoryService.findOneById(id);

    }

    @GetMapping("/category/{categoryId}")
    public ResponseEntity<List<Subcategory>> findByCategoryId(@PathVariable("categoryId") long categoryId) {
        return subCategoryService.findByCategoryId(categoryId);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return subCategoryService.softDelete(id);
    }

}
