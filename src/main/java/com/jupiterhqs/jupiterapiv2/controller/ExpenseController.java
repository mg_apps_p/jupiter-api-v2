package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Expense;
import com.jupiterhqs.jupiterapiv2.model.ExpenseDto;
import com.jupiterhqs.jupiterapiv2.service.ExpenseService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/expenses")
@AllArgsConstructor
public class ExpenseController {
    private ExpenseService expenseService;

    @PostMapping("/")
    public ResponseEntity<Expense> save(@Valid @RequestBody ExpenseDto expenseDto) {
        return ResponseUtil.created(expenseService.save(expenseDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<Expense>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(expenseService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Expense> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(expenseService.findOneById(id));
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(expenseService.softDelete(id));
    }

    @PutMapping({"/{id}"})
    public ResponseEntity<Expense> update(@PathVariable("id") long id, @RequestBody ExpenseDto expenseDto) {
        return ResponseEntity.ok(expenseService.update(id, expenseDto));
    }
}
