package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.PaymentType;
import com.jupiterhqs.jupiterapiv2.model.PaymentTypeDto;
import com.jupiterhqs.jupiterapiv2.service.PaymentTypeService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment-types")
@AllArgsConstructor
public class PaymentTypeController {

    private PaymentTypeService paymentTypeService;

    @PostMapping("/")
    public ResponseEntity<PaymentType> save(@Valid @RequestBody PaymentTypeDto paymentTypeDto) {
        return ResponseUtil.created(paymentTypeService.save(paymentTypeDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<PaymentType>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(paymentTypeService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PaymentType> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(paymentTypeService.findOneById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(paymentTypeService.softDelete(id));
    }
}
