package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.ProductTag;
import com.jupiterhqs.jupiterapiv2.model.ProductTagDto;
import com.jupiterhqs.jupiterapiv2.service.ProductTagService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product-tags")
@AllArgsConstructor
public class ProductTagController {
    private ProductTagService productTagService;

    @PostMapping("/")
    public ResponseEntity<ProductTag> save(@Valid @RequestBody ProductTagDto productTagDto) {
        return ResponseUtil.created(productTagService.save(productTagDto));
    }

    @GetMapping("/")
    public ResponseEntity<List<ProductTag>> findAll() {
        return ResponseEntity.ok(productTagService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductTag> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(productTagService.findOneById(id));
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(productTagService.softDelete(id));
    }
}
