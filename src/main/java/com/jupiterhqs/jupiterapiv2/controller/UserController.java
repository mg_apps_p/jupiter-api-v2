package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.User;
import com.jupiterhqs.jupiterapiv2.service.UserService;
import lombok.AllArgsConstructor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private UserService userService;

    @GetMapping("/")
    public ResponseEntity<Page<User>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(userService.findAll(pageable, pattern));
    }

    @GetMapping("/store/{storeId}")
    public ResponseEntity<Page<User>> findByStore(
            Pageable pageable,
            @PathVariable("storeId") long storeId,
            @RequestParam("pattern") String pattern
    ) {
        return ResponseEntity.ok(userService.findByStore(pageable, storeId, pattern));
    }
}
