package com.jupiterhqs.jupiterapiv2.controller;


import com.jupiterhqs.jupiterapiv2.entity.ExpenseCategory;
import com.jupiterhqs.jupiterapiv2.model.ExpenseCategoryDto;
import com.jupiterhqs.jupiterapiv2.service.ExpenseCategoryService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/expense-category")
@AllArgsConstructor
public class ExpenseCategoryController {
    private ExpenseCategoryService expenseCategoryService;

    @PostMapping("/")
    public ResponseEntity<ExpenseCategory> save(@Valid @RequestBody ExpenseCategoryDto expenseCategoryDto) {
        return ResponseUtil.created(expenseCategoryService.save(expenseCategoryDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<ExpenseCategory>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(expenseCategoryService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ExpenseCategory> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(expenseCategoryService.findOneById(id));
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(expenseCategoryService.softDelete(id));
    }
}
