package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Customer;
import com.jupiterhqs.jupiterapiv2.model.CustomerDto;
import com.jupiterhqs.jupiterapiv2.service.CustomerService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
@AllArgsConstructor
public class CustomerController {

    private CustomerService customerService;

    @PostMapping("/")
    public ResponseEntity<Customer> save(@Valid @RequestBody CustomerDto customerDto) {
        return ResponseUtil.created(customerService.save(customerDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<Customer>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(customerService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> findOneById(@PathVariable("id") long id) {

        return ResponseEntity.ok(customerService.findOneById(id));
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(customerService.softDelete(id));
    }
}
