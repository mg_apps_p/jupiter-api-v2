package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.SaleDetail;
import com.jupiterhqs.jupiterapiv2.model.SaleDetailDto;
import com.jupiterhqs.jupiterapiv2.service.SaleDetailService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sale-detail")
@AllArgsConstructor
public class SaleDetailController {

    private SaleDetailService saleDetailService;

    @PostMapping("/")
    public ResponseEntity<SaleDetail> save(@Valid @RequestBody SaleDetailDto saleDetailDto) {
        return saleDetailService.save(saleDetailDto);
    }


    @GetMapping("/")
    public ResponseEntity<List<SaleDetail>> findAll() {
        return saleDetailService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<SaleDetail> findOneById(@PathVariable("id") long id){
        return saleDetailService.findOneById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softdelete(@PathVariable("id") long id){
        return saleDetailService.softdelete(id);
    }
}
