package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.PurchasePayment;
import com.jupiterhqs.jupiterapiv2.model.PurchasePaymentDto;
import com.jupiterhqs.jupiterapiv2.service.PurchasePaymentService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/purchase-payments")
@AllArgsConstructor
public class PurchasePaymentController {

    private PurchasePaymentService purchasePaymentService;

    @PostMapping("/")
    public ResponseEntity<PurchasePayment> save(@Valid @RequestBody PurchasePaymentDto purchasePaymentDto) {
        return ResponseUtil.created(purchasePaymentService.save(purchasePaymentDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<PurchasePayment>> findAll(Pageable pageable) {
        return ResponseEntity.ok(purchasePaymentService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PurchasePayment> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(purchasePaymentService.findOneById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(purchasePaymentService.sofDelete(id));
    }
}
