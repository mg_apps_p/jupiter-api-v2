package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.RelatedProduct;
import com.jupiterhqs.jupiterapiv2.model.RelatedProductDto;
import com.jupiterhqs.jupiterapiv2.service.RelatedProductService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/related-products")
@AllArgsConstructor
public class RelatedProductController {

    private RelatedProductService relatedProductService;

    @PostMapping("/")
    public ResponseEntity<List<RelatedProduct>> save(@Valid @RequestBody RelatedProductDto relatedProductDto) {
        return ResponseUtil.created(relatedProductService.save(relatedProductDto));
    }

    @GetMapping("/")
    public ResponseEntity<List<RelatedProduct>> findAll() {
        return ResponseEntity.ok(relatedProductService.findAll());
    }

}
