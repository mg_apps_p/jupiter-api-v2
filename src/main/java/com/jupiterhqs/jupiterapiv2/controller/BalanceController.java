package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.model.BalanceResponse;
import com.jupiterhqs.jupiterapiv2.service.BalanceService;
import lombok.AllArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/balance")
@AllArgsConstructor
public class BalanceController {

    private BalanceService balanceService;

    @GetMapping("/today")
    public ResponseEntity<BalanceResponse> getTodayBalance() {
        return ResponseEntity.ok(balanceService.getTodayBalance());
    }

    @GetMapping("/range")
    public ResponseEntity<BalanceResponse> getBalanceByDates(@RequestParam String from, @RequestParam String to) {
        return ResponseEntity.ok(balanceService.getBalanceByDates(from, to));
    }
}