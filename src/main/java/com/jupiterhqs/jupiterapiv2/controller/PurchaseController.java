package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Purchase;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDto;
import com.jupiterhqs.jupiterapiv2.service.PurchaseService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/purchases")
@AllArgsConstructor
public class PurchaseController {

    private PurchaseService purchaseService;

    @PostMapping("/")
    public ResponseEntity<Purchase> save(@Valid @RequestBody PurchaseDto purchaseDto) throws NoSuchAlgorithmException {
        return ResponseUtil.created(purchaseService.save(purchaseDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<Purchase>> findAll(Pageable pageable, @RequestParam("pattern") String pattern) {
        return ResponseEntity.ok(purchaseService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Purchase> findOneById(@PathVariable("id") long id) {
        return ResponseEntity.ok(purchaseService.findOneById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id) {
        return ResponseEntity.ok(purchaseService.sofDelete(id));
    }
}
