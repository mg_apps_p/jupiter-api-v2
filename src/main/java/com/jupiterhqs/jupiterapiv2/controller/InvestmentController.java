package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Investment;
import com.jupiterhqs.jupiterapiv2.model.InvestmentDto;
import com.jupiterhqs.jupiterapiv2.service.InvestmentService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/investment")
public class InvestmentController {

    private InvestmentService investmentService;

    @PostMapping("/")
    public ResponseEntity<Investment> save(@Valid @RequestBody InvestmentDto investmentDto) {
        return investmentService.save(investmentDto);
    }

    @GetMapping("/")
    public ResponseEntity<List<Investment>> findALL() {

        return investmentService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Investment> findOneById(@PathVariable("id") long id) {

        return investmentService.findOneByID(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable("id") long id){
        return investmentService.softDelete(id);
    }
}
