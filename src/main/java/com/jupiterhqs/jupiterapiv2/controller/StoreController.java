package com.jupiterhqs.jupiterapiv2.controller;

import com.jupiterhqs.jupiterapiv2.entity.Store;
import com.jupiterhqs.jupiterapiv2.model.StoreDto;
import com.jupiterhqs.jupiterapiv2.service.StoreService;
import com.jupiterhqs.jupiterapiv2.util.ResponseUtil;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stores")
@AllArgsConstructor
public class StoreController {

    private StoreService storeService;

    @PostMapping("/")
    public ResponseEntity<Store> save(@Valid @RequestBody StoreDto storeDto) {
        return ResponseUtil.created(storeService.save(storeDto));
    }

    @GetMapping("/")
    public ResponseEntity<Page<Store>> findAll(Pageable pageable, @RequestParam("pattern") String pattern){
        return ResponseEntity.ok(storeService.findAll(pageable, pattern));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Store> findOneById(@PathVariable long id){
        return ResponseEntity.ok(storeService.findOneById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> softDelete(@PathVariable long id){
        return ResponseEntity.ok(storeService.softDelete(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Store> update(@PathVariable long id, @Valid @RequestBody StoreDto storeDto) {
        return ResponseEntity.ok(storeService.update(id, storeDto));
    }
}
