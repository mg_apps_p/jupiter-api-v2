package com.jupiterhqs.jupiterapiv2.controller;


import com.jupiterhqs.jupiterapiv2.service.ConfigService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/config")
@AllArgsConstructor
public class ConfigController {

    public ConfigService configService;

    @GetMapping("/db/")
    public ResponseEntity<Boolean> db() {
        return ResponseEntity.ok(configService.initDb());
    }
}
