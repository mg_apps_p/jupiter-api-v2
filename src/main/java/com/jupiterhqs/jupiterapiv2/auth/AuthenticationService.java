package com.jupiterhqs.jupiterapiv2.auth;

import com.jupiterhqs.jupiterapiv2.auth.jwt.JwtService;
import com.jupiterhqs.jupiterapiv2.entity.User;
import com.jupiterhqs.jupiterapiv2.model.AuthenticationRequest;
import com.jupiterhqs.jupiterapiv2.model.AuthenticationResponse;
import com.jupiterhqs.jupiterapiv2.model.RegisterRequest;
import com.jupiterhqs.jupiterapiv2.repository.UserRepository;
import com.jupiterhqs.jupiterapiv2.util.UserUtil;

import lombok.AllArgsConstructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class AuthenticationService {
	
    protected static final Logger log = LoggerFactory.getLogger(AuthenticationService.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(RegisterRequest request) {
    	log.info("Register new user: " + request.getEmail());
        request.setPassword(passwordEncoder.encode((request.getPassword())));
        User user = userRepository.save(UserUtil.create(request));

        return buildToken(user);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
    	try {
        	log.info("Authenticating user: " + request.getEmail());
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getEmail(),
                            request.getPassword()
                    )
            );
            User user = userRepository.findByEmail(request.getEmail()).orElseThrow();

            return buildToken(user);
    	} catch(Exception ex) {
    		log.error(ex.toString());
    		
    		throw ex;
    	}
  
    }

    private AuthenticationResponse buildToken(User user) {
        String jwtToken = jwtService.generateToken(user);

        return AuthenticationResponse.builder().token(jwtToken).build();
    }
}