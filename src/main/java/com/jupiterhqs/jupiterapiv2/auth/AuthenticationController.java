package com.jupiterhqs.jupiterapiv2.auth;

import com.jupiterhqs.jupiterapiv2.model.AuthenticationRequest;
import com.jupiterhqs.jupiterapiv2.model.AuthenticationResponse;
import com.jupiterhqs.jupiterapiv2.model.RegisterRequest;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthenticationController {

	   private final AuthenticationService authenticationService;

	    @PostMapping("/register")
	    public ResponseEntity<AuthenticationResponse> register(@Valid @RequestBody RegisterRequest request) {
	        return ResponseEntity.ok(authenticationService.register(request));
	    }

	    @PostMapping("/authenticate")
	    public ResponseEntity<AuthenticationResponse> authenticate(@Valid @RequestBody AuthenticationRequest request) {
	        return ResponseEntity.ok(authenticationService.authenticate(request));
	    }
 }
