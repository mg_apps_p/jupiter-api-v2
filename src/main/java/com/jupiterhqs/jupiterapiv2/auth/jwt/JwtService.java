package com.jupiterhqs.jupiterapiv2.auth.jwt;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.jupiterhqs.jupiterapiv2.entity.Store;
import com.jupiterhqs.jupiterapiv2.entity.User;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

@Service
public class JwtService {
	
	
	@Value("${jupiter.auth.secret-key}")
	private String secretKey;
	
	@Value("${jupiter.auth.refreshExpiration}")
    private long refreshExpiration;
	
	@Value("${jupiter.auth.expiration}")
    private long expiration;

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public String generateToken(User user) {
        return generateToken(new HashMap<>(), user);
    }

    public String generateToken(
            Map<String, Object> extraClaims,
            User user
    ) {
    	extraClaims.put("stores", buildStoreClaim(user.getStores()));
    	extraClaims.put("user", buildUserClaim(user));
        return Jwts
                .builder()
                .setClaims(extraClaims)
                .setSubject(user.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(getSignInKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSignInKey() {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        return Keys.hmacShaKeyFor(keyBytes);
    }
    
    private ArrayList<Object> buildStoreClaim(Set<Store> stores) {
    	ArrayList<Object> storesToToken = new ArrayList<>();
    	stores.forEach(store -> {
        	HashMap<String, Object> storeMap = new HashMap<>();
        	storeMap.put("id", Long.valueOf(store.getStoreId()));
        	storeMap.put("name", store.getName());
        	
        	storesToToken.add(storeMap);
    	});
    	
    	
    	return storesToToken;
    }
    
    private Map<String, Object> buildUserClaim(User user) {
    	HashMap<String, Object> userMap = new HashMap<>();
    	userMap.put("surname", user.getLastname());
    	userMap.put("name", user.getFirstname());
    	userMap.put("role", user.getRole());
    	userMap.put("id", user.getUserId());
    	
    	return userMap;
    }
    
    
}

