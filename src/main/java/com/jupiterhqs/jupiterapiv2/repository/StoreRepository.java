package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {
    Page<Store> findAllByNameContainingIgnoreCase(Pageable pageable, String name);
}
