package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.Subcategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SubcategoryRepository extends JpaRepository<Subcategory, Long> {

    Optional<List<Subcategory>> findByCategoryCategoryId(long categoryId);
}
