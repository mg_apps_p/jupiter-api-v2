package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.Sale;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface SaleRepository extends JpaRepository<Sale, Long> {
    List<Sale> findByCreatedBetween(LocalDateTime from, LocalDateTime to);
    Page<Sale> findAllByCertContainingIgnoreCase(Pageable pageable, String cert);
    Page<Sale> findByStoreStoreIdAndCertContainingIgnoreCase(Pageable pageable, Long storeId, String cert);
}
