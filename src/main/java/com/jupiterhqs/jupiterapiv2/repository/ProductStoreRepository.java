package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.ProductStore;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductStoreRepository extends JpaRepository<ProductStore, Long> {
    Page<ProductStore> findAllByStoreStoreId(Pageable pageable, long storeId);
    Page<ProductStore> findAllByProductStoreIdInAndProductNameContainingIgnoreCase(Pageable pageable, List<Long> productStoreId, String name);
    
    @Query(value = "select * from {h-schema}product_stores productStores left join {h-schema}products product ON product.product_id = productStores.product_store_product_id where productStores.product_store_product_id = :productId", nativeQuery = true)
    public List<ProductStore> findAllByProductIgnoreDeleted(@Param("productId") long productId);
    
}
