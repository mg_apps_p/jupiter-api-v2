package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByProductStoresStoreStoreIdAndProductCodeNameIgnoreCaseOrSkuIgnoreCase(
            long storeId, String code, String sku);
    List<Product> findAllByProductIdIn(List<Long> productIds);
    Page<Product> findAllByNameContainingIgnoreCase(Pageable pageable, String name);
    Page<Product> findAllByProductStoresStoreStoreIdAndNameContainingIgnoreCase(Pageable pageable,long storeId, String name);
    Page<Product> findAllByProductStoresStoreStoreIdAndDescriptionContainingIgnoreCase(Pageable pageable,long storeId, String description);
    Page<Product> findAllByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(Pageable pageable, String name, String description);
    Page<Product> findAllByProductIdInAndProductStoresStoreStoreId(Pageable pageable, List<Long> productIds, long storeId);
    
    @Modifying
    @Query("update Product p set p.averageSalePrice = ?1 where p.productId = ?2")
    int setAverageSalePrice(BigDecimal averageSalePrice, Long productId);
    
    @Modifying
    @Query("update Product p set p.averagePurchasePrice = ?1 where p.productId = ?2")
    int setAveragePurchasePrice(BigDecimal averagePurchasePrice, Long productId);
}