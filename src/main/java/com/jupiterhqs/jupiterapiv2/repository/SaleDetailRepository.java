package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.SaleDetail;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleDetailRepository  extends JpaRepository<SaleDetail, Long> {
    Page<SaleDetail> findAllByProductStoreProductProductId(Pageable pageable, Long productId);
}
