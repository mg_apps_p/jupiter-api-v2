package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.PaymentStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentStatusRepository extends JpaRepository<PaymentStatus, Long> {
    Page<PaymentStatus> findAllByNameContainingIgnoreCase(Pageable pageable, String name);
}
