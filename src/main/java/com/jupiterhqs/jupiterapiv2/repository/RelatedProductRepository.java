package com.jupiterhqs.jupiterapiv2.repository;

import com.jupiterhqs.jupiterapiv2.entity.RelatedProduct;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RelatedProductRepository extends JpaRepository<RelatedProduct, Long> {;
}
