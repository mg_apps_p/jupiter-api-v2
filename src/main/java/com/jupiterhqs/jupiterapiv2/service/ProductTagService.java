package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.ProductTag;
import com.jupiterhqs.jupiterapiv2.exception.ProductTagDuplicateNumberException;
import com.jupiterhqs.jupiterapiv2.exception.ProductTagNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.ProductTagDto;
import com.jupiterhqs.jupiterapiv2.repository.ProductTagRepository;
import com.jupiterhqs.jupiterapiv2.util.ProductTagUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductTagService {
    protected static final Logger log = LoggerFactory.getLogger(ProductTagService.class);

    private ProductTagRepository productTagRepository;

    /**
     * Saves new productTag
     *
     * @param ProductTagDto productTagDto
     * @return ProductTag A new productTag saved
     */

    public ProductTag save(ProductTagDto productTagDto) {
        try {
            ProductTag productTag = ProductTagUtil.create(productTagDto);

            return productTagRepository.save(productTag);
        } catch (DataIntegrityViolationException ex) {
            log.error(ex.getMessage());

            throw new ProductTagDuplicateNumberException(productTagDto.getSerialNumber());
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("PRODUCT_TAG_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     * Find All productTag entities
     *
     * @return List<ProductTag> A list with all productTags
     */
    public List<ProductTag> findAll() {
        try {
            return productTagRepository.findAll();

        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("PRODUCT_TAG_GET_ALL_ERROR");

            throw ex;
        }
    }

    /**
     * Find one productTag with an id given
     *
     * @param long id The id to find
     * @return A productTag found or throw ProductTagNotFoundException
     */
    public ProductTag findOneById(long id) {
        return productTagRepository
                .findById(id).orElseThrow(() -> new ProductTagNotFoundException("PRODUCT_TAG_NOT_FOUND"));
    }

    /**
     * Does a soft delete for entity
     *
     * @param long id The id to delete
     * @return boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            productTagRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

}
