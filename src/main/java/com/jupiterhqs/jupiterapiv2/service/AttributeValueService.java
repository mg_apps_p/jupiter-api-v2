package com.jupiterhqs.jupiterapiv2.service;


import com.jupiterhqs.jupiterapiv2.entity.AttributeValue;
import com.jupiterhqs.jupiterapiv2.exception.AttributeValueNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.AttributeValueDto;
import com.jupiterhqs.jupiterapiv2.repository.AttributeValueRepository;
import com.jupiterhqs.jupiterapiv2.util.AttributeValueUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AttributeValueService {

    protected static final Logger log = LoggerFactory.getLogger(AttributeValueService.class);

    private AttributeValueRepository attributeValueRepository;

    /**
     * Saves new attribute
     *
     * @param AttributeValueDto attributeValueDto
     * @return ProductType A new attribute saved
     */
    public AttributeValue save(AttributeValueDto attributeValueDto) {
        try {
            log.info(ServiceMessages.ATTRIBUTE_VALUE_CREATE);
            AttributeValue attributeValue = AttributeValueUtil.create(attributeValueDto);

            return attributeValueRepository.save(attributeValue);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.ATTRIBUTE_VALUE_CREATE);

            throw ex;
        }
    }

    /**
     * Find all attributes entities
     *
     * @return List<AttributeValue> A list with all attributes
     */
    public List<AttributeValue> findAll() {
        try {
            log.info(ServiceMessages.ATTRIBUTE_VALUE_FIND_ALL);

            return attributeValueRepository.findAll();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.ATTRIBUTE_VALUE_FIND_ALL);

            throw ex;
        }
    }

    /**
     * Find one attribute with id given
     *
     * @param long id The id to find
     * @return A attribute found or throw AttributeValueNotFoundException
     */
    public AttributeValue findOneById(long attributeValueId) {
        log.info(StringParser.parseWithId(ServiceMessages.ATTRIBUTE_VALUE_FIND_BY_ID, attributeValueId));

        return attributeValueRepository.findById(attributeValueId)
                .orElseThrow(() -> new AttributeValueNotFoundException(attributeValueId));
    }

    /**
     * it does soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            log.info(ServiceMessages.ATTRIBUTE_VALUE_DELETE);
            attributeValueRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }
}
