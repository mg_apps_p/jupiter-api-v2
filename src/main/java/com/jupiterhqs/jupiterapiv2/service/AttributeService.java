package com.jupiterhqs.jupiterapiv2.service;


import com.jupiterhqs.jupiterapiv2.entity.Attribute;
import com.jupiterhqs.jupiterapiv2.exception.AttributeNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.AttributeDto;
import com.jupiterhqs.jupiterapiv2.repository.AttributeRepository;
import com.jupiterhqs.jupiterapiv2.util.AttributeUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AttributeService {

    protected static final Logger log = LoggerFactory.getLogger(AttributeService.class);

    private AttributeRepository attributeRepository;

    /**
     * Saves new attribute
     *
     * @param AttributeDto attributeDto
     * @return ProductType A new attribute saved
     */
    public Attribute save(AttributeDto attributeDto) {
        try {
            log.info(ServiceMessages.ATTRIBUTE_CREATE);
            Attribute attribute = AttributeUtil.create(attributeDto);

            return attributeRepository.save(attribute);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.ATTRIBUTE_CREATE);

            throw ex;
        }
    }

    /**
     * Find all attributes entities
     *
     * @return List<Attribute> A list with all attributes
     */
    public List<Attribute> findAll() {
        try {
            log.info(ServiceMessages.ATTRIBUTE_FIND_ALL);

            return attributeRepository.findAll();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.ATTRIBUTE_FIND_ALL);

            throw ex;
        }
    }

    /**
     * Find one attribute with id given
     *
     * @param long id The id to find
     * @return A attribute found or throw AttributeNotFoundException
     */
    public Attribute findOneById(long attributeId) {
        log.info(StringParser.parseWithId(ServiceMessages.ATTRIBUTE_FIND_BY_ID, attributeId));

        return attributeRepository.findById(attributeId).orElseThrow(() -> new AttributeNotFoundException(attributeId));
    }

    /**
     * it does soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            log.info(ServiceMessages.ATTRIBUTE_DELETE);
            attributeRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }
}
