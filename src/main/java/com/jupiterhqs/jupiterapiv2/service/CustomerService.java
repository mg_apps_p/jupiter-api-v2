package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Customer;
import com.jupiterhqs.jupiterapiv2.exception.CustomerNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.CustomerDto;
import com.jupiterhqs.jupiterapiv2.repository.CustomerRepository;
import com.jupiterhqs.jupiterapiv2.util.CustomerUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {
    protected static final Logger log = LoggerFactory.getLogger(CustomerService.class);

    private CustomerRepository customerRepository;

    /**
     * Saves new customer
     *
     * @param CustomerDTO customerDTO
     * @return Customer A new customer saved
     */
    public Customer save(CustomerDto customerDto) {
        try {
            log.info(ServiceMessages.CUSTOMER_SAVE);
            Customer customer = CustomerUtil.create(customerDto);

            return customerRepository.save(customer);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("CUSTOMER_SAVE_ERROR");

            throw ex;
        }
    }
    
    /**
     * Find All customer entities
     *
     * @return Page<Customer>  A list with all customers
     */
    public Page<Customer> findAll(Pageable pageable, String pattern) {
        try {
            log.info(ServiceMessages.CUSTOMER_FIND_ALL);

            return customerRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("CUSTOMER_GET_ALL_ERROR");

            throw ex;
        }
    }

    /**
     * Find one customer with an id given
     *
     * @param long id The id to find
     * @return A customer found or throw CustomerNotFoundException
     */
    public Customer findOneById(long id) {
        log.info(ServiceMessages.CUSTOMER_FIND_BY_ID);
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new CustomerNotFoundException());

        return customer;
    }

    /**
     * Does a soft delete for entity
     *
     * @param long id The id to delete
     * @return boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            log.info(ServiceMessages.CUSTOMER_DELETE);
            customerRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

}
