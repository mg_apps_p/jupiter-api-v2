    package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Sale;
import com.jupiterhqs.jupiterapiv2.entity.SalePayment;
import com.jupiterhqs.jupiterapiv2.exception.SalePaymentNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.SalePaymentDto;
import com.jupiterhqs.jupiterapiv2.repository.SalePaymentRepository;
import com.jupiterhqs.jupiterapiv2.util.PaymentStatusUtil;
import com.jupiterhqs.jupiterapiv2.util.SalePaymentUtil;
import com.jupiterhqs.jupiterapiv2.util.StatusUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.PaymentStatusIds;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.StatusIdConstants;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;


@Service
@AllArgsConstructor
public class SalePaymentService {

    protected static final Logger log = LoggerFactory.getLogger(SalePaymentService.class);

    private SalePaymentRepository salePaymentRepository;
    private SaleService saleService;

    /**
     * Saves a new sale payment
     *
     * @param SalePaymentDto salePaymentDto
     * @return SalePayment A new sale payment saved
     */
    @Transactional
    public SalePayment save(SalePaymentDto salePaymentDto) {
        try {
            log.info(ServiceMessages.SALE_PAYMENT_CREATE + ": saleId: " + salePaymentDto.getSale().getSaleId());
            SalePayment salePayment = SalePaymentUtil.create(salePaymentDto);
            Sale sale = saleService.findOneById(salePayment.getSale().getSaleId());
            BigDecimal partialPaid = sale.getPartialPaid();
            BigDecimal saleAmount = sale.getAmount();
            BigDecimal newPartialPaid = partialPaid.add(salePaymentDto.getAmount());
            sale.setPartialPaid(newPartialPaid);

            if(newPartialPaid.compareTo(saleAmount) >= 0 ) {
                sale.setPaymentStatus(PaymentStatusUtil.create(PaymentStatusIds.PAID));
                sale.setStatus(StatusUtil.create(StatusIdConstants.COMPLETED));
            }

            saleService.save(sale);

            return salePaymentRepository.save(salePayment);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("SALE_PAYMENT_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     * Find all sale payments entities
     *
     * @return List<SalePayment> A list with all the sales
     */
    public Page<SalePayment> findAll(Pageable pageable) {
        try {
            log.info(ServiceMessages.SALE_PAYMENT_FIND_ALL);

            return salePaymentRepository.findAll(pageable);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.SALE_FIND_ALL);

            throw ex;
        }
    }

    /**
     * Find one sale payment with an id given
     *
     * @param long id The id to find
     * @return A sale payment found or throw SalePaymentNotFoundException
     */
    public SalePayment findOneById(long id) {
        log.info(ServiceMessages.SALE_PAYMENT_FIND_BY_ID);
        return salePaymentRepository
                .findById(id)
                .orElseThrow(() -> new SalePaymentNotFoundException(String.valueOf(id)));

    }

    /**
     * it does a soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean sofDelete(long id) {
        try {
            log.info(ServiceMessages.SALE_PAYMENT_DELETE);
            salePaymentRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }
}
