package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.repository.ProductStoreRepository;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import lombok.AllArgsConstructor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ProductStoreService {

	protected static final Logger log = LoggerFactory.getLogger(ProductStoreService.class);

	private ProductStoreRepository productStoreRepository;

	/**
	 * Saves a new ProductCode
	 *
	 * @param ProductCodeDto productCodeDTO
	 * @return ProductCode A new ProductCode saved
	 */
	@Transactional
	public List<ProductStore> saveAll(List<ProductStore> productStores) {
		try {
			log.info(ServiceMessages.PRODUCT_STORE_CREATE_ALL);

			return productStoreRepository.saveAll(productStores);
		} catch (Exception ex) {
			log.info(String.valueOf(ex));
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_CODE_CREATE_ALL);

			throw ex;
		}
	}

	public List<ProductStore> findAllByIds(List<Long> ids) {
		try {
			log.info(ServiceMessages.PRODUCT_STORE_FIND_ALL_BY_IDS);

			return productStoreRepository.findAllById(ids);
		} catch (Exception ex) {
			log.info(ex.getMessage());
			log.info(ErrorMessages.PRODUCT_STORE_FIND_ALL_BY_IDS);

			throw ex;
		}
	}

	public Page<ProductStore> findAllProductByPatternAndStore(Pageable pageable, long storeId, String pattern) {
		try {
			log.info("Find product products stores by pattern in product and store.");
			List<Long> productStoreIds = productStoreRepository.findAllByStoreStoreId(pageable, storeId).stream()
					.map(productStore -> productStore.getProductStoreId()).toList();

			log.info(productStoreIds.toString());
			return productStoreRepository.findAllByProductStoreIdInAndProductNameContainingIgnoreCase(pageable,
					productStoreIds, pattern);
		} catch (Exception ex) {
			log.error(ex.getMessage());

			throw ex;
		}
	}
	
	public List<ProductStore> findByProductIgnoreDeleted(Long productId) {
		return productStoreRepository.findAllByProductIgnoreDeleted(productId);
	}
}
