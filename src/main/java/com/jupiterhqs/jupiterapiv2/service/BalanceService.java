package com.jupiterhqs.jupiterapiv2.service;


import com.jupiterhqs.jupiterapiv2.entity.Expense;
import com.jupiterhqs.jupiterapiv2.entity.Sale;
import com.jupiterhqs.jupiterapiv2.model.BalanceResponse;
import com.jupiterhqs.jupiterapiv2.util.BalanceUtil;
import com.jupiterhqs.jupiterapiv2.util.ExpenseUtil;
import com.jupiterhqs.jupiterapiv2.util.SaleUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class BalanceService {

    protected static final Logger log = LoggerFactory.getLogger(BalanceService.class);
    private ExpenseService expenseService;
    private SaleService saleService;

    public BalanceResponse getTodayBalance() {
        log.info(ServiceMessages.BALANCE_TODAY);
        LocalDateTime from = BalanceUtil.getTodayDateWithCustomTime(BalanceUtil.MIN_HOUR, BalanceUtil.MIN_MINUTE);
        LocalDateTime to = BalanceUtil.getTodayDateWithCustomTime(BalanceUtil.MAX_HOUR, BalanceUtil.MAX_MINUTE);
        return requestBalance(from, to);
    }

    public BalanceResponse getBalanceByDates(String fromRequest, String toRequest) {
        log.info(ServiceMessages.BALANCE_BY_DATE_RANGE);
        LocalDateTime from = BalanceUtil.getCustomDateWithTime(fromRequest, BalanceUtil.MIN_HOUR, BalanceUtil.MIN_MINUTE);
        LocalDateTime to = BalanceUtil.getCustomDateWithTime(toRequest, BalanceUtil.MAX_HOUR, BalanceUtil.MAX_MINUTE);

        return requestBalance(from, to);
    }

    private BalanceResponse requestBalance(LocalDateTime from, LocalDateTime to) {
        BigDecimal expensesAmount;
        BigDecimal salesAmount;
        List<Expense> expenses = expenseService.findAllByDates(from, to);
        List<Sale> sales = saleService.findAllByDates(from, to);
        if (expenses.isEmpty()) {
            expensesAmount = BigDecimal.ZERO;
        } else {
            expensesAmount = ExpenseUtil.sumAmount(expenses);
        }
        if (sales.isEmpty()) {
            salesAmount = BigDecimal.ZERO;
        } else {
            salesAmount = SaleUtil.sumAmount(sales);
        }

        BigDecimal balance = salesAmount.subtract(expensesAmount);

        return BalanceUtil.create(balance, expensesAmount, salesAmount, from, to);
    }

}
