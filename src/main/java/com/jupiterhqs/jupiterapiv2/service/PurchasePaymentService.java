    package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Purchase;
import com.jupiterhqs.jupiterapiv2.entity.PurchasePayment;
import com.jupiterhqs.jupiterapiv2.exception.PurchasePaymentNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.PurchasePaymentDto;
import com.jupiterhqs.jupiterapiv2.repository.PurchasePaymentRepository;
import com.jupiterhqs.jupiterapiv2.util.PaymentStatusUtil;
import com.jupiterhqs.jupiterapiv2.util.PurchasePaymentUtil;
import com.jupiterhqs.jupiterapiv2.util.StatusUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.PaymentStatusIds;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.StatusIdConstants;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;


@Service
@AllArgsConstructor
public class PurchasePaymentService {

    protected static final Logger log = LoggerFactory.getLogger(PurchasePaymentService.class);

    private PurchasePaymentRepository purchasePaymentRepository;
    private PurchaseService purchaseService;

    /**
     * Saves a new purchase
     *
     * @param PurchasePaymentDto purchasePaymentDto
     * @return PurchasePayment A new purchase payment saved
     */
    @Transactional
    public PurchasePayment save(PurchasePaymentDto purchasePaymentDto) {
        try {
            log.info(ServiceMessages.PURCHASE_PAYMENT_CREATE + ": purchaseId: " + purchasePaymentDto.getPurchase().getPurchaseId());
            PurchasePayment purchasePayment = PurchasePaymentUtil.create(purchasePaymentDto);
            Purchase purchase = purchaseService.findOneById(purchasePayment.getPurchase().getPurchaseId());
            BigDecimal partialPaid = purchase.getPartialPaid();
            BigDecimal purchaseAmount = purchase.getAmount();
            BigDecimal newPartialPaid = partialPaid.add(purchasePaymentDto.getAmount());
            purchase.setPartialPaid(newPartialPaid);

            if(newPartialPaid.compareTo(purchaseAmount) >= 0 ) {
                purchase.setPaymentStatus(PaymentStatusUtil.create(PaymentStatusIds.PAID));
                purchase.setStatus(StatusUtil.create(StatusIdConstants.COMPLETED));
            }

            purchaseService.save(purchase);

            return purchasePaymentRepository.save(purchasePayment);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("PURCHASE_PAYMENT_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     * Find all purchase payments entities
     *
     * @return List<PurchasePayment> A list with all the purchases
     */
    public Page<PurchasePayment> findAll(Pageable pageable) {
        try {
            log.info(ServiceMessages.PURCHASE_PAYMENT_FIND_ALL);

            return purchasePaymentRepository.findAll(pageable);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.PURCHASE_FIND_ALL);

            throw ex;
        }
    }

    /**
     * Find one purchase payment with an id given
     *
     * @param long id The id to find
     * @return A purchase payment found or throw PurchasePaymentNotFoundException
     */
    public PurchasePayment findOneById(long id) {
        log.info(ServiceMessages.PURCHASE_PAYMENT_FIND_BY_ID);
        return purchasePaymentRepository
                .findById(id)
                .orElseThrow(() -> new PurchasePaymentNotFoundException(String.valueOf(id)));

    }

    /**
     * it does a soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean sofDelete(long id) {
        try {
            log.info(ServiceMessages.PURCHASE_PAYMENT_DELETE);
            purchasePaymentRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }
}
