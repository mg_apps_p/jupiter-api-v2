package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Customer;
import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.entity.Sale;
import com.jupiterhqs.jupiterapiv2.entity.SaleDetail;
import com.jupiterhqs.jupiterapiv2.exception.ProductCurrentQuantityIsNotEnoughForSaleException;
import com.jupiterhqs.jupiterapiv2.exception.SaleNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.CustomerDto;
import com.jupiterhqs.jupiterapiv2.model.SaleDetailDto;
import com.jupiterhqs.jupiterapiv2.model.SaleDto;
import com.jupiterhqs.jupiterapiv2.repository.SaleRepository;
import com.jupiterhqs.jupiterapiv2.util.*;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.StatusIdConstants;
import com.jupiterhqs.jupiterapiv2.util.export.SalePDFExporter;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SaleService {

	protected static final Logger log = LoggerFactory.getLogger(SaleService.class);

	private SaleRepository saleRepository;
	private SaleDetailService saleDetailService;
	private ProductStoreService productStoreService;
	private ProductService productService;
	private CustomerService customerService;
	private SalePDFExporter salePDFExporter;

	/**
	 * Saves new sale
	 *
	 * @param SaleDto saleDto
	 * @return ProductType A new sale saved
	 */
	@Transactional
	public Sale save(SaleDto saleDto) {
		try {
			log.info(ServiceMessages.SALE_CREATE);

			Sale sale = SaleUtil.create(saleDto);
			Set<SaleDetailDto> saleDetailsDto = saleDto.getSaleDetails();
			List<Long> productStoreIds = ProductStoreUtil.create(saleDetailsDto);
			Map<Long, ProductStore> productsStoresToUpdate = productStoreService.findAllByIds(productStoreIds).stream()
					.collect(Collectors.toMap(productStore -> productStore.getProductStoreId(), Function.identity()));

			Set<SaleDetail> saleDetails = processSale(sale, saleDetailsDto, productsStoresToUpdate);

			productStoreService.saveAll(new ArrayList<>(productsStoresToUpdate.values()));

			BigDecimal amount = SaleDetailUtil.sumTotalAmount(saleDetails);
			sale.setCustomer(buildCustomer(saleDto.getCustomer()));
			sale.setSaleDetails(saleDetails);
			sale.setQuantity(SaleDetailUtil.sumQuantity(saleDetails));
			sale.setAmount(amount);
			sale.setPartialPaid(SaleUtil.getPartialPaid(saleDto, amount));
			sale.setCert(SaleUtil.buildCert(sale));

			Sale saleSaved = saleRepository.save(sale);

			calculateAveragePrice(saleSaved.getSaleDetails());
			

			return saleSaved;
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.SALE_CREATE);

			throw ex;
		}
	}

	/**
	 * Saves new sale
	 * 
	 * @param purchase
	 * @return
	 */
	public Sale save(Sale sale) {
		try {
			log.info(ServiceMessages.SALE_CREATE);

			return saleRepository.save(sale);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.PURCHASE_CREATE);

			throw ex;
		}
	}

	/**
	 * Find all sales entities
	 *
	 * @return List<Sale> A list with all sales
	 */
	public Page<Sale> findAll(Pageable pageable, String pattern) {
		try {
			log.info(ServiceMessages.SALE_FIND_ALL);

			return saleRepository.findAllByCertContainingIgnoreCase(pageable, pattern);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.SALE_FIND_ALL);

			throw ex;
		}
	}

	/**
	 * Find one sale with id given
	 *
	 * @param long id The id to find
	 * @return A sale found or throw SaleNotFoundException
	 */
	public Sale findOneById(long saleId) {
		log.info(StringParser.parseWithId(ServiceMessages.SALE_FIND_BY_ID, saleId));

		return saleRepository.findById(saleId).orElseThrow(() -> new SaleNotFoundException());
	}

	public Page<Sale> findByStore(Pageable pageable, Long storeId, String pattern) {
		try {
			log.info("Find sales by store.");

			return saleRepository.findByStoreStoreIdAndCertContainingIgnoreCase(pageable, storeId, pattern);
		} catch (Exception ex) {
			log.error(ex.getMessage());

			throw ex;
		}
	}

	/**
	 * it does soft delete for entity
	 *
	 * @param long id The id given
	 * @return Boolean true if soft deleted did
	 */
	public Boolean softDelete(long id) {
		try {
			log.info(ServiceMessages.SALE_DELETE);
			saleRepository.deleteById(id);

			return true;
		} catch (Exception ex) {
			log.error(ex.getMessage());

			return false;
		}
	}

	/**
	 * Generate the sale ticket in pdf
	 *
	 * @param response
	 * @param saleId
	 * @throws IOException
	 */
	@Transactional
	public void exportToPdf(HttpServletResponse response, long saleId) throws IOException {
		log.info(StringParser.parseWithId(ServiceMessages.SALE_PDF_EXPORT, saleId));
		Sale sale = findOneById(saleId);
		sale.setStatus(StatusUtil.create(StatusIdConstants.COMPLETED));

		salePDFExporter.setSale(sale);
		salePDFExporter.export(response);
	}

	public List<Sale> findAllByDates(LocalDateTime from, LocalDateTime to) {
		return saleRepository.findByCreatedBetween(from, to);
	}

	private Set<SaleDetail> processSale(Sale sale, Set<SaleDetailDto> saleDetailsDto,
			Map<Long, ProductStore> productsToUpdate) {
		return saleDetailsDto.stream().map(saleDetailDto -> {
			Long productStoreId = saleDetailDto.getProductStore().getProductStoreId();
			Long saleDetailQuantity = saleDetailDto.getQuantity();
			ProductStore productStore = productsToUpdate.get(productStoreId);
			if (saleDetailQuantity > productStore.getQuantity()) {
				throw new ProductCurrentQuantityIsNotEnoughForSaleException("", productStore.getQuantity());
			}
			Long currentQuantity = productStore.getQuantity() - saleDetailQuantity;
			productStore.setQuantity(currentQuantity);

			SaleDetail saleDetail = SaleDetailUtil.create(saleDetailDto, sale, productStore);
			saleDetail.setProductTags(ProductTagUtil.create(saleDetailDto.getProductTags(), saleDetail));

			return saleDetail;
		}).collect(Collectors.toSet());
	}

	private Customer buildCustomer(CustomerDto customerDto) {
		Customer customer;
		if (customerDto.getCustomerId() == null) {
			customer = customerService.save(customerDto);
		} else {
			customer = CustomerUtil.create(customerDto.getCustomerId());
		}

		return customer;
	}

	private void calculateAveragePrice(Set<SaleDetail> saleDetails) {
		saleDetails.stream().map(saleDetail -> saleDetail.getProductStore().getProduct().getProductId())
				.forEach(productId -> {
					Page<SaleDetail> saleDetailsByProduct = saleDetailService.findAllByProductIdDesc(productId);

					if (saleDetailsByProduct.getTotalElements() != 0) {
						BigDecimal priceSum = saleDetailsByProduct.stream()
								.map(saleDetailByProduct -> saleDetailByProduct.getPrice())
								.reduce(BigDecimal.ZERO, (acumulator, nextValue) -> acumulator.add(nextValue));
						BigDecimal averageSalePrice = priceSum
								.divide(new BigDecimal(saleDetailsByProduct.getTotalElements()), RoundingMode.HALF_UP);
						productService.updateAverageSalePrice(averageSalePrice, productId);
					}

				});
	}
}