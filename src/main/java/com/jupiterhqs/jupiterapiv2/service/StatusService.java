package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Status;
import com.jupiterhqs.jupiterapiv2.exception.StatusNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.StatusDto;
import com.jupiterhqs.jupiterapiv2.repository.StatusRepository;
import com.jupiterhqs.jupiterapiv2.util.StatusUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StatusService {

    protected static final Logger log = LoggerFactory.getLogger(StatusService.class);

    private StatusRepository statusRepository;

    /**
     * Saves new status
     *
     * @param StatusDto statusDto
     * @return Status A new status saved
     */
    public Status save(StatusDto statusDto) {
        try {
            log.info(ServiceMessages.STATUS_CREATE);
            Status status = StatusUtil.create(statusDto);

            return statusRepository.save(status);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.STATUS_CREATE);

            throw ex;
        }
    }

    /**
     * Find all statuses entities
     *
     * @return List<Status> A list with all statuses
     */
    public List<Status> findAll() {
        try {
            log.info(ServiceMessages.STATUS_FIND_ALL);

            return statusRepository.findAll();

        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.STATUS_FIND_ALL);

            throw ex;
        }
    }

    /**
     * it does soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            log.info(ServiceMessages.STATUS_DELETE);
            statusRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

    /**
     * Find one status with id given
     *
     * @param long id The id to find
     * @return A status found or throw StatusNotFoundException
     */
    public Status findOneById(long statusId) {
        log.info(StringParser.parseWithId(ServiceMessages.STATUS_FIND_BY_ID, statusId));

        return  statusRepository.findById(statusId).orElseThrow(() -> new StatusNotFoundException(statusId));
    }
}
