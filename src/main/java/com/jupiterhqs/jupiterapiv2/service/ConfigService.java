package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.config.DataSeed;
import com.jupiterhqs.jupiterapiv2.exception.SeedInitConfigExecuteTwiceException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ConfigService {

    private static Boolean isNotDatabaseInit = true;

    public DataSeed dbInit;

    public Boolean initDb() {
        if (isNotDatabaseInit) {
            Boolean result = dbInit.seeds();
            isNotDatabaseInit = false;
            return result;
        }
        throw new SeedInitConfigExecuteTwiceException();
    }
}
