package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Product;
import com.jupiterhqs.jupiterapiv2.entity.ProductAttribute;
import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.exception.ProductNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.ProductDto;
import com.jupiterhqs.jupiterapiv2.model.ProductStoreDto;
import com.jupiterhqs.jupiterapiv2.repository.ProductRepository;
import com.jupiterhqs.jupiterapiv2.util.ProductAttributeUtil;
import com.jupiterhqs.jupiterapiv2.util.ProductStoreUtil;
import com.jupiterhqs.jupiterapiv2.util.ProductUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class ProductService {

	protected static final Logger log = LoggerFactory.getLogger(ProductService.class);

	private ProductRepository productRepository;
	private ProductAttributeService productAttributeService;
	private ProductStoreService productStoreService;
	private ProductCodeService productCodeService;

	/**
	 * Saves a new product
	 *
	 * @param ProductDto productDTO
	 * @return Product A new product saved
	 */
	@Transactional
	public Product save(ProductDto productDto) {
		try {
			log.info(ServiceMessages.PRODUCT_CREATE);
			Product product = ProductUtil.create(productDto);
			product.setProductCode(productCodeService.buildProductCode(productDto.getProductCode()));
			Product savedProduct = productRepository.save(product);
			List<ProductAttribute> productAttributes = ProductAttributeUtil.create(savedProduct,
					productDto.getAttributes());
			List<ProductStore> productStores = ProductStoreUtil.create(savedProduct, productDto.getProductStores());

			productAttributeService.saveAll(productAttributes);
			productStoreService.saveAll(productStores);

			return savedProduct;
		} catch (Exception ex) {
			log.info(String.valueOf(ex));
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_CREATE);

			throw ex;
		}
	}

	public Product save(Product product) {
		try {
			log.info(ServiceMessages.PRODUCT_CREATE);

			return productRepository.save(product);
		} catch (Exception ex) {
			log.info(String.valueOf(ex));
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_CREATE);

			throw ex;
		}
	}

	/**
	 * Saves a new product
	 *
	 * @param Iterable<Product> products
	 * @return List<Product> A list with products saved.
	 */
	public List<Product> saveAll(Iterable<Product> products) {
		try {
			log.info(ServiceMessages.PRODUCT_SAVE_ALL);

			return productRepository.saveAll(products);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_SAVE_ALL);

			throw ex;
		}
	}

	@Transactional
	public Product update(Long productId, ProductDto productToUpdate) {
		log.info("Updating product with id " + productId);
		try {
			Product savedProduct = findOneById(productId);
			ProductUtil.update(savedProduct, productToUpdate);
			savedProduct.setProductCode(productCodeService.buildProductCode(productToUpdate.getProductCode()));
			deleteProductStores(savedProduct, productToUpdate);
			Product updatedProduct = productRepository.save(savedProduct);
			addProductStores(savedProduct, productToUpdate);
			updateAttributes(savedProduct, productToUpdate);

			return updatedProduct;
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error("PRODUCT_UPDATE_ERROR");

			throw ex;
		}

	}

	/**
	 * Saves a new product
	 *
	 * @param pageable
	 * @param pattern
	 * @return
	 */
	public Page<Product> findAll(Pageable pageable, String pattern) {
		try {
			log.info(ServiceMessages.PRODUCT_CREATE);

			return productRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
		} catch (Exception ex) {
			log.info(String.valueOf(ex));
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_CREATE);

			throw ex;
		}
	}

	/**
	 * Find all product entities
	 *
	 * @return List<Product> A list with all the products
	 */
	public List<Product> findAll() {
		try {
			log.info(ServiceMessages.PRODUCT_FIND_ALL);

			return productRepository.findAll();
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_FIND_ALL);

			throw ex;
		}
	}

	/**
	 * Find one product with an id given
	 *
	 * @param long id The id to find
	 * @return A product found or throw ProductNotFoundException
	 */
	public Product findOneById(long id) {
		log.info(ServiceMessages.PRODUCT_FIND_BY_ID + " " + id);
		return productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(String.valueOf(id)));

	}

	/**
	 * it does a soft delete for entity
	 *
	 * @param long id The id given
	 * @return Boolean true if soft deleted did
	 */
	public Boolean sofDelete(long id) {
		try {
			log.info(ServiceMessages.PRODUCT_DELETE);
			productRepository.deleteById(id);

			return true;
		} catch (Exception ex) {
			log.error(ex.getMessage());

			return false;
		}
	}

	public Product findByCodeOrSku(String codeOrSku, long storeId) {
		return productRepository.findByProductStoresStoreStoreIdAndProductCodeNameIgnoreCaseOrSkuIgnoreCase(storeId,
				codeOrSku, codeOrSku).orElseThrow(() -> new ProductNotFoundException(codeOrSku));
	}

	public List<Product> findAllByIds(List<Long> ids) {
		try {
			log.info(ServiceMessages.PRODUCT_FIND_ALL_BY_IDS);

			return productRepository.findAllByProductIdIn(ids);
		} catch (Exception ex) {
			log.info(ex.getMessage());
			log.info(ErrorMessages.PRODUCT_FIND_ALL_BY_IDS);

			throw ex;
		}
	}

	public Page<Product> findByStore(Pageable pageable, long storeId, String pattern) {
		try {
			log.info("Find product by store.");

			Page<Product> productsByName = productRepository
					.findAllByProductStoresStoreStoreIdAndNameContainingIgnoreCase(pageable, storeId, pattern);
			productsByName.getContent().forEach(product -> product.getProductStores()
					.removeIf(productStore -> productStore.getStore().getStoreId() != storeId));

			Page<Product> productsByDescription = productRepository
					.findAllByProductStoresStoreStoreIdAndDescriptionContainingIgnoreCase(pageable, storeId, pattern);
			productsByDescription.getContent().forEach(product -> product.getProductStores()
					.removeIf(productStore -> productStore.getStore().getStoreId() != storeId));

			Set<Product> allProducts = Stream
					.concat(productsByName.getContent().stream(), productsByDescription.getContent().stream())
					.collect(Collectors.toSet());

			return new PageImpl<Product>(new ArrayList<>(allProducts), pageable, allProducts.size());
		} catch (Exception ex) {
			log.error(ex.getMessage());

			throw ex;
		}
	}

	public Page<Product> findInBothStores(Pageable pageable, long storeIdOrigin, long storeIdDestination,
			String pattern) {
		try {
			log.info("Find products in the same store.");

			Page<Product> originProducts = productRepository
					.findAllByProductStoresStoreStoreIdAndNameContainingIgnoreCase(pageable, storeIdOrigin, pattern);
			List<Long> productOriginIds = originProducts.stream().map(Product::getProductId).toList();

			return productRepository.findAllByProductIdInAndProductStoresStoreStoreId(pageable, productOriginIds,
					storeIdDestination);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error("BOTH_STORES_ERROR");

			throw ex;
		}
	}

	public Page<Product> findByPattern(Pageable pageable, String pattern) {
		try {
			log.info("Find product by pattern.");

			return productRepository.findAllByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(pageable,
					pattern, pattern);
		} catch (Exception ex) {
			log.error(ex.getMessage());

			throw ex;
		}
	}

	public int updateAverageSalePrice(BigDecimal averageSalePrice, Long productId) {
		return productRepository.setAverageSalePrice(averageSalePrice, productId);
	}

	@Transactional
	public int updateAveragePurchasePrice(BigDecimal averagePurchasePrice, Long productId) {
		return productRepository.setAveragePurchasePrice(averagePurchasePrice, productId);
	}

	private void deleteProductStores(Product savedProduct, ProductDto productToUpdate) {
		if (productToUpdate.getProductStores().size() < savedProduct.getProductStores().size()) {
			List<Long> productStoresIdsExisting = productToUpdate.getProductStores().stream()
					.map(ProductStoreDto::getProductStoreId).toList();
			savedProduct.getProductStores().forEach(productStore -> {
				if (!productStoresIdsExisting.contains(productStore.getProductStoreId())
						&& productStore.getQuantity() == 0) {
					productStore.setDeleted(true);
				}
			});
		}
	}

	private void addProductStores(Product savedProduct, ProductDto productToUpdate) {
		if (productToUpdate.getProductStores().size() > savedProduct.getProductStores().size()) {
			List<ProductStore> allProductStores = productStoreService
					.findByProductIgnoreDeleted(savedProduct.getProductId());
			productToUpdate.getProductStores().removeIf(productStore -> productStore.getProductStoreId() != null);
			List<ProductStore> newProductStores = ProductStoreUtil.create(savedProduct,
					productToUpdate.getProductStores());
			newProductStores.removeIf(newProductStore -> {
				Boolean isRemove = false;
				for (ProductStore productStoreExisting : allProductStores) {
					if (productStoreExisting.getStore().getStoreId() == newProductStore.getStore().getStoreId()) {
						productStoreExisting.setDeleted(false);
						isRemove = true;
					}
				}

				return isRemove;
			});

			productStoreService.saveAll(Stream.concat(allProductStores.stream(), newProductStores.stream()).toList());

		}
	}

	private void updateAttributes(Product savedProduct, ProductDto productToUpdate) {
		if (productToUpdate.getProductAttributes() == null) {
			return;
		}
		
		List<ProductAttribute> productAttributes = ProductAttributeUtil.update(savedProduct,
				productToUpdate.getProductAttributes());

		productAttributeService.saveAll(productAttributes);
	}

}
