package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.ExpenseCategory;
import com.jupiterhqs.jupiterapiv2.exception.ExpenseCategoryNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.ExpenseCategoryDto;
import com.jupiterhqs.jupiterapiv2.repository.ExpenseCategoryRepository;
import com.jupiterhqs.jupiterapiv2.util.ExpenseCategoryUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class ExpenseCategoryService {
    protected static final Logger log = LoggerFactory.getLogger(ExpenseCategoryService.class);

    private ExpenseCategoryRepository expenseCategoryRepository;

    /**
     * Saves new expense category
     *
     * @param ExpenseCategoryDTO expenseCategoryDTO
     * @return ExpenseCategory A new expense category saved
     */

    public ExpenseCategory save(ExpenseCategoryDto expenseCategoryDto) {
        try {
            ExpenseCategory expenseCategory = ExpenseCategoryUtil.create(expenseCategoryDto);

            return expenseCategoryRepository.save(expenseCategory);
        }  catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("EXPENSE_CATEGORY_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     * Find All expense entities
     *
     * @return org.springframework.data.domain.Page<ExpenseCategory> A list with all expense categories
     */
    public Page<ExpenseCategory> findAll(Pageable pageable, String pattern) {
        try {
            log.info("FIND_ALL");

            return expenseCategoryRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("EXPENSE_CATEGORY_GET_ALL_ERROR");

            throw ex;
        }
    }

    /**
     * Find one expense category with an id given
     *
     * @param long id The id to find expense category
     * @return A expense found or throw ExpenseCategoryNotFoundException
     */
    public ExpenseCategory findOneById(long id) {
        return expenseCategoryRepository
                .findById(id).orElseThrow(() -> new ExpenseCategoryNotFoundException("EXPENSE_CATEGORY_NOT_FOUND"));
    }

    /**
     * Does a soft delete for entity
     *
     * @param long id The id to delete
     * @return boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            expenseCategoryRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }
}
