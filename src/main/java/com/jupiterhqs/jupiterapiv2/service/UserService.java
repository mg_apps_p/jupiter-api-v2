package com.jupiterhqs.jupiterapiv2.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jupiterhqs.jupiterapiv2.entity.User;
import com.jupiterhqs.jupiterapiv2.repository.UserRepository;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;


@Service
@AllArgsConstructor
public class UserService {

    protected static final Logger log = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;


    
    public Page<User> findAll(Pageable pageable, String pattern) {
        try {
            log.info(ServiceMessages.PRODUCT_CREATE);

            return userRepository.findAllByFirstnameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.info(String.valueOf(ex));
            log.error(ex.getMessage());
            log.error(ErrorMessages.PRODUCT_CREATE);

            throw ex;
        }
    }
    
    public Page<User> findByStore(Pageable pageable, long storeId, String pattern) {
        try {
            log.info("Finding users by store.");

            return userRepository.findAllByStoresStoreIdAndEmailContainingIgnoreCase(pageable, storeId, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());

            throw ex;
        }
    }


}
