package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.ProductCode;
import com.jupiterhqs.jupiterapiv2.model.ProductCodeDto;
import com.jupiterhqs.jupiterapiv2.repository.ProductCodeRepository;
import com.jupiterhqs.jupiterapiv2.util.ProductCodeUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ProductCodeService {

	protected static final Logger log = LoggerFactory.getLogger(ProductCodeService.class);

	private ProductCodeRepository productCodeRepository;

	/**
	 * Saves a new ProductCode
	 *
	 * @param ProductCodeDto productCodeDTO
	 * @return ProductCode A new ProductCode saved
	 */
	@Transactional
	public ProductCode save(ProductCodeDto productCodeDto) {
		try {
			log.info(ServiceMessages.PRODUCT_CODE_CREATE);

			return productCodeRepository.save(ProductCodeUtil.create(productCodeDto));
		} catch (Exception ex) {
			log.info(String.valueOf(ex));
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_CODE_CREATE);

			throw ex;
		}
	}

	/**
	 * Saves a new ProductCode
	 *
	 * @param pageable
	 * @param pattern
	 * @return
	 */
	public Page<ProductCode> findAll(Pageable pageable, String pattern) {
		try {
			log.info(ServiceMessages.PRODUCT_CODE_FIND_ALL);

			return productCodeRepository.findAllByDescriptionContainingIgnoreCaseOrNameContainingIgnoreCase(pageable,
					pattern, pattern);
		} catch (Exception ex) {
			log.info(String.valueOf(ex));
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_CREATE);

			throw ex;
		}
	}

	/**
	 * it does a soft delete for entity
	 *
	 * @param long id The id given
	 * @return Boolean true if soft deleted did
	 */
	public Boolean sofDelete(long id) {
		try {
			log.info(ServiceMessages.PRODUCT_DELETE);
			productCodeRepository.deleteById(id);

			return true;
		} catch (Exception ex) {
			log.error(ex.getMessage());

			return false;
		}
	}

	public ProductCode buildProductCode(ProductCodeDto productCodeDto) {
		ProductCode productCode;
		if (productCodeDto.getProductCodeId() == null) {
			productCode = save(productCodeDto);
		} else {
			productCode = ProductCodeUtil.create(productCodeDto);
		}

		return productCode;
	}

}
