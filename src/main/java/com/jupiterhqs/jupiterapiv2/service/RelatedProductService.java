package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.RelatedProduct;
import com.jupiterhqs.jupiterapiv2.model.RelatedProductDto;
import com.jupiterhqs.jupiterapiv2.repository.RelatedProductRepository;
import com.jupiterhqs.jupiterapiv2.util.RelatedProductUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RelatedProductService {

    protected static final Logger log = LoggerFactory.getLogger(RelatedProductService.class);

    private RelatedProductRepository relatedProductRepository;

    /**
     * Saves a new product
     *
     * @param RelatedProductDto relatedProductDto
     * @return List<RelatedProduct> A list with related products.
     */
    public List<RelatedProduct> save(RelatedProductDto relatedProductDto) {
        try {
            log.info(ServiceMessages.RELATED_PRODUCT_CREATE);
            List<RelatedProduct> relatedProducts = RelatedProductUtil.create(relatedProductDto);

            return relatedProductRepository.saveAll(relatedProducts);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.RELATED_PRODUCTS_CREATE);

            throw ex;
        }
    }

    /**
     * Find all product entities
     *
     * @return List<Product> A list with all the products
     */
    public List<RelatedProduct> findAll() {
        try {
            log.info(ServiceMessages.RELATED_PRODUCT_FIND_ALL);

            return relatedProductRepository.findAll();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.RELATED_PRODUCTS_FIND_ALL);

            throw ex;
        }
    }

}
