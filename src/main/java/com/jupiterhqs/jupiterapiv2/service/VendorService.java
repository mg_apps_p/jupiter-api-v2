package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Vendor;
import com.jupiterhqs.jupiterapiv2.exception.VendorNameDuplicateException;
import com.jupiterhqs.jupiterapiv2.exception.VendorNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.VendorDto;
import com.jupiterhqs.jupiterapiv2.repository.VendorRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class VendorService {
    protected static final Logger log = LoggerFactory.getLogger(VendorService.class);

    private VendorRepository vendorRepository;

    /**
     * Saves new vendor
     *
     * @param VendorDto vendorDto
     * @return Vendor A new vendor saved
     */

    public Vendor save(VendorDto vendorDto) {
        try {
            String name = vendorDto.getName();
            if (vendorRepository.findByName(name).isPresent()) {
                throw new VendorNameDuplicateException(name);
            }

            Vendor vendor = new Vendor();
            vendor.setName(vendorDto.getName());
            vendor.setDescription(vendorDto.getDescription());

            return vendorRepository.save(vendor);
        } catch (VendorNameDuplicateException ex) {
            log.error("VENDOR_NAME_DUPLICATED_ERROR");
            throw ex;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("VENDOR_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     * Find All vendor entities
     *
     * @return List<Vendor> A list with all vendors
     */
    public Page<Vendor> findAll(Pageable pageable, String pattern) {
        try {
            log.info("Finding all vendors.");

            return vendorRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("VENDOR_GET_ALL_ERROR");

            throw ex;
        }
    }

    /**
     * Find one vendor with an id given
     *
     * @param long id The id to find
     * @return A vendor found or throw VendorNotFoundException
     */
    public Vendor findOneById(long id) {
        return vendorRepository.findById(id).orElseThrow(() -> new VendorNotFoundException());
    }

    /**
     * Does a soft delete for entity
     *
     * @param long id The id to delete
     * @return boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            vendorRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

}
