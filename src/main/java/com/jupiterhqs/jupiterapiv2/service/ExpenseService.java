package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Expense;
import com.jupiterhqs.jupiterapiv2.exception.ExpenseNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.ExpenseDto;
import com.jupiterhqs.jupiterapiv2.repository.ExpenseRepository;
import com.jupiterhqs.jupiterapiv2.util.DateUtil;
import com.jupiterhqs.jupiterapiv2.util.ExpenseUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class ExpenseService {
    protected static final Logger log = LoggerFactory.getLogger(ExpenseService.class);

    private ExpenseRepository expenseRepository;

    /**
     * Saves new expense
     *
     * @param ExpenseDto expenseDTO
     * @return Expense A new expense saved
     */

    public Expense save(ExpenseDto expenseDto) {
        try {
            log.info(ServiceMessages.EXPENSE_CREATE);
            Expense expense = ExpenseUtil.create(expenseDto);
            
            Expense expenseSaved = expenseRepository.save(expense);
            
            createDateHandler(expenseDto, expenseSaved);
            
            return expenseSaved;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.EXPENSE_CREATE);

            throw ex;
        }
    }

    /**
     * Find All expense entities
     *
     * @return List<Expense> A list with all expenses
     */
    public Page<Expense> findAll(Pageable pageable, String pattern) {
        try {
            log.info(ServiceMessages.EXPENSE_FIND_ALL);

            return expenseRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.EXPENSE_FIND_ALL);

            throw ex;
        }
    }

    /**
     * Find one expense with an id given
     *
     * @param long id The id to find
     * @return A expense found or throw ExpenseNotFoundException
     */
    public Expense findOneById(long id) {
        log.info(StringParser.parseWithId(ServiceMessages.EXPENSE_FIND_BY_ID, id));

        return expenseRepository
                .findById(id).orElseThrow(() -> new ExpenseNotFoundException(String.valueOf(id)));
    }

    /**
     * Does a soft delete for entity
     *
     * @param long id The id to delete
     * @return boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            log.info(StringParser.parseWithId(ServiceMessages.EXPENSE_DELETE, id));
            expenseRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

    public List<Expense> findAllByDates(LocalDateTime from, LocalDateTime to) {
        log.info(ServiceMessages.EXPENSE_FIND_BY_RANGE_DATE);

        return expenseRepository.findByCreatedBetween(from, to);
    }

    public Expense update(long id, ExpenseDto expenseDto) {
        log.info("Updating expense");
        Expense expenseToUpdate = this.findOneById(id);
        ExpenseUtil.update(expenseToUpdate, expenseDto);

        return this.expenseRepository.save(expenseToUpdate);
    }
    
    private void createDateHandler(ExpenseDto expenseDto, Expense expense) {
        if (expenseDto.getCreated() != null) {
        	String createdDateString = expenseDto.getCreated();
        	expense.setCreated(DateUtil.getLocalDateTime(createdDateString));
        	expenseRepository.save(expense);
        }
    }
}
