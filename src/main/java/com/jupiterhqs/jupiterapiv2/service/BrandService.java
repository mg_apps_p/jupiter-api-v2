package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.exception.BrandNotFoundException;
import com.jupiterhqs.jupiterapiv2.entity.Brand;
import com.jupiterhqs.jupiterapiv2.model.BrandDto;
import com.jupiterhqs.jupiterapiv2.repository.BrandRepository;
import com.jupiterhqs.jupiterapiv2.util.BrandUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BrandService {
    protected static final Logger log = LoggerFactory.getLogger(BrandService.class);

    private BrandRepository brandRepository;

    /**
     * Saves new brand
     * @param BrandDto brandDto
     * @return Brand A new brand saved
     */

    public Brand save(BrandDto brandDto) {
        try {
            Brand brand = BrandUtil.create(brandDto);

            return brandRepository.save(brand);
        }catch (Exception ex){
            log.error(ex.getMessage());
            log.error(ErrorMessages.BRAND_CREATE);

            throw ex;
        }
    }

    /**
     * Find All brand entities
     * @return List<Brand> A list with all brands
     */
    public Page<Brand> findAll(Pageable pageable, String pattern) {
        try {
            log.info("Finding all brands");

            return brandRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.BRAND_FIND_ALL);

            throw ex;
        }
    }

    /**
     * Find one brand with an id given
     * @param long id The id to find
     * @return A brand found or throw BrandNotFoundException
     */
    public Brand findOneById(long id) {
       return  brandRepository.findById(id).orElseThrow(() -> new BrandNotFoundException(id));
    }

    /**
     * Does a soft delete for entity
     * @param long id The id to delete
     * @return boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            brandRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

}
