package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Category;
import com.jupiterhqs.jupiterapiv2.entity.Subcategory;
import com.jupiterhqs.jupiterapiv2.exception.category.CategoryNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.CategoryDto;
import com.jupiterhqs.jupiterapiv2.repository.CategoryRepository;
import com.jupiterhqs.jupiterapiv2.util.CategoryUtil;
import com.jupiterhqs.jupiterapiv2.util.SubcategoryUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@AllArgsConstructor
public class CategoryService {

    protected static final Logger log = LoggerFactory.getLogger(CategoryService.class);

    private CategoryRepository categoryRepository;

    /**
     * Saves new product type
     *
     * @param CategoryDTO categoryDTO
     * @return Category A new category saved
     */
    public Category save(CategoryDto categoryDto) {
        try {
            Category category = CategoryUtil.create(categoryDto);
            Set<Subcategory> subcategories = SubcategoryUtil.create(categoryDto.getSubcategories(), category);
            category.setSubcategories(subcategories);

            return categoryRepository.saveAndFlush(category);
        } catch (DataIntegrityViolationException ex) {
            log.error("CATEGORY_DUPLICATED_CODE_ERROR");
            throw ex;

        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("CATEGORY_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     * Find all product types entities
     *
     * @return List<Category> A list with all categories
     */
    public Page<Category> findAll(Pageable pageable, String pattern) {
        try {
            log.info("Finding all categories...");

            return categoryRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("CATEGORY_GET_ALL_ERROR");

            throw ex;
        }

    }

    /**
     * Find one product type with id given
     *
     * @param long id The id to find
     * @return A category found or throw CategoryNotFoundException
     */
    public Category findOneById(long id) {
        return categoryRepository.findById(id).orElseThrow(() -> new CategoryNotFoundException());
    }

    /**
     * it does soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            categoryRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }
    
    @Transactional
    public Category update(Long categoryId, CategoryDto categoryToUpdate) {
    	log.info("Updating category with id " + categoryId);
    	try {
    		Category savedCategory = findOneById(categoryId);
    		savedCategory.setName(categoryToUpdate.getName());	
    		savedCategory.setSubcategories(SubcategoryUtil.create(categoryToUpdate.getSubcategories(), savedCategory));
    		
    		return categoryRepository.save(savedCategory);
    	} catch (Exception ex) {
    		log.error(ex.getMessage());
    		log.error("CATEGORY_UPDATE_ERROR");
    		
    		throw ex;
    	}
    	
    }

}
