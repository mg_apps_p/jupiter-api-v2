package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.entity.Purchase;
import com.jupiterhqs.jupiterapiv2.entity.PurchaseDetail;
import com.jupiterhqs.jupiterapiv2.entity.Vendor;
import com.jupiterhqs.jupiterapiv2.exception.PurchaseNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDetailDto;
import com.jupiterhqs.jupiterapiv2.model.PurchaseDto;
import com.jupiterhqs.jupiterapiv2.model.VendorDto;
import com.jupiterhqs.jupiterapiv2.repository.PurchaseRepository;
import com.jupiterhqs.jupiterapiv2.util.ProductStoreIdUtil;
import com.jupiterhqs.jupiterapiv2.util.PurchaseDetailUtil;
import com.jupiterhqs.jupiterapiv2.util.PurchaseUtil;
import com.jupiterhqs.jupiterapiv2.util.VendorUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PurchaseService {

	protected static final Logger log = LoggerFactory.getLogger(PurchaseService.class);

	private PurchaseRepository purchaseRepository;
	private ProductStoreService productStoreService;
	private PurchaseDetailService purchaseDetailService;
	private ProductService productService;
	private VendorService vendorService;

	/**
	 * Saves a new purchase
	 *
	 * @param PurchaseDto purchaseDTO
	 * @return Purchase A new purchase saved
	 */
	@Transactional
	public Purchase save(PurchaseDto purchaseDto) throws NoSuchAlgorithmException {
		try {
			log.info(ServiceMessages.PURCHASE_CREATE);

			List<Long> productStoreIds = ProductStoreIdUtil.create(purchaseDto.getPurchaseDetails());

			Map<Long, ProductStore> productsStoresToUpdate = productStoreService.findAllByIds(productStoreIds).stream()
					.collect(Collectors.toMap(productStore -> productStore.getProductStoreId(), Function.identity()));

			Purchase purchase = PurchaseUtil.create(purchaseDto);
			purchase.setVendor(buildVendor(purchaseDto.getVendor()));
			purchase.setPurchaseDetails(
					processPurchases(purchase, purchaseDto.getPurchaseDetails(), productsStoresToUpdate));
			purchase.setReference(PurchaseUtil.buildReference(purchase.toString()));

			productStoreService.saveAll(new ArrayList<>(productsStoresToUpdate.values()));

			Purchase purchaseSaved = purchaseRepository.save(purchase);

			calculateAveragePrice(purchaseSaved.getPurchaseDetails());

			return purchaseSaved;
		} catch (NoSuchAlgorithmException ex) {
			log.error(ex.getMessage());
			log.error("SHA_HASH_CREATOR_ERROR");

			throw ex;
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.PURCHASE_CREATE);

			throw ex;
		}
	}

	public Purchase save(Purchase purchase) {
		try {
			log.info(ServiceMessages.PURCHASE_CREATE);

			return purchaseRepository.save(purchase);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.PURCHASE_CREATE);

			throw ex;
		}
	}

	/**
	 * Find all purchase entities
	 *
	 * @return List<Purchase> A list with all the purchases
	 */
	public Page<Purchase> findAll(Pageable pageable, String pattern) {
		try {
			log.info(ServiceMessages.PURCHASE_FIND_ALL);

			return purchaseRepository.findAllByReferenceContainingIgnoreCase(pageable, pattern);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.PURCHASE_FIND_ALL);

			throw ex;
		}
	}

	/**
	 * Find one purchase with an id given
	 *
	 * @param long id The id to find
	 * @return A purchase found or throw PurchaseNotFoundException
	 */
	public Purchase findOneById(long id) {
		log.info(ServiceMessages.PURCHASE_FIND_BY_ID);
		return purchaseRepository.findById(id).orElseThrow(() -> new PurchaseNotFoundException(String.valueOf(id)));

	}

	/**
	 * it does a soft delete for entity
	 *
	 * @param long id The id given
	 * @return Boolean true if soft deleted did
	 */
	public Boolean sofDelete(long id) {
		try {
			log.info(ServiceMessages.PURCHASE_DELETE);
			purchaseRepository.deleteById(id);

			return true;
		} catch (Exception ex) {
			log.error(ex.getMessage());

			return false;
		}
	}

	private Set<PurchaseDetail> processPurchases(Purchase purchase, List<PurchaseDetailDto> purchaseDetailsDto,
			Map<Long, ProductStore> productStores) {
		return purchaseDetailsDto.stream().map((purchaseDetailDto -> {
			ProductStore productStoreToUpdate = productStores
					.get(purchaseDetailDto.getProductStore().getProductStoreId());
			productStoreToUpdate.setQuantity(productStoreToUpdate.getQuantity() + purchaseDetailDto.getQuantity());

			return PurchaseDetailUtil.create(purchaseDetailDto, purchase, productStoreToUpdate);
		})).collect(Collectors.toSet());
	}

	private void calculateAveragePrice(Set<PurchaseDetail> purchaseDetails) {
		purchaseDetails.stream().map(saleDetail -> saleDetail.getProductStore().getProduct().getProductId())
				.forEach(productId -> {
					Page<PurchaseDetail> purchaseDetailsByProduct = purchaseDetailService
							.findAllByProductIdDesc(productId);

					if (purchaseDetailsByProduct.getTotalElements() != 0) {
						BigDecimal priceSum = purchaseDetailsByProduct.stream()
								.map(saleDetailByProduct -> saleDetailByProduct.getPrice())
								.reduce(BigDecimal.ZERO, (acumulator, nextValue) -> acumulator.add(nextValue));
						BigDecimal averagePurchasePrice = priceSum.divide(
								new BigDecimal(purchaseDetailsByProduct.getTotalElements()), RoundingMode.HALF_UP);
						productService.updateAveragePurchasePrice(averagePurchasePrice, productId);
					}
				});

	}

	private Vendor buildVendor(VendorDto vendorDto) {
		Vendor vendor;
		if (vendorDto.getVendorId() == null) {
			vendor = vendorService.save(vendorDto);
		} else {
			vendor = VendorUtil.create(vendorDto.getVendorId());
		}

		return vendor;
	}
}
