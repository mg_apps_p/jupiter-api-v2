package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Investment;
import com.jupiterhqs.jupiterapiv2.exception.InvestmentNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.InvestmentDto;
import com.jupiterhqs.jupiterapiv2.repository.InvestmentRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class InvestmentService {

    protected static Logger log = LoggerFactory.getLogger(InvestmentService.class);

    private InvestmentRepository investmentRepository;

    public ResponseEntity<Investment> save(InvestmentDto investmentDto) {
        try {
            Investment investment = new Investment();
            investment.setName(investmentDto.getName());
            investment.setAmount(investmentDto.getAmount());


            return ResponseEntity.ok( investmentRepository.save(investment));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("INVESTMENT_SAVE_ERROR");

            return ResponseEntity.internalServerError().build();
        }
    }

    public ResponseEntity<List<Investment>> findAll() {
        try {
            return ResponseEntity.ok(investmentRepository.findAll());

        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("INVESTMENT_GET_ALL_ERROR");

            return ResponseEntity.internalServerError().build();
        }
    }

    public ResponseEntity<Investment> findOneByID(long id) {
        Investment investment = investmentRepository.findById(id).orElseThrow(InvestmentNotFoundException::new);

        return ResponseEntity.ok(investment);
    }

    public ResponseEntity<Boolean> softDelete(long id) {
        try {
            investmentRepository.deleteById(id);

            return ResponseEntity.ok(true);
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return ResponseEntity.ok(false);
        }
    }
}
