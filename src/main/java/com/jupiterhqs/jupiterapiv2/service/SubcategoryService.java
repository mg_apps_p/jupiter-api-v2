package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Subcategory;
import com.jupiterhqs.jupiterapiv2.exception.SubcategoryNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.SubcategoryDto;
import com.jupiterhqs.jupiterapiv2.repository.SubcategoryRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@AllArgsConstructor
public class SubcategoryService {

    protected static final Logger log = LoggerFactory.getLogger(SubcategoryService.class);

    private SubcategoryRepository subCategoryRepository;

    /**
     * Saves a new subcategory
     * @param SubcategoryDto subcategoryDTO
     * @return Subcategory A subcategory saved
     */
    public ResponseEntity<Subcategory> save(SubcategoryDto subCategoryDto){
        try {
            Subcategory subcategory = new Subcategory();

            subcategory.setName(subCategoryDto.getName());

            return ResponseEntity.ok(subCategoryRepository.save(subcategory));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("SUB_CATEGORY_SAVE_ERROR");

            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Find all subcategory entities
     * @return List<Subcategory> A list with all Subcategories
     */
    public ResponseEntity<List<Subcategory>> findAll() {
        try {
            return ResponseEntity.ok(subCategoryRepository.findAll());
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("SUB_CATEGORY_GET_ALL_ERROR");

            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Find one subcategory with an id given
     * @param long id The id to find
     * @return A business found or throw SubcategoryNotFoundException
     */
    public ResponseEntity<Subcategory> findOneById(long id) {
        Subcategory subCategory = subCategoryRepository.findById(id).orElseThrow(SubcategoryNotFoundException::new);

        return ResponseEntity.ok(subCategory);
    }

    /**
     * Finds a subcategories with given categoryId
     * @param categoryId The category identifier to find
     * @return A list with subcategories related with categoryId
     */
    public ResponseEntity<List<Subcategory>> findByCategoryId(long categoryId) {
        List<Subcategory> foundSubcategories = subCategoryRepository
                .findByCategoryCategoryId(categoryId).orElseThrow(SubcategoryNotFoundException::new);

        return ResponseEntity.ok(foundSubcategories);
    }

    /**
     * It does a soft delete for entity by id
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    @Transactional
    public ResponseEntity<Boolean> softDelete(long id){
    	
        try{
            subCategoryRepository.deleteById(id);

            return ResponseEntity.ok(true);
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return ResponseEntity.ok(false);
        }
    }
}
