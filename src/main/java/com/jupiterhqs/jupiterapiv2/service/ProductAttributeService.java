package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.ProductAttribute;
import com.jupiterhqs.jupiterapiv2.repository.ProductAttributeRepository;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductAttributeService {

    protected static final Logger log = LoggerFactory.getLogger(ProductAttributeService.class);

    private ProductAttributeRepository productAttributeRepository;

    
    public List<ProductAttribute> saveAll(Iterable<ProductAttribute> productAttributes) {
        try {
            log.info("Saving all product attributes...");

            return productAttributeRepository.saveAll(productAttributes);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.PRODUCT_ATTRIBUTE_CREATE);

            throw ex;
        }
    }

    public List<ProductAttribute> findByProduct(Long productId) {
        log.info(StringParser.parseWithId("Finding product attributes by product id = ", productId));

        return productAttributeRepository.findByProductProductId(productId);
    }

}
