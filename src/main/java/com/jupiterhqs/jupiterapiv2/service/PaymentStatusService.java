package com.jupiterhqs.jupiterapiv2.service;


import com.jupiterhqs.jupiterapiv2.entity.PaymentStatus;
import com.jupiterhqs.jupiterapiv2.exception.PaymentStatusNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.PaymentStatusDto;
import com.jupiterhqs.jupiterapiv2.repository.PaymentStatusRepository;
import com.jupiterhqs.jupiterapiv2.util.PaymentStatusUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PaymentStatusService {

    protected static final Logger log = LoggerFactory.getLogger(PaymentStatusService.class);

    private PaymentStatusRepository paymentStatusRepository;

    /**
     * Saves new payment type
     *
     * @param PaymentStatus paymentStatusDto
     * @return PaymentStatus A new payment type saved
     */
    public PaymentStatus save(PaymentStatusDto paymentStatusDto) {
        try {
            PaymentStatus paymentType = PaymentStatusUtil.create(paymentStatusDto);

            return paymentStatusRepository.save(paymentType);
        } catch (DataIntegrityViolationException ex) {
            throw new RuntimeException("PAYMENT_STATUS_NAME_DUPLICATED: " + paymentStatusDto.getName());
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("PAYMENT_STATUS_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     *
     * @param pageable A pageable data
     * @param pattern A pattern to find
     * @return A Page with payment status content
     */
    public Page<PaymentStatus> findAll(Pageable pageable, String pattern) {
        try {
            log.info("Find all payment status...");

            return paymentStatusRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("PAYMENT_STATUS_GET_ALL_ERROR");

            throw ex;
        }
    }


    /**
     * Find one payment type with an id given
     *
     * @param long id The id to find
     * @return A payment type found or throw PaymentStatusNotFoundException
     */
    public PaymentStatus findOneById(long id) {
        return paymentStatusRepository.findById(id).orElseThrow(() -> new PaymentStatusNotFoundException());
    }

    /**
     * It does a soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            paymentStatusRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

}
