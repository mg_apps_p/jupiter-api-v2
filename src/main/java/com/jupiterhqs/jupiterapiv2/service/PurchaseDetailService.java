package com.jupiterhqs.jupiterapiv2.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.jupiterhqs.jupiterapiv2.entity.PurchaseDetail;
import com.jupiterhqs.jupiterapiv2.repository.PurchaseDetailRepository;


@Service
@AllArgsConstructor
public class PurchaseDetailService {

	protected static final Logger log = LoggerFactory.getLogger(PurchaseDetailService.class);

	private PurchaseDetailRepository purchaseDetailRepository;

	public Page<PurchaseDetail> findAllByProductIdDesc(Long productId) {
		return purchaseDetailRepository.findAllByProductStoreProductProductId(
				PageRequest.of(0, 50, Sort.by(Order.desc("created"))), productId);
	}
}
