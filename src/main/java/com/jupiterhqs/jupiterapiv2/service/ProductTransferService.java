package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.ProductStore;
import com.jupiterhqs.jupiterapiv2.entity.ProductTransfer;
import com.jupiterhqs.jupiterapiv2.entity.ProductTransferDetail;
import com.jupiterhqs.jupiterapiv2.exception.ProductCurrentQuantityIsNotEnoughForProductTransferException;
import com.jupiterhqs.jupiterapiv2.model.ProductTransferDetailDto;
import com.jupiterhqs.jupiterapiv2.model.ProductTransferDto;
import com.jupiterhqs.jupiterapiv2.repository.ProductTransferRepository;
import com.jupiterhqs.jupiterapiv2.util.*;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProductTransferService {

	protected static final Logger log = LoggerFactory.getLogger(ProductTransferService.class);

	private ProductTransferRepository productTransferRepository;
	private ProductStoreService productStoreService;

	/**
	 * Saves new product transfer
	 *
	 * @param ProductTransferDto productTranferDto
	 * @return ProductType A new sale saved
	 */
	@Transactional
	public ProductTransfer save(ProductTransferDto productTranferDto) {
		try {
			log.info(ServiceMessages.PRODUCT_TRANSFER_CREATE);

			ProductTransfer productTransfer = ProductTransferUtil.create(productTranferDto);
			Set<Long> productStoreIds = ProductTransferUtil
					.getProductStoreIds(productTranferDto.getProductTransferDetails());
			Map<Long, ProductStore> productsStoresToUpdate = productStoreService
					.findAllByIds(new ArrayList<>(productStoreIds)).stream()
					.collect(Collectors.toMap(productStore -> productStore.getProductStoreId(), Function.identity()));
			Set<ProductTransferDetail> productTransferDetails = processProductTransfer(productTransfer,
					productTranferDto.getProductTransferDetails(), productsStoresToUpdate);

			productStoreService.saveAll(new ArrayList<>(productsStoresToUpdate.values()));
			productTransfer.setProductTransferDetails(productTransferDetails);
			productTransfer.setQuantity(ProductTransferDetailUtil.sumQuantity(productTransferDetails));

			return productTransferRepository.save(productTransfer);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_TRANSFER_CREATE);

			throw ex;
		}
	}
	
	public Page<ProductTransfer> findAll(Pageable pageable) {
		try {
			log.info(ServiceMessages.PRODUCT_TRANSFER_FIND_ALL);

			return productTransferRepository.findAll(pageable);
		} catch (Exception ex) {
			log.info(String.valueOf(ex));
			log.error(ex.getMessage());
			log.error(ErrorMessages.PRODUCT_TRASFER_FIND_ALL);

			throw ex;
		}
	}

	/**
	 * it does soft delete for entity
	 *
	 * @param long id The id given
	 * @return Boolean true if soft deleted did
	 */
	public Boolean softDelete(long id) {
		try {
			log.info(ServiceMessages.PRODUCT_TRANSFER_DELETE);
			productTransferRepository.deleteById(id);

			return true;
		} catch (Exception ex) {
			log.error(ex.getMessage());

			return false;
		}
	}

	private Set<ProductTransferDetail> processProductTransfer(
	            ProductTransfer productTransfer,
	            Set<ProductTransferDetailDto> productTransferDetailDtos,
	            Map<Long, ProductStore> productsToUpdate
    ) {
       
		return productTransferDetailDtos.stream().map(productTransferDetailDto -> {
        	ProductStore productStoreOrigin = productsToUpdate.get(productTransferDetailDto.getProductStoreOrigin().getProductStoreId());
        	ProductStore productStoreDestination = productsToUpdate.get(productTransferDetailDto.getProductStoreDestination().getProductStoreId());
        	
        	Long productTransferQuantity = productTransferDetailDto.getQuantity();
            if ( productTransferQuantity > productStoreOrigin.getQuantity()) {
                throw new ProductCurrentQuantityIsNotEnoughForProductTransferException(productStoreOrigin.getProduct().getName(), productTransferQuantity);
            }
            productStoreOrigin.setQuantity(productStoreOrigin.getQuantity() - productTransferQuantity);
            productStoreDestination.setQuantity(productStoreDestination.getQuantity() + productTransferQuantity);

            ProductTransferDetail productTransferDetail = ProductTransferDetailUtil.create(productTransferDetailDto, productTransfer);

            return productTransferDetail;
		}).collect(Collectors.toSet());
    }
}