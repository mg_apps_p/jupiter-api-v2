package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.Store;
import com.jupiterhqs.jupiterapiv2.exception.StoreNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.StoreDto;
import com.jupiterhqs.jupiterapiv2.repository.StoreRepository;
import com.jupiterhqs.jupiterapiv2.util.StoreUtil;
import com.jupiterhqs.jupiterapiv2.util.constants.ErrorMessages;
import com.jupiterhqs.jupiterapiv2.util.constants.ServiceMessages;
import com.jupiterhqs.jupiterapiv2.util.parse.StringParser;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StoreService {

    protected static final Logger log = LoggerFactory.getLogger(StoreService.class);

    private StoreRepository storeRepository;

    /**
     * saves a new store
     *
     * @param StoreDTO storeDto
     * @return Store A new store saved
     */
    public Store save(StoreDto storeDto) {

        try {
            log.info(ServiceMessages.STORE_CREATE);
            Store store = StoreUtil.create(storeDto);

            return storeRepository.save(store);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.STORE_CREATE);

            throw ex;
        }
    }

    /**
     * Find all store entities
     *
     * @return List<Store> A list with all stores
     */
    public Page<Store> findAll(Pageable pageable, String pattern) {
        try {
            log.info(ServiceMessages.STORE_FIND_ALL);

            return storeRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(ErrorMessages.STORE_FIND_ALL);

            throw ex;
        }
    }

    /**
     * Find one store with an id given
     *
     * @param long id The id to find
     * @return A store found or throw StoreNotFoundException
     */
    public Store findOneById(long id) {
        log.info(StringParser.parseWithId(ServiceMessages.STORE_FIND_BY_ID, id));

        return storeRepository.findById(id).orElseThrow(() -> new StoreNotFoundException(String.valueOf(id)));
    }

    /**
     * It does a soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            log.info(StringParser.parseWithId(ServiceMessages.STORE_DELETE, id));
            storeRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

    public Store update(long id, StoreDto storeDto) {
        try {
            log.info((StringParser.parseWithId(ServiceMessages.STORE_UPDATE, id)));
            Store storeToUpdate = this.findOneById(id);
            storeToUpdate.setName(storeDto.getName());
            storeToUpdate.setAddress(storeDto.getAddress());
            storeToUpdate.setEmail(storeDto.getEmail());
            storeToUpdate.setPhone(storeDto.getPhone());

            return this.storeRepository.save(storeToUpdate);
        } catch (Exception ex){
            log.error(ex.getMessage());
            log.info(ErrorMessages.STORE_UPDATE);

            throw ex;
        }
    }
}
