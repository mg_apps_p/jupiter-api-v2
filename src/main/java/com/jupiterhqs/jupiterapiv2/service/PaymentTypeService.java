package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.PaymentType;
import com.jupiterhqs.jupiterapiv2.exception.PaymentTypeNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.PaymentTypeDto;
import com.jupiterhqs.jupiterapiv2.repository.PaymentTypeRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PaymentTypeService {

    protected static final Logger log = LoggerFactory.getLogger(PaymentTypeService.class);

    private PaymentTypeRepository paymentTypeRepository;

    /**
     * Saves new payment type
     *
     * @param PaymentTypeDto paymentTypeDto
     * @return PaymentType A new payment type saved
     */
    public PaymentType save(PaymentTypeDto paymentTypeDto) {
        try {
            PaymentType paymentType = new PaymentType();
            paymentType.setName(paymentTypeDto.getName());
            paymentType.setDescription(paymentTypeDto.getDescription());

            return paymentTypeRepository.save(paymentType);
        } catch (DataIntegrityViolationException ex) {
            throw new RuntimeException("PAYMENT_TYPE_NAME_DUPLICATED: " + paymentTypeDto.getName());
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("PAYMENT_TYPE_SAVE_ERROR");

            throw ex;
        }
    }

    /**
     *
     * @param pageable
     * @param pattern
     * @return
     */
    public Page<PaymentType> findAll(Pageable pageable, String pattern) {
        try {
            log.info("Find all payment types");

            return paymentTypeRepository.findAllByNameContainingIgnoreCase(pageable, pattern);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error("PAYMENT_TYPE_GET_ALL_ERROR");

            throw ex;
        }
    }


    /**
     * Find one payment type with an id given
     *
     * @param long id The id to find
     * @return A payment type found or throw PaymenrTypeNotFoundException
     */
    public PaymentType findOneById(long id) {
        return paymentTypeRepository.findById(id).orElseThrow(() -> new PaymentTypeNotFoundException());
    }

    /**
     * It does a soft delete for entity
     *
     * @param long id The id given
     * @return Boolean true if soft deleted did
     */
    public Boolean softDelete(long id) {
        try {
            paymentTypeRepository.deleteById(id);

            return true;
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return false;
        }
    }

}
