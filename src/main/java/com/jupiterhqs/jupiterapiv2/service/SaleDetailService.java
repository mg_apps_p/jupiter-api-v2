package com.jupiterhqs.jupiterapiv2.service;

import com.jupiterhqs.jupiterapiv2.entity.SaleDetail;
import com.jupiterhqs.jupiterapiv2.exception.SaleDetailNotFoundException;
import com.jupiterhqs.jupiterapiv2.model.SaleDetailDto;
import com.jupiterhqs.jupiterapiv2.repository.SaleDetailRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@AllArgsConstructor
public class SaleDetailService {

	protected static final Logger log = LoggerFactory.getLogger(SaleDetailService.class);

	private SaleDetailRepository saleDetailRepository;

	/**
	 * Saves a new Sale Detail
	 *
	 * @param SaleDetailDto saleDetailDto
	 * @return SaleDetail A new sale detail saved
	 */
	public ResponseEntity<SaleDetail> save(SaleDetailDto saleDetailDto) {
		try {
			SaleDetail saleDetail = new SaleDetail();
			saleDetail.setDescription(saleDetailDto.getDescription());
			saleDetail.setPrice(saleDetailDto.getPrice());
			saleDetail.setTotalPrice(saleDetailDto.getPrice().multiply(new BigDecimal(saleDetailDto.getQuantity())));
			saleDetail.setQuantity(saleDetailDto.getQuantity());

			return ResponseEntity.ok(saleDetailRepository.save(saleDetail));
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error("SALE_DETAIL_SAVE_ERROR");

			return ResponseEntity.internalServerError().build();
		}
	}

	/**
	 * Finds all sale details entities
	 *
	 * @return List<SaleDetail> A list with all sale details
	 */
	public ResponseEntity<List<SaleDetail>> findAll() {
		try {
			return ResponseEntity.ok(saleDetailRepository.findAll());
		} catch (Exception ex) {
			log.error(ex.getMessage());
			log.error("SALE_DETAIL_GET_ALL_ERROR");

			return ResponseEntity.internalServerError().build();
		}
	}

	/**
	 * Find one sale detail with an id given
	 *
	 * @param long id The id to find
	 * @return A sale detail found or throw SaleDetailNotFoundException
	 */
	public ResponseEntity<SaleDetail> findOneById(long id) {
		SaleDetail saleDetail = saleDetailRepository.findById(id).orElseThrow(() -> new SaleDetailNotFoundException());

		return ResponseEntity.ok(saleDetail);
	}

	/**
	 * It does a soft delete for entity
	 *
	 * @param long id The id given
	 * @return Boolean true if soft delete did
	 */
	public ResponseEntity<Boolean> softdelete(long id) {
		try {
			saleDetailRepository.deleteById(id);

			return ResponseEntity.ok(true);
		} catch (Exception ex) {
			log.error(ex.getMessage());

			return ResponseEntity.ok(false);
		}
	}

	public Page<SaleDetail> findAllByProductIdDesc(Long productId) {
		return saleDetailRepository.findAllByProductStoreProductProductId(
				PageRequest.of(0, 50, Sort.by(Order.desc("created"))), productId);
	}
}
